<?php
//include("../includes/db_connect.php");
$con = connect();

$sql = "SELECT * FROM tbl_product_cat ORDER BY product_cat_name ASC ";

$r = $con->query($sql) or die ($sql);

?>
<div class="container-fluid my-3">
    <div class="row">
        <div class="col-lg-12">
<h3 class="text-center textshadow"> หมวดสินค้า </h3>
<hr>
<span> 

<a href="#" class="btn btn-outline-info" data-toggle="modal" data-target="#myModal"  > <i class="fas fa-plus-circle"></i> เพิ่มหมวดสินค้า </a> 

<a href="?page=main" class="btn btn-outline-success" > <i class="fas fa-list"></i> กลับไปรายการสินค้า </a> 

</span>
<table id="tb1" class="table table-striped table-bordered table-sm">
                        <thead>
                            <tr>
                                <th>ลำดับ</th>
                                <th>ชื่อหมวด</th>
                                <th class='text-center'>การกระทำ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        if($r->num_rows > 0 ) {
                            $i = 0;
                            while ($result = $r->fetch_object()) {
                                $i++;
                                ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $result->product_cat_name; ?></td>
                                    <td class='text-center'>
                                        <a href="#" onclick="edit_cat('<?php echo $result->product_cat_id;?>');" data-toggle="modal" data-target="#md2" class="text-primary"><i class="fa fa-edit"></i></a> &nbsp;  &nbsp;
                                        <a href="#" onclick="del_cat('<?php echo $result->product_cat_id;?>');" class="text-danger"><i class="far fa-trash-alt"></i> </a>
                                    </td>
                                </tr>
                            <?php
                        }
                    }
                        $con->close();
                        ?>
                        </tbody>
                    </table>

        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal" id="myModal">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">เพิ่มหมวดสินค้า</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="md_body">

        <form id="form_cat" method="POST" class="needs-validation" >
            <div class="form-group">
                <label for="uname">ชื่อหมวดสินค้า:</label>
                <input type="text" class="form-control" id="product_cat_name"  name="product_cat_name" required>
                <div class="text-danger" id="show_same"></div>
            </div>
            <button type="submit" class="btn btn-primary">บันทึก</button>
        </form>

      </div>


    </div>
  </div>
</div>
<!-- The Modal -->

<!-- The Modal -->
<div class="modal" id="md2">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">แก้ไขหมวดสินค้า</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="md_body2">

        

      </div>


    </div>
  </div>
</div>
<!-- The Modal -->


<script>

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารายการ :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [0, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 25,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": false,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": false,
    //"info": false,
    //"ordering": false,
    //"searching": false,
    //"paging":  false

});

function del_cat(p_id) {
    Swal.fire({
        title: 'ลบหมวดสินค้า ?',
        text: "กดยืนยันหากต้องการลบ",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.post("cat_del.php",{cat_id: p_id},function(info){
                if(info == 'ok') {
                    window.location = "?page=cat";
                } else {
                    alert(info);
                }
            });
        }
    });
}

$('#form_cat').submit(function(e){
    e.preventDefault();
    //alert("ss");
    $.post("cat_add_action.php",$('#form_cat').serialize(),function(info){
        if(info == 'ok') {
            $("#myModal").modal("hide");
            window.location = "?page=cat";
        } else {
            $('#show_same').html(info);
        }
    });
});

function edit_cat(id) {
    $('#md_body2').load("cat_edit.php?product_cat_id="+id);
}
</script>                    