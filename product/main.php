<?php
//include("../includes/db_connect.php");
$con = connect();

$sql = "SELECT * FROM tbl_product as e left join tbl_product_cat as t on t.product_cat_id = e.product_cat_id ORDER BY e.product_name ASC ";

$r = $con->query($sql) or die ($sql);

?>
<!-- <h3 class="text-center textshadow"> ข้อมูลสินค้า </h3> -->
<!-- <hr> -->
<div class="container-fluid my-3">
    <div class="row">
        <div class="col-lg-12">
        
        
<span> 
<a href="../?page=main" class="btn btn-outline-success mb-2" > <i class="fas fa-home"></i> กลับหน้าหลัก </a> 
<a href="?page=product_add" class="btn btn-info mb-2" > <i class="fas fa-plus"></i> เพิ่มรายการสินค้า </a> 
<a href="?page=cat" class="btn btn-outline-secondary mb-2" > <i class="fas fa-tools"></i> จัดการหมวดสินค้า </a> 
</span>
<table id="tb1" class="table table-striped table-bordered table-sm">
                        <thead>
                            <tr>
                                <th width="50">รหัส</th>
                                <th class='text-center'>รูปภาพ</th>
                                <th>ชื่อสินค้า</th>
                                <th>ชื่อหมวด</th>
                                <th>คำอธิบาย</th>
                                <th class='text-right'>ราคาทุน</th>
                                <th class='text-right'>ราคาขาย</th>
                                <th class='text-center'>หน่วยนับ</th>
                                <th class='text-center' width="70">จุดสั่งซื้อ</th>
                                <th class='text-center' width="50"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        if($r->num_rows > 0 ) {
                            while ($result = $r->fetch_object()) {
                                if($result->product_images == NULL || $result->product_images == "") {
                                    $img = "images/noimage89.png";
                                } else {
                                    $img = "images/".$result->product_images;
                                }
                                ?>
                                <tr>
                                    <td><?php echo $result->product_id; ?></td>
                                    <td class='text-center'>
                                    <img src="<?php echo $img;?>" class="img-thumbnail img-fluid" alt="<?php echo $result->product_name; ?>"  width="60" height="34" >
                                    <?php
                                        
                                    ?>
                                    </td>
                                    <td><?php echo $result->product_name; ?></td>
                                    <td><?php echo $result->product_cat_name; ?></td>
                                    <td><?php echo $result->product_details; ?></td>
                                    <td class='text-right'><?php echo money($result->unit_cost); ?></td>
                                    <td class='text-right'><?php echo money($result->unit_price); ?></td>
                                    <td class='text-center'><?php echo $result->unit_name; ?></td>
                                    <td class='text-right'><?php echo money($result->min_stock); ?></td>
                                    <td class='text-center'>
                                        <a href="?page=product_edit&product_id=<?php echo $result->product_id;?>" class="text-primary"><i class="fa fa-edit"></i></a> &nbsp;
                                        <a href="#" onclick="del_product('<?php echo $result->product_id;?>');" class="text-danger"><i class="far fa-trash-alt"></i> </a>
                                    </td>
                                </tr>
                            <?php
                        }
                    }
                        $con->close();
                        ?>
                        </tbody>
                    </table>


        </div>
    </div>
</div>

<script>

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารายการสินค้า :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [0, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": true,
    //"info": false,
    //"ordering": false,
    //"searching": false,
    //"paging":  false

});

function del_product(p_id) {
    Swal.fire({
        title: 'ลบสินค้า ?',
        text: "กดยืนยันหากต้องการลบ",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.post("product_del.php",{product_id: p_id},function(info){
                if(info == 'ok') {
                    window.location = "?page=main";
                } else {
                    alert(info);
                }
            });
        }
    });
}

</script>                    