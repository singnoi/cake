<?php
include("../includes/db_connect.php");
$con = connect();
$cat_id = $_GET['product_cat_id'];
$q = "SELECT * from tbl_product_cat where product_cat_id = '$cat_id' ";
$r = $con->query($q) or die ($q);
$ob = $r->fetch_object();
?>
<form id="form_cat_edit" method="POST" class="needs-validation" >
    <input type="hidden" name="product_cat_id" value="<?php echo $cat_id;?>" >
            <div class="form-group">
                <label for="uname">ชื่อหมวดสินค้า:</label>
                <input type="text" class="form-control" id="product_cat_name"  name="product_cat_name" value="<?php echo $ob->product_cat_name;?>" required>
                <div class="text-danger" id="show_edit"></div>
            </div>
            <button type="submit" class="btn btn-primary">บันทึก</button>
</form>

<script>
$('#form_cat_edit').submit(function(e){
    e.preventDefault();

    $.post("cat_add_action.php",$('#form_cat_edit').serialize(),function(info){


        if(info == 'ok') {
            $("#md2").modal("hide");
            window.location = "?page=cat";
        } else {
            $('#show_edit').html(info);
        }
        
    });
});
</script>