<h3 class="text-center textshadow"> เพิ่มสินค้า </h3>
<form id="data" action="product_add_action.php" method="post" enctype="multipart/form-data">
<div class="card">
        <div class="row">
            <div class="col-lg-3 col-md-1">
            </div>
            <div class="col-lg-6 col-md-10">
                <div class="card-body card-block">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>

                                <td align="right">รูปภาพสินค้า:</td>
                                <td><input type="file" name="myfile" id="myfile" class="form-control-file border"></td>
                            </tr>
                            <tr>
                                <td align="right">ชื่อสินค้า:</td>
                                <td style="color:#878787;"><input class="form-control" name="product_name" type="text" required ></td>
                            </tr>
                            <tr>
                                <td align="right">หมวดสินค้า:</td>
                                <td style="color:#878787;"> <select name="product_cat_id" type="text" id="product_cat_id" class="form-control">
                                    <?php
                                    $con = connect();
                                    $q = "SELECT * from tbl_product_cat order by product_cat_name ASC ";
                                    $r = $con->query($q) or die ($q);
                                    if($r->num_rows > 0) {
                                        while ($ob = $r->fetch_object()) {
                                            echo "<option value='$ob->product_cat_id' > $ob->product_cat_name </option> ";
                                        }
                                    }
                                    $con->close();
                                    ?>
                                    </select></td>
                            </tr>
                            <tr>
                                <td align="right">ข้อมูลสินค้า:</td>
                                <td style="color:#878787;"><input class="form-control" name="product_details" type="text" required ></td>
                            </tr>
                            <tr>
                                <td align="right">ราคาทุน:</td>
                                <td style="color:#878787;"><input class="form-control" name="unit_cost" type="number" min="0" step="0.01" required ></td>
                            </tr>
                            <tr>
                                <td align="right">ราคาขาย:</td>
                                <td style="color:#878787;"><input class="form-control" name="unit_price" type="number" min="0" step="0.01" required ></td>
                            </tr>
                            <tr>
                                <td align="right">หน่วยนับ:</td>
                                <td style="color:#878787;"><input class="form-control" name="unit_name" type="text" required ></td>
                            </tr>
                            <tr>
                                <td align="right">กำหนดคงคลังอย่างต่ำ:</td>
                                <td style="color:#878787;"><input class="form-control" name="min_stock" type="number" min="0" value="0" required ></td>
                            </tr>
                            <tr>
                                <td align="right">
                                    
                                </td>
                                <td>
                                    <button name="submit" type="submit" class="btn btn-success">
                                        <i class="fa fa-save"></i> บันทึก
                                    </button>
                                    <button type="reset" class="btn btn-secondary btn-sm">
                                        <i class="fa fa-ban"></i> รีเซ็ท
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <span id="show_up" class="text-danger"></span>
                </div>
            </div>
        </div>
    </div>
</form>

<script>

$("#data").submit(function(e) {
    e.preventDefault();
    // Get form
    var form = $('#data')[0];

    // Create an FormData object 
    var data = new FormData(form);
    // get url
    var url = form.action;

    // send request
    $.ajax({
            type: 'POST',
            url: url,
            data: data,
            processData: false,
            contentType: false,
        success: function(data){
            if(data == 'ok') {
                window.location = "?page=main";
            } else {
                $('#show_up').html(data);
            }

        }
    });

});
</script>