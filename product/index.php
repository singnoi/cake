<?php
session_start();
include("../includes/db_connect.php");
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = "main";
}
  
if (isset($_SESSION['user_id'])) {
    $logined = true;
    $user_type_id = $_SESSION['user_type_id'];
    /*
    if($user_type_id != 0 && $user_type_id != 4) {
        header('location:../index.php?page=403');
    }
    */
    switch ($user_type_id) {
        case '1':
            $menu = "../../cake/owner/menu.php";
            break;
        case '2':
            $menu = "../../cake/cashier/menu.php";
            break;
        case '3':
            $menu = "../../cake/account/menu.php";
            break;
        case '4':
            $menu = "../../cake/admin/menu.php";
            break;
        
        default:
            $menu = "../../cake/admin/menu.php";
            break;
    }
} else {
    $logined = false;
    header('location:../index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ระบบบริหารจัดการขายอุปกรณ์วัสดุก่อสร้าง ร้านถวัชชัย</title>
    <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../node_modules//@fortawesome/fontawesome-free/css/all.css">
    <link rel="stylesheet" href="../node_modules/sweetalert2/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="../node_modules/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="../node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="../css/style.css">

    <script src="../node_modules/jquery/dist/jquery.js"></script>
    <script src="../node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="../node_modules/select2/dist/js/select2.min.js"></script>
    <script src="../node_modules/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="../node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    

</head>
<body>

<?php
include '../includes/header.php';
include($page.".php");
?>
<script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

<script>
$('[data-toggle="tooltip"]').tooltip(); 
</script>

</body>
</html>