<?php
$con = connect();
$product_id = $_GET['product_id'];
$q = "SELECT * from tbl_product where product_id = '$product_id'";
$r = $con->query($q) or die ($q);
$ob = $r->fetch_object();
?>
<h3 class="text-center textshadow"> แก้ไขสินค้า </h3>
<form id="data" action="product_edit_action.php" method="post" enctype="multipart/form-data">
<input type="hidden" name="product_id" value="<?php echo $product_id;?>" >
<div class="card">
        <div class="row">
            <div class="col-lg-3 col-md-1">
            </div>
            <div class="col-lg-6 col-md-10">
                <div class="card-body card-block">
                    <table class="table table-borderless">
                        <tbody>
                            <tr>
                                <td align="right">แสดงรูปภาพ:</td>
                                <td><img src="images/<?php echo $ob->product_images;?>" class="rounded" alt="Cinque Terre" width="360" height="245" ></td>
                            </tr>
                            <tr>
                                <td align="right">เปลี่ยนภาพ:</td>
                                <td><input type="file" name="myfile" id="myfile" class="form-control-file border"></td>
                            </tr>
                            <tr>
                                <td align="right">ชื่อสินค้า:</td>
                                <td style="color:#878787;"><input class="form-control" name="product_name" type="text" value="<?php echo $ob->product_name;?>" required ></td>
                            </tr>
                            <tr>
                                <td align="right">หมวดสินค้า:</td>
                                <td style="color:#878787;"> <select name="product_cat_id" type="text" id="product_cat_id" class="form-control">
                                    <?php
                                    $con = connect();
                                    $q1 = "SELECT * from tbl_product_cat order by product_cat_name ASC ";
                                    $r1 = $con->query($q1) or die ($q1);
                                    if($r1->num_rows > 0) {
                                        
                                        while ($ob1 = $r1->fetch_object()) {
                                            if($ob->product_cat_id == $ob1->product_cat_id) $sl = " selected ";
                                            else $sl = "";
                                            echo "<option value='$ob1->product_cat_id' $sl > $ob1->product_cat_name </option> ";
                                        }
                                    }
                                    $con->close();
                                    ?>
                                    </select></td>
                            </tr>
                            <tr>
                                <td align="right">ข้อมูลสินค้า:</td>
                                <td style="color:#878787;"><input class="form-control" name="product_details" type="text" value="<?php echo $ob->product_details;?>"></td>
                            </tr>
                            <tr>
                                <td align="right">ราคาทุน:</td>
                                <td style="color:#878787;"><input class="form-control" name="unit_cost" type="number" min="1" step="0.01" value="<?php echo $ob->unit_cost;?>" required ></td>
                            </tr>
                            <tr>
                                <td align="right">ราคาขาย:</td>
                                <td style="color:#878787;"><input class="form-control" name="unit_price" type="number" min="1" step="0.01" value="<?php echo $ob->unit_price;?>" required ></td>
                            </tr>
                            <tr>
                                <td align="right">หน่วยนับ:</td>
                                <td style="color:#878787;"><input class="form-control" name="unit_name" type="text" value="<?php echo $ob->unit_name;?>" required ></td>
                            </tr>
                            <tr>
                                <td align="right">กำหนดคงคลังอย่างต่ำ:</td>
                                <td style="color:#878787;"><input class="form-control" name="min_stock" type="number" min="0" value="<?php echo $ob->min_stock;?>" required ></td>
                            </tr>
                            <tr>
                                <td align="right">
                                    
                                </td>
                                <td>
                                    <button name="submit" type="submit" class="btn btn-success">
                                        <i class="fa fa-save"></i> บันทึก
                                    </button>
                                    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <span id="show_up" class="text-danger"></span>
                </div>
            </div>
        </div>
    </div>
</form>

<script>

$("#data").submit(function(e) {
    e.preventDefault();
    // Get form
    var form = $('#data')[0];

    // Create an FormData object 
    var data = new FormData(form);
    // get url
    var url = form.action;

    // send request
    $.ajax({
            type: 'POST',
            url: url,
            data: data,
            processData: false,
            contentType: false,
        success: function(data){
            if(data == 'ok') {
                window.location = "?page=main";
            } else {
                $('#show_up').html(data);
            }

        }
    });

});
</script>