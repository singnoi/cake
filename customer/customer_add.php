<h3 class="text-center textshadow"> เพิ่มลูกค้า </h3>

<div class="card">
    <div class="row">
      <div class="col-lg-3 col-md-1">
      </div>
      <div class="col-lg-6 col-md-10">
        <div class="card-body card-block">

        <form id="form_add" method="post">
         
          <table class="table table-borderless">
            <tbody>
              <tr>
                <td align="right">ชื่อ:<code>*</code></td>
                <td style="color:#878787;"><input class="form-control" name="fname" type="text" required ></td>
              </tr>
              <tr>
                <td align="right">นามสกุล:<code>*</code></td>
                <td style="color:#878787;"><input class="form-control" name="lname" type="text"  required ></td>
              </tr>
              <tr>
                <td align="right">ที่อยู่:<code>*</code></td>
                <td style="color:#878787;"><input class="form-control" name="address" type="text" required ></td>
              </tr>
              <tr>
                <td align="right">เบอร์โทรศํพท์:<code>*</code></td>
                <td style="color:#878787;"><input class="form-control" name="tel" type="text"  required ></td>
              </tr>
              <tr>
                <td align="right">อีเมล์:</td>
                <td style="color:#878787;"><input class="form-control" name="email" type="text" ></td>
              </tr>
              
              
              <tr>
                <td></td>
                <td>
                <button name="submit" type="submit" class="btn btn-success btn-sm">
                                            <i class="fa fa-save"></i> บันทึก
                                        </button>
                </td>
              </tr>
            </tbody>
          </table>

         </form>
        </div>
      </div>
    </div>
  </div>
  
<script>

$('#form_add').submit(function(e){
    e.preventDefault();
    //alert("s");
    $.post("customer_add_action.php",$('#form_add').serialize(),function(info){
        if(info == 'ok') {
            window.location = "?page=main";
        } else {
            $('#message').html(info);
        }
    });
});
</script>