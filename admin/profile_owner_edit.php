<?php
//include("./includes/db_connect.php");
$con = connect();
$user_id = $_GET['user_id'];

$q = "SELECT * from tbl_user as u left join tbl_user_type as t on t.user_type_id = u.user_type_id where u.user_id = '$user_id' ";

$r = $con->query($q) or die ($q);
$ob = $r->fetch_object();

?>
<h3 class="text-center textshadow"> แก้ไขข้อมูลเจ้าของกิจการ </h3>

<div class="card">
    <div class="row">
      <div class="col-lg-3 col-md-1">
      </div>
      <div class="col-lg-6 col-md-10">
        <div class="card-body card-block">

        <form id="form_edit" method="post">
            <input type="hidden" name="user_id" value="<?php echo $ob->user_id;?>" >
          <table class="table table-borderless">
            <tbody>
              <tr>
                <td align="right">รหัส: </td>
                <td style="color:#878787;"><?php echo $ob->user_id; ?></td>
              </tr>
              <tr>
                <td align="right">ชื่อ:</td>
                <td style="color:#878787;"><input class="form-control" name="fname" type="text" value="<?php echo $ob->fname; ?>"></td>
              </tr>
              <tr>
                <td align="right">นามสกุล:</td>
                <td style="color:#878787;"><input class="form-control" name="lname" type="text" value="<?php echo $ob->lname; ?>"></td>
              </tr>
              <tr>
                <td align="right">ที่อยู่:</td>
                <td style="color:#878787;"><input class="form-control" name="address" type="text" value="<?php echo $ob->address; ?>"></td>
              </tr>
              <tr>
                <td align="right">เบอร์โทรศํพท์:</td>
                <td style="color:#878787;"><input class="form-control" name="tel" type="text" value="<?php echo $ob->tel; ?>"></td>
              </tr>
              <tr>
                <td align="right">อีเมล์:</td>
                <td style="color:#878787;"><input class="form-control" name="email" type="text" value="<?php echo $ob->email; ?>"></td>
              </tr>
              <tr>
                <td align="right">ชื่อผู้ใช้:</td>
                <td style="color:#878787;"><input class="form-control" name="user_name" type="text" value="<?php echo $ob->user_name; ?>"></td>
              </tr>
              <tr>
                <td align="right">รหัสผ่าน:</td>
                <td style="color:#878787;"><input class="form-control" id="pass_word" type="password" name="pass_word" value="<?php echo $ob->pass_word; ?>" onkeyup="check()"></td>
              </tr>
              <tr>
                <td></td>
                <td style="color:#878787;"><input class="form-control" id="confirm_pass_word" type="password" value="<?php echo $ob->pass_word; ?>" onkeyup="check()"><span id="message"></span></td>
                </td>
              </tr>
              <tr>
                <td align="right">สถานะ:</td>
                <td style="color:#878787;">
                  <?php 
                  
                    echo $ob->user_type_name;
                  
                  ?>
                  </td>
              </tr>
              <tr>
                <td></td>
                <td>
                <button name="submit" type="submit" class="btn btn-success btn-sm">
                                            <i class="fa fa-save"></i> บันทึก
                                        </button>
                </td>
              </tr>
            </tbody>
          </table>

         </form>
        </div>
      </div>
    </div>
  </div>
  
<script>
var check = function () {
    if (document.getElementById('pass_word').value ==
        document.getElementById('confirm_pass_word').value) {
        document.getElementById('message').style.color = 'green';
        document.getElementById('message').innerHTML = 'ถูกต้อง';
    } else {
        document.getElementById('message').style.color = 'red';
        document.getElementById('message').innerHTML = 'กรอกรหัสไม่ตรงกัน';
    }
}

$('#form_edit').submit(function(e){
    e.preventDefault();
    //alert("s");
    $.post("profile_owner_action.php",$('#form_edit').serialize(),function(info){
        if(info == 'ok') {
            window.location = "?page=profile_owner";
        } else {
            $('#message').html(info);
        }
    });
});
</script>