<?php
$con = connect();
$q = "SELECT Count(u.user_id) as c_id FROM tbl_user AS u WHERE u.user_status = 'Y'";
$sum_user = $con->query($q)->fetch_object()->c_id;

$q4 = "SELECT Count(u.staff_id) as c_id FROM tbl_staff AS u WHERE u.staff_status = 'Y'";
$sum_staff = $con->query($q4)->fetch_object()->c_id;

$q2 = "SELECT Count(u.customer_id) as c_id FROM tbl_customer AS u";
$sum_customer = $con->query($q2)->fetch_object()->c_id;

$q3 = "SELECT Count(u.product_id) as c_id FROM tbl_product AS u ";
$sum_product = $con->query($q3)->fetch_object()->c_id;

$con->close();
?>
        <!-- Bootstrap List Group -->
        <ul class="list-group sticky-top sticky-offset">

            <li class="list-group-item sidebar-separator menu-collapsed"></li>
            <!-- Separator with title -->
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
            <span class="fas fa-home fa-fw mr-1"></span>
                <a class="navbar-brand" href="../../cake/"><img src="../../cake/img/logo.png" alt="Logo"></a>
            </li>
            <!-- /END Separator -->
            
            <a href="../../cake/admin/?page=main" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-user fa-fw mr-3"></span>
                    <span class="menu-collapsed">ข้อมูลส่วนตัว</span>
                </div>
            </a>

            <!-- Separator with title -->
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small>ตั้งค่า (ผู้ดูแลระบบ)</small>
            </li>
            <!-- /END Separator -->

            <a href="../../cake/admin/?page=profile_owner" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-user fa-fw mr-3 text-info"></span>
                    <span class="menu-collapsed">เจ้าของกิจการ</span>
                </div>
            </a>

            <a href="../../cake/admin/?page=store" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-warehouse fa-fw mr-3 text-danger"></span>
                    <span class="menu-collapsed">ตั้งค่าข้อมูลร้าน</span>
                </div>
            </a>

            <!-- Separator with title -->
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small>งานบันทึกข้อมูล</small>
            </li>
            <!-- /END Separator -->
            <a href="../../cake/user/" class="bg-dark list-group-item list-group-item-action text-info">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-users-cog fa-fw mr-3"></span>
                    <span class="menu-collapsed">พนักงานฝ่าย  <span class="badge badge-pill badge-primary ml-2"><?php echo comma($sum_user);?></span> </span>
                </div>
            </a>

            <a href="../../cake/staff/" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-users-cog fa-fw mr-3"></span>
                    <span class="menu-collapsed">พนักงานทั่วไป  <span class="badge badge-pill badge-primary ml-2"><?php echo comma($sum_staff);?></span> </span>
                </div>
            </a>
            <a href="../../cake/car/" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-truck-pickup fa-fw mr-3"></span>
                    <span class="menu-collapsed">ข้อมูลรถส่งของ <span class="badge badge-pill badge-primary ml-2"><?php echo comma($sum_customer);?></span></span>
                </div>
            </a>
            

            <a href="../../cake/product/" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-chevron-right fa-fw mr-3"></span>
                    <span class="menu-collapsed">ข้อมูลสินค้า <span class="badge badge-pill badge-primary ml-2"><?php echo comma($sum_product);?></span></span>
                </div>
            </a>
            
            <a href="../../cake/customer/" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-chevron-right fa-fw mr-3"></span>
                    <span class="menu-collapsed">ข้อมูลลูกค้า <span class="badge badge-pill badge-primary ml-2"><?php echo comma($sum_customer);?></span></span>
                </div>
            </a>


             <!-- Separator with title -->
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small>เมนูแยกตามแผนก</small>
            </li>
            <!-- /END Separator -->
            <a href="../../cake/account/" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-chevron-right fa-fw mr-3"></span>
                    <span class="menu-collapsed">ระบบงานบัญชี</span>
                </div>
            </a>
            <a href="../../cake/cashier/" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-chevron-right fa-fw mr-3"></span>
                    <span class="menu-collapsed">ระบบงานขาย </span>
                </div>
            </a>

            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small>เมนูรายงาน</small>
            </li>
            <a href="../../cake/report/" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-tasks fa-fw mr-3"></span>
                    <span class="menu-collapsed">รายงานยอดขาย</span>
                </div>
            </a>
            <!-- Separator without title -->
            <li class="list-group-item sidebar-separator menu-collapsed"></li>

            
        </ul>
     