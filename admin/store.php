<?php
//include("./includes/db_connect.php");
$con = connect();

$q = "SELECT * from tbl_store  ";

$r = $con->query($q) or die ($q);

if($r->num_rows > 0 ) {
    $ob = $r->fetch_object();
    $store_name = $ob->store_name;
    $store_address = $ob->store_address;
    $store_tel = $ob->store_tel;
} else {
    $store_name = "";
    $store_address = "";
    $store_tel = "";
}
?>
<h3 class="text-center textshadow"> ตั้งค่าข้อมูลร้าน</h3>

<div class="card">
    <div class="row">
      <div class="col-lg-3 col-md-1">
      </div>
      <div class="col-lg-6 col-md-10">
        <div class="card-body card-block">

        <form id="form_store" method="post">
            
          <table class="table table-borderless">
            <tbody>
           
              <tr>
                <td align="right">ชื่อร้าน:</td>
                <td style="color:#878787;"><input class="form-control" name="store_name" type="text" value="<?php echo $store_name; ?>" required ></td>
              </tr>
              <tr>
                <td align="right">ที่อยู่:</td>
                <td style="color:#878787;"><textarea class="form-control" rows="5" id="store_address" name="store_address" required ><?php echo $store_address;?></textarea></td>
              </tr>
              <tr>
                <td align="right">เบอร์โทร:</td>
                <td style="color:#878787;"><input class="form-control" name="store_tel" type="text" value="<?php echo $store_tel; ?>" required ></td>
              </tr>
              
              <tr>
                <td></td>
                <td>
                <button name="submit" type="submit" class="btn btn-success btn-sm">
                                            <i class="fa fa-save"></i> บันทึก
                                        </button>
                 <span class="text-success ml-5" id="message"></span>
                </td>
              </tr>
            </tbody>
          </table>

         </form>
         
        </div>
      </div>
    </div>
  </div>
  
<script>

$('#form_store').submit(function(e){
    e.preventDefault();
    //alert("s");
    $.post("store_action.php",$('#form_store').serialize(),function(info){
        if(info == 'ok') {
            $('#message').html(" บันทึกสำเร็จ ");
            //window.location = "?page=store";
        } else {
            $('#message').html(info);
        }
    });
});
</script>