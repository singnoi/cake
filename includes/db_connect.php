<?php
// ฟังก์ชันสำหรับเชื่อมต่อกับฐานข้อมูล
function connect()
{
  // เริ่มต้นส่วนกำหนดการเชิ่อมต่อฐานข้อมูล //
	 $db_config=array(
		"host"=>"127.0.0.1",  // กำหนด host
		"user"=>"root", // กำหนดชื่อ user
		"pass"=>"66396639",   // กำหนดรหัสผ่าน
		"dbname"=>"cake_db",  // cars
		"charset"=>"utf8"  // กำหนด charset
	);
  // สิ้นสุดดส่วนกำหนดการเชิ่อมต่อฐานข้อมูล //
	$con = @new mysqli($db_config["host"], $db_config["user"], $db_config["pass"], $db_config["dbname"]);
	if(mysqli_connect_error()) {
		die('Connect Error (' . mysqli_connect_errno() . ') '. mysqli_connect_error());
		exit;
	}
	if(!$con->set_charset($db_config["charset"])) { // เปลี่ยน charset เป้น utf8 พร้อมตรวจสอบการเปลี่ยน
	//    printf("Error loading character set utf8: %sn", $con->error);  // ถ้าเปลี่ยนไม่ได้
	}else{
	//    printf("Current character set: %sn", $con->character_set_name()); // ถ้าเปลี่ยนได้
	}
	return $con;
	//echo $con->character_set_name();  // แสดง charset เอา comment ออก
	//echo 'Success... ' . $con->host_info . "n";
	//$con->close();
}
//	  ฟังก์ชันสำหรับคิวรี่คำสั่ง sql
function query($sql)
{
	global $con;
	if($con->query($sql)) { return true; }
	else { die("SQL Error: <br>".$sql."<br>".$con->error); return false; }
}
//    ฟังก์ชัน select ข้อมูลในฐานข้อมูลมาแสดง
function select($sql)
{
	global $con;
	$result=array();
	$res = $con->query($sql) or die("SQL Error: <br>".$sql."<br>".$con->error);
	while($data= $res->fetch_assoc()) {
		$result[]=$data;
	}
	return $result;
}
//    ฟังก์ชันสำหรับการ insert ข้อมูล
function insert($table,$data)
{
	global $con;
	$fields=""; $values="";
	$i=1;
	foreach($data as $key=>$val)
	{
		if($i!=1) { $fields.=", "; $values.=", "; }
		$fields.="$key";
		$values.="'$val'";
		$i++;
	}
	$sql = "INSERT INTO $table ($fields) VALUES ($values)";
	if($con->query($sql)) { return true; }
	else { die("SQL Error: <br>".$sql."<br>".$con->error); return false; }
}
//    ฟังก์ชันสำหรับการ update ข้อมูล
function update($table,$data,$where)
{
	global $con;
	$modifs="";
	$i=1;
	foreach($data as $key=>$val)
	{
		if($i!=1){ $modifs.=", "; }
		if(is_numeric($val)) { $modifs.=$key.'='.$val; }
		else { $modifs.=$key.' = "'.$val.'"'; }
		$i++;
	}
	$sql = ("UPDATE $table SET $modifs WHERE $where");
	if($con->query($sql)) { return true; }
	else { die("SQL Error: <br>".$sql."<br>".$con->error); return false; }
}
//    ฟังก์ชันสำหรับการ delete ข้อมูล
function delete($table, $where)
{
	global $con;
	$sql = "DELETE FROM $table WHERE $where";
	if($con->query($sql)) { return true; }
	else { die("SQL Error: <br>".$sql."<br>".$con->error); return false; }
}
//    ฟังก์ชันสำหรับแสดงรายการฟิลด์ในตาราง
function listfield($table)
{
	global $con;
	$sql="SELECT * FROM $table LIMIT 1 ";
	$row_title="\$data=array(<br/>";
	$res = $con->query($sql) or die("SQL Error: <br>".$sql."<br>".$con->error);
	$i=1;
	while($data= $res->fetch_field()) {
		$var=$data->name;
		$row_title.="\"$var\"=>\"value$i\",<br/>";
		$i++;
	}
	$row_title.=");<br/>";
	echo $row_title;
}

date_default_timezone_set('Asia/Bangkok');
$dtz = new DateTimeZone("Asia/Bangkok");
$now = new DateTime(date("Y-m-d  H:i:s"), $dtz);
$today = $now->format("Y-m-d H:i:s");
$time_now = $now->format("H:i");
$today_date = $now->format("Y-m-d");
$month_now = $now->format("m");
$year_now = $now->format("Y");
$year_now_thai = $year_now+543;

function year_eng_to_date($strDate){
    $arr = array();
    $arr = explode('/', $strDate);
    $strYear=$arr[2];
    $newDate = $strYear.'-'.$arr[1].'-'.$arr[0];
    return "$newDate";
}
function date_to_input($strDate){
    $strYear = date("Y",strtotime($strDate));
    $strMonth= date("m",strtotime($strDate));
    $strDay= date("d",strtotime($strDate));
    return "$strDay/$strMonth/$strYear";
}
$thai_day_arr=array("อาทิตย์","จันทร์","อังคาร","พุธ","พฤหัสบดี","ศุกร์","เสาร์");
$thai_month_arr=array(
    "0"=>"",
    "1"=>"มกราคม",
    "2"=>"กุมภาพันธ์",
    "3"=>"มีนาคม",
    "4"=>"เมษายน",
    "5"=>"พฤษภาคม",
    "6"=>"มิถุนายน",
    "7"=>"กรกฎาคม",
    "8"=>"สิงหาคม",
    "9"=>"กันยายน",
    "10"=>"ตุลาคม",
    "11"=>"พฤศจิกายน",
    "12"=>"ธันวาคม"
);
function date_thai_full_time($time){
    global $thai_day_arr,$thai_month_arr;
    $thai_date_return="วัน".$thai_day_arr[date("w",strtotime($time))];
    $thai_date_return.= "ที่ ".date("j",strtotime($time));
    $thai_date_return.=" เดือน ".$thai_month_arr[date("n",strtotime($time))];
    $thai_date_return.= " พ.ศ.".(date("Y",strtotime($time))+543);
    $thai_date_return.= "  ".date("H:i",strtotime($time))." น.";
    return $thai_date_return;
}
function date_thai_full($time){
    global $thai_day_arr,$thai_month_arr;
    $thai_date_return= "วันที่  &nbsp;".date("j",strtotime("$time"));
    $thai_date_return.="  &nbsp;".$thai_month_arr[date("n",strtotime("$time"))];
    $thai_date_return.= "&nbsp;".(date("Y",strtotime("$time"))+543);
    return $thai_date_return;
}
function date_thai($strDate){
	if($strDate!='' && $strDate!='0000-00-00') {
		  $strYear = date("Y",strtotime($strDate))+543;
		  $strMonth= date("n",strtotime($strDate));
		  $strDay= date("j",strtotime($strDate));
		  $strHour= date("H",strtotime($strDate));
		  $strMinute= date("i",strtotime($strDate));
		  $strSeconds= date("s",strtotime($strDate));
		  $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
		  $strMonthThai=$strMonthCut[$strMonth];
		  return "$strDay $strMonthThai $strYear";
	} else {
	  return "ไม่มีวันที่";
	}
}

function date_thai_time($strDate){
	if($strDate!='' && $strDate!='0000-00-00') {
		  $strYear = date("Y",strtotime($strDate))+543;
		  $strMonth= date("n",strtotime($strDate));
		  $strDay= date("j",strtotime($strDate));
		  $strHour= date("H",strtotime($strDate));
		  $strMinute= date("i",strtotime($strDate));
		  $strSeconds= date("s",strtotime($strDate));
		  $strMonthCut = Array("","ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
			$strMonthThai=$strMonthCut[$strMonth];
		  return "$strDay $strMonthThai $strYear ".$strHour.".".$strMinute." น.";
	} else {
	  return "ไม่มีวันที่";
	}
}
function money($i) {
    return (empty($i)) ? 0.00 : number_format($i,2);
}
function comma($i) {
    return (empty($i)) ? 0 : number_format($i,0);
}

function money_a($i) {
    return (empty($i) || $i == 0) ? '' : number_format($i,2);
}
function comma_a($i) {
    return (empty($i) || $i == 0) ? '' : number_format($i,0);
}

function DateDiff($strDate1,$strDate2){
  return (strtotime($strDate2) - strtotime($strDate1))/  ( 60 * 60 * 24 );  // 1 day = 60*60*24
}
function TimeDiff($strTime1,$strTime2){
  return (strtotime($strTime2) - strtotime($strTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}
function DateTimeDiff($strDateTime1,$strDateTime2){
  return (strtotime($strDateTime2) - strtotime($strDateTime1))/  ( 60 * 60 ); // 1 Hour =  60*60
}


function ThaiBaht($amount_number){
	if($amount_number!=""){
	  $amount_number = number_format($amount_number, 2, ".","");
	  //echo "<br/>amount = " . $amount_number . "<br/>";
	  $pt = strpos($amount_number , ".");
	  $number = $fraction = "";
	  if ($pt === false)
		  $number = $amount_number;
	  else
	  {
		  $number = substr($amount_number, 0, $pt);
		  $fraction = substr($amount_number, $pt + 1);
	  }
  
	  //list($number, $fraction) = explode(".", $number);
	  $ret = "";
	  $baht = ReadNumber ($number);
	  if ($baht != "")
		  $ret .= $baht . "บาท";
  
	  $satang = ReadNumber($fraction);
	  if ($satang != "")
		  $ret .=  $satang . "สตางค์";
	  else
		  $ret .= "ถ้วน";
	  //return iconv("UTF-8", "TIS-620", $ret);
	  return $ret;
	}
  
  }
  function ReadNumber($number)
  {
	  $position_call = array("แสน", "หมื่น", "พัน", "ร้อย", "สิบ", "");
	  $number_call = array("", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า");
	  $number = $number + 0;
	  $ret = "";
	  if ($number == 0) return $ret;
	  if ($number > 1000000)
	  {
		  $ret .= ReadNumber(intval($number / 1000000)) . "ล้าน";
		  $number = intval(fmod($number, 1000000));
	  }
  
	  $divider = 100000;
	  $pos = 0;
	  while($number > 0)
	  {
		  $d = intval($number / $divider);
		  $ret .= (($divider == 10) && ($d == 2)) ? "ยี่" :
			  ((($divider == 10) && ($d == 1)) ? "" :
			  ((($divider == 1) && ($d == 1) && ($ret != "")) ? "เอ็ด" : $number_call[$d]));
		  $ret .= ($d ? $position_call[$pos] : "");
		  $number = $number % $divider;
		  $divider = $divider / 10;
		  $pos++;
	  }
	  return $ret;
  }
  
function show_status($status)
{
	switch ($status) {
		case 'Y':
			$show = "บันทึกแล้ว";
			break;
		case 'N':
			$show = "ถูกยกเลิก";
			break;
		case 'W':
			$show = "รอบันทึก";
			break;		
		default:
		$show = "รอบันทึก";
			break;
	}
	return $show;
}
?>
