
    <nav class="navbar navbar-expand-lg sticky-top navbar-dark bg-secondary justify-content-between">
        <a class="navbar-brand" href="../../cake/">
          <img src="../../cake/img/home.png" class="d-inline-block align-middle  shadow " alt="LOGO" width="30" height="30" >
  
          <span class="kanit_l textshadow " > ระบบบริหารจัดการขายอุปกรณ์วัสดุก่อสร้าง </span>
        </a>
          <!-- Links -->
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        </ul>
       
   
            <?php if($logined) { ?>

 
               
                     
                <a class="my-2 my-sm-0 text-white mr-3" href="../../cake/index.php?page=profile" data-toggle="tooltip" title="<?php echo $_SESSION['user_fullname']." (".$_SESSION['user_type_name'].")"; ?>" data-placement="bottom" > 
                <img src="../../cake/img/admin.jpg" class="d-inline-block align-middle rounded-circle shadow " alt="user" width="30" height="30" >
                <?php echo $_SESSION['user_fullname']." (".$_SESSION['user_type_name'].")"; ?>
                </a>
                
                <a class="my-2 my-sm-0 btn btn-danger " onclick="confirm_out();" id="bt_logout" href="#" data-toggle="tooltip" title="ออกจากระบบ" > <i class="fas fa-power-off"></i></a>
          
            <?php } ?>
                
       
    </nav>
    <script>
      function confirm_out() {

        Swal.fire({
          title: "ต้องการออกจากระบบใช่ใหม?",
          text: "ควรออกจากระบบทุกครั้ง เมื่อเลิกใช้งาน",
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#f46b02',
          cancelButtonColor: '#e0dcd9',
          confirmButtonText: "ออกจากระบบ",
        }).then((result) => {
          if (result.value) {
            window.location = '../../cake/logout.php';
          }
        })

      }
      </script>


                    
