<?php
//include("../includes/db_connect.php");
$con = connect();
$user_fullname = $_SESSION['user_fullname'];
$edit = false;

if(isset($_SESSION['stock_code'])) {
    $stock_code = $_SESSION['stock_code'];
    $edit = true;
}
if( isset($_GET['stock_code'])) {
    $stock_code = $_GET['stock_code'];
    $_SESSION['stock_code'] = $stock_code;
    $edit = true;
}

if($edit){
    $qo = "SELECT *,s.user_id from tbl_stock as s left join tbl_user as u on u.user_id = s.user_id where s.stock_code = '$stock_code' limit 1";
    $ro = $con->query($qo) or die ($qo);
    $ob = $ro->fetch_object();
    $user_fullname = $ob->fname." ".$ob->lname;
    $user_id = $ob->user_id;
    $stock_date = $ob->stock_date;

    // ถ้าเป็นชื่อใน admin
    if($user_id == 2000) {
        $qm = "SELECT * from tbl_admin where user_id = '$user_id'";
        $rm = $con->query($qm) or die ($qm);
        $obm = $rm->fetch_object();
        $user_fullname = $obm->fname." ".$obm->lname;
    }

} else {

    // ----- ออกเลขที่ stock_code
    $qo = "SELECT stock_code from tbl_stock order by stock_code DESC limit 1";
    $ro = $con->query($qo) or die ($qo);
    if( $ro->num_rows > 0 ) {
        $max_code =  $ro->fetch_object()->stock_code;
        $code_arr = explode("-",$max_code);
        $m = substr($code_arr[0],-2);
        if($m == $month_now) {
            $next_code = intval($code_arr[1]) + 1;
        } else {
            $next_code = 1;
        }
        
    } else {
        $next_code = 1;
    }
    $stock_code = "ST".substr($year_now_thai,-2).$month_now."-".sprintf("%04d",$next_code);
    $user_id = $_SESSION['user_id'];
    $user_fullname = $_SESSION['user_fullname'];
    $stock_date = $today_date;

// --------------
}
?>
<div class="container-fluid mt-3">
    <div class="row">
        <div class="col-lg-2">
            <a href="?page=main" class="btn btn-outline-success mb-2" > <i class="fas fa-home"></i> กลับหน้าหลัก สินค้าคงเหลือ</a> 
        </div>
        <div class="col-lg-10">
            <h4 class="text-center">ตรวจนับสินค้าคงเหลือ</h4>
        </div>
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                <form class="form-inline" id="form_date">
                    <label for="email" class="mr-sm-2">บันทึก ณ วันที่:</label>
                    <input type="date" class="form-control mb-2 mr-sm-2" id="stock_date" name="stock_date" value="<?php echo $stock_date;?>" required >
                    <label for="stock_code" class="mr-sm-2">เลขที่ใบตรวจสินค้าคงเหลือ:</label>
                    <input type="text" class="form-control mb-2 mr-sm-2" id="stock_code" name="stock_code" value="<?php echo $stock_code;?>" readonly >
                    <label for="stock_code" class="mr-sm-2">ผู้บันทึก:</label>
                    <input type="text" class="form-control mb-2 mr-sm-2" id="user_name" value="<?php echo $user_fullname;?>" disabled >
                    <input type="hidden" name="user_id" value="<?php echo $user_id;?>" >
                    <button type="submit" class="btn btn-primary mb-2"><i class="fas fa-plus"></i> เริ่มบันทึกรายการ</button>
                    <span id="show_s"></span>
                </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
if($edit) {
?>
<div class="container-fluid my-3">
    <div class="row">
        <div class="col-lg-12">
        <table class="table table-sm table-striped table-bordered">
            <thead class="thead-light">
                <tr>
                    <th width="30" class='text-center'>ลำดับ</th>
                    <th width="70">รหัส</th>
                    <th>สินค้า</th>
                    <th width="70">หน่วยนับ</th>
                    <th width="110" class="text-right">จำนวนในระบบ</th>
                    <th width="100" class="text-right">คงเหลือจริง</th>
                    <th class='text-danger'>หมายเหตุ</th>
                    <th width="50" class='text-center'>#</th>
                </tr>
            </thead>
            <tbody>
            <form id="form_list">
                <tr>
                    <td class='text-center'><i class="fas fa-plus"></i></td>
                    <td colspan="2">
                    <select class="form-control select2" id="product_id" name="product_id" style="width:100%" onchange="show_val()" >
                        <option value=""></option>
                        <?php
                        
                        $q = "SELECT * from tbl_product where product_id not in ( SELECT product_id from tbl_stock_list where stock_code = '$stock_code' )  order by product_id ASC";
                        $r = $con->query($q) or die ($q);
                        if($r->num_rows > 0) {
                            while ($ob = $r->fetch_object()) {
                                echo "<option value='$ob->product_id' > (".$ob->product_id.") ".$ob->product_name." </option> ";
                            }
                        }
                        
                        ?>
                    </select>
                    </td>
                    <td class="text-center" id="unit"></td>
                    <td class="text-right" >
                    <input type="text" id="stock_qty" name="stock_qty" value="0" class="form-control text-right" readonly >
                    </td>
                    <td><input type="number" min="0" name="cut_stock_qty" id="cut_stock_qty" value="" class="form-control text-right" required ></td>
                    <td><input type="text" name="stock_note" id="stock_note" value="" class="form-control text-danger"  ></td>
                    <td class='text-center'> <button type="submit" class="btn btn-sm btn-info" > <i class="fas fa-save"></i> </button> </td>
                </tr>
            </form>
            <?php 
            $qx = "SELECT * from tbl_stock_list as l left join tbl_product as p on p.product_id = l.product_id where stock_code = '$stock_code' ";
            $rx = $con->query($qx) or die ($qx);
            if($rx->num_rows > 0) {
                $i = 0;
                while($obx = $rx->fetch_object()){
                    $i++;
                    echo "<tr>";
                    echo "<td class='text-center'> $i </td>";
                    echo "<td> $obx->product_id </td>";
                    echo "<td> $obx->product_name </td>";
                    echo "<td class='text-center'> $obx->unit_name </td>";
                    echo "<td class='text-right'>".comma($obx->stock_qty)."</td>";
                    echo "<td class='text-right'>".comma($obx->cut_stock_qty)."</td>";
                    echo "<td class='text-danger'> $obx->stock_note </td>";
                    echo "<td class='text-center'>";
                    ?>
                    <a href="#" onclick="del_list('<?php echo $obx->stock_list_id;?>');" class="text-danger" > <i class="far fa-trash-alt"></i> </a>
                    <?php 
                    echo "</td>";
                    echo "</tr>";
                }

                echo "<tr>";
                echo "<td colspan='8' class='text-center'>";
                ?>
                <button type="button" class="btn btn-success" onclick="finish_add();" > <i class="fas fa-save"></i> บันทึกการตรวจสินค้าคงเหลือ </button>
                <?php 
                echo "</td>";
                echo "</tr>";
            }
            ?>
            </tbody>
        </table>
        <span id="show_err"></span>

        </div>
    </div>
</div>

<?php 
} // end edit

?>

<script>
function del_list(id) {
    Swal.fire({
        title: 'ลบรายการ ?',
        text: "กดยืนยันหากต้องการลบ",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.post("stock_add_del.php",{stock_list_id: id},function(info){
                if(info == 'ok') {
                    window.location = "?page=stock_add";
                } else {
                    alert(info);
                }
            });
        }
    });    
}
function finish_add(){
    Swal.fire({
        title: 'บันทึกสำเร็จ',
        text: "",
        type: 'cussess',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            window.location = "?page=stock";
        }
    });    
}

$('#form_date').submit(function(e){
    e.preventDefault();
    //alert("sss");
    var stock_code = $('#stock_code').val();
    $.post("stock_action.php",$('#form_date').serialize(),function(info){
        if(info=='ok'){
            window.location = "?page=stock_add&stock_code="+stock_code;
        } else {
            $('#show_s').html(info);
        }
    });
});

$('#form_list').submit(function(e){
    e.preventDefault();
    var stock_qty = $('#stock_qty').val();
    var cut_stock_qty = $('#cut_stock_qty').val();
    var stock_note = $('#stock_note').val();
    if(stock_note == '') {
        if(stock_qty != cut_stock_qty) {
            alert("ยอดคงเหลือไม่ตรงกัน จำเป็นต้องชี้แจงในช่องหมายเหตุ !");
            $('#stock_note').focus();
            return false;
        }
    }

    var stock_code = $('#stock_code').val();
    $.post("stock_add_action.php",$('#form_list').serialize(),function(info){
        if(info=='ok'){
            window.location = "?page=stock_add&stock_code="+stock_code;
        } else {
            $('#show_s').html(info);
        }
    });
});

$('.select2').select2({ 
    theme: "bootstrap4"
});

function show_val() {
    var product_id = $('#product_id').val();
    if(product_id == ''){
        $('#unit').html("");
        $('#stock_qty').val("");
    } else {
    //alert(product_id);
        $.post("stock_js.php",{product_id: product_id},function(info){
            //$('#show_err').html(info);
            var obj = JSON.parse(info);
            $('#unit').html(obj.unit_name);
            $('#stock_qty').val(obj.sum_stock);
        });
    }
}
</script>