<?php
//include("../includes/db_connect.php");
$con = connect();
if( isset($_GET['set_date'])) {
    $set_date = $_GET['set_date'];

} else {
    $set_date = $today_date;
}
if(isset($_SESSION['stock_code'])) unset($_SESSION['stock_code']);
$wherex = "";

if(isset($_GET['stock'])) {
    if($_GET['stock']=='full') {
        $wherex = " WHERE ( IFNULL((SELECT SUM(t2.transaction_qty) FROM tbl_transaction as t2 WHERE t2.product_id = e.product_id AND t2.transaction_id >= IFNULL(ts.transaction_id,0) and t2.transaction_date <= '$set_date' ),0) + IFNULL(ts2.cut_stock_qty,0) ) > e.min_stock ";
    }
    if($_GET['stock']=='min') {
        $wherex = " WHERE ( IFNULL((SELECT SUM(t2.transaction_qty) FROM tbl_transaction as t2 WHERE t2.product_id = e.product_id AND t2.transaction_id >= IFNULL(ts.transaction_id,0) and t2.transaction_date <= '$set_date' ),0) + IFNULL(ts2.cut_stock_qty,0) )  <= e.min_stock AND e.min_stock <> 0 AND ( IFNULL((SELECT SUM(t2.transaction_qty) FROM tbl_transaction as t2 WHERE t2.product_id = e.product_id AND t2.transaction_id >= IFNULL(ts.transaction_id,0) and t2.transaction_date <= '$set_date' ),0) + IFNULL(ts2.cut_stock_qty,0) ) <> 0 ";
    }
    if($_GET['stock']=='emtry') {
        $wherex = " WHERE ( IFNULL((SELECT SUM(t2.transaction_qty) FROM tbl_transaction as t2 WHERE t2.product_id = e.product_id AND t2.transaction_id >= IFNULL(ts.transaction_id,0) and t2.transaction_date <= '$set_date' ),0) + IFNULL(ts2.cut_stock_qty,0) ) < 1 ";
    }
}

$sql = "SELECT
e.product_id,
e.product_cat_id,
ts.transaction_id,
ts2.transaction_id,
( IFNULL((SELECT SUM(t2.transaction_qty) FROM tbl_transaction as t2 WHERE t2.product_id = e.product_id AND t2.transaction_id >= IFNULL(ts.transaction_id,0) and t2.transaction_date <= '$set_date' ),0) + IFNULL(ts2.cut_stock_qty,0) )  AS sum_stock,
e.product_name,
e.product_details,
e.unit_cost,
e.unit_price,
e.unit_name,
e.product_images,
c.product_cat_name,
e.min_stock 
FROM
tbl_product AS e
LEFT JOIN tbl_product_cat AS c ON c.product_cat_id = e.product_cat_id
LEFT OUTER JOIN 
(SELECT t.product_id, max(t.transaction_id) as transaction_id  FROM tbl_transaction as t WHERE  t.stock_type = 'Y' and t.transaction_date <= '$set_date' GROUP BY t.product_id  ) as ts
ON ts.product_id = e.product_id
LEFT OUTER JOIN
tbl_transaction as ts2 ON ts2.transaction_id = ts.transaction_id
$wherex 
ORDER BY
e.product_id ASC ";

//echo $sql;

$r = $con->query($sql) or die ($sql);

?>
<!-- <h3 class="text-center textshadow"> ข้อมูลสินค้า </h3> -->
<!-- <hr> -->
<div class="container-fluid my-3">
    <div class="row">
        <div class="col-lg-12">
        
        
<span> 
<a href="../?page=main" class="btn btn-outline-success mb-2" > <i class="fas fa-home"></i> กลับหน้า โปรแกรมหลัก </a> 
<a href="?page=stock_add" class="btn btn-secondary mb-2" > <i class="fas fa-tools"></i> บันทึกยอดสินค้าคงเหลือ (นับสต๊อก) </a> 
<a href="?page=stock" class="btn btn-secondary mb-2" > <i class="fas fa-list"></i> ประวัติรายการตรวจยอดสินค้าคงเหลือ </a> 
</span>

<div class="card">
    <div class="card-body">
<?php 

$sql1 = "SELECT
COUNT(e.product_id) as totals,
(SELECT COUNT(e1.product_id) FROM tbl_product as e1 where min_stock < (SELECT IFNULL(SUM(t2.transaction_qty),0)+IFNULL(ts.cut_stock_qty,0) FROM tbl_transaction as t2 WHERE t2.product_id = e1.product_id AND t2.transaction_id > IFNULL(ts.transaction_id,0) and t2.transaction_date <= '$set_date' ) ) as sum_full,

(SELECT COUNT(e1.product_id) FROM tbl_product as e1 where min_stock <> 0 AND min_stock >= (SELECT SUM(t2.transaction_qty)+IFNULL(ts.cut_stock_qty,0) FROM tbl_transaction as t2 WHERE t2.product_id = e1.product_id AND t2.transaction_id >= IFNULL(ts.transaction_id,0) and t2.transaction_date <= '$set_date' ) ) as sum_min,

(SELECT COUNT(e1.product_id) FROM tbl_product as e1 where (SELECT SUM(t2.transaction_qty)+IFNULL(ts.cut_stock_qty,0) FROM tbl_transaction as t2 WHERE t2.product_id = e1.product_id AND t2.transaction_id > IFNULL(ts.transaction_id,0) and t2.transaction_date <= '$set_date' ) is NULL ||   (SELECT SUM(t2.transaction_qty)+IFNULL(ts.cut_stock_qty,0) FROM tbl_transaction as t2 WHERE t2.product_id = e1.product_id AND t2.transaction_id > IFNULL(ts.transaction_id,0) and t2.transaction_date <= '$set_date' ) = 0) as sum_emtry
FROM
tbl_product AS e
LEFT JOIN tbl_product_cat AS c ON c.product_cat_id = e.product_cat_id
LEFT OUTER JOIN 
(SELECT t.product_id,t.transaction_id,t.cut_stock_qty FROM tbl_transaction as t WHERE  t.transaction_code = '0' and t.transaction_date <= '$set_date' ORDER BY t.transaction_date DESC LIMIT 1 ) as ts
ON ts.product_id = e.product_id
 ";
echo $sql1;

$r1 = $con->query($sql1) or die ($sql1);
$ob1 = $r1->fetch_object();
?>
        <h5 class="card-title">จำนวนรายการ สินค้าทั้งหมด: <ins><a href="?page=main">  &nbsp;<?php echo comma($ob1->totals);?> &nbsp; </a></ins>   &nbsp;&nbsp; สินค้าพร้อมจำหน่าย: <ins><a href="?page=main&stock=full">  &nbsp;<?php echo comma($ob1->sum_full);?> &nbsp; </a></ins>   &nbsp;&nbsp; <a href="?page=main&stock=min" class="text-info"> สินค้าเหลือน้อย: <ins>  &nbsp;<?php echo comma($ob1->sum_min);?> &nbsp; </ins></a>   &nbsp;&nbsp; <a href="?page=main&stock=emtry" class="text-danger">สินค้าหมด: <ins> &nbsp; <?php echo comma($ob1->sum_emtry);?> &nbsp; </ins></a> </h5>
    </div>
</div>

<table id="tb1" class="table table-striped table-bordered table-sm">
                        <thead>
                            <tr>
                                <th>รหัสสินค้า</th>
                                <th class='text-center'>รูปภาพ</th>
                                <th>ชื่อสินค้า</th>
                                <th>ชื่อหมวด</th>
                                <th>คำอธิบาย</th>
                                <th class='text-center'>หน่วยนับ</th>
                                <th class='text-right'>เกณฑ์คงคลัง</th>
                                <th class='text-right'>จำนวนคงเหลือ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        if($r->num_rows > 0 ) {
                            while ($result = $r->fetch_object()) {
                                $cl = "";
                                if($result->sum_stock == NULL || $result->sum_stock == 0) {
                                    $cl = " class='text-danger' ";
                                } else {
                              
                                    if($result->sum_stock < $result->min_stock) {
                                        $cl = " class='text-info' ";
                                    }

                                }

                                if($result->product_images == NULL || $result->product_images == "") {
                                    $img = "../../cake/product/images/noimage89.png";
                                } else {
                                    $img = "../../cake/product/images/".$result->product_images;
                                }
                                ?>
                                <tr <?php echo $cl;?> >
                                    <td><?php echo $result->product_id; ?></td>
                                    <td class='text-center'>
                                    <img src="<?php echo $img;?>" class="img-thumbnail img-fluid" alt="<?php echo $result->product_name; ?>"  width="60" height="34" >
                                    </td>
                                    <td><?php echo $result->product_name; ?></td>
                                    <td><?php echo $result->product_cat_name; ?></td>
                                    <td><?php echo $result->product_details; ?></td>
                                    <td class='text-center'><?php echo $result->unit_name; ?></td>
                                    <td class='text-right'><?php echo comma($result->min_stock);?></td>
                                    <th class='text-right'><?php echo comma($result->sum_stock);?></th>
                                </tr>
                            <?php
                        }
                    }
                        $con->close();
                        ?>
                        </tbody>
                    </table>


        </div>
    </div>
</div>

<script>

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีรายการ",
        "sSearch": "ค้นหารายการสินค้า :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [0, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": true,
    //"info": false,
    //"ordering": false,
    //"searching": false,
    //"paging":  false

});

function del_product(p_id) {
    Swal.fire({
        title: 'ลบสินค้า ?',
        text: "กดยืนยันหากต้องการลบ",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.post("product_del.php",{product_id: p_id},function(info){
                if(info == 'ok') {
                    window.location = "?page=main";
                } else {
                    alert(info);
                }
            });
        }
    });
}

</script>                    