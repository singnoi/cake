<?php
//include("../includes/db_connect.php");
$con = connect();
$user_type_id = $_SESSION['user_type_id'];
if( isset($_SESSION['stock_code'])) unset($_SESSION['stock_code']);

$sql = "SELECT *,e.user_id,
( SELECT count(l2.stock_list_id) from tbl_stock_list as l2 where l2.stock_code = e.stock_code and l2.stock_qty <> l2.cut_stock_qty  ) as sum_cut,
( SELECT count(l2.stock_list_id) from tbl_stock_list as l2 where l2.stock_code = e.stock_code ) as sum_list
FROM tbl_stock as e 
LEFT OUTER JOIN tbl_user as s on s.user_id = e.user_id
ORDER BY e.stock_code DESC ";

$r = $con->query($sql) or die ($sql);

?>
<div class="container-fluid mt-3">
    <div class="row">

        <div class="col-lg-12">
        <span> 
            <a href="?page=main" class="btn btn-outline-success mb-2" > <i class="fas fa-home"></i> กลับหน้าหลัก สินค้าคงเหลือ</a> 
            <a href="?page=stock_add" class="btn btn-secondary mb-2" > <i class="fas fa-tools"></i> บันทึกยอดสินค้าคงเหลือ (นับสต๊อก) </a> 
            <a href="?page=stock" class="btn btn-secondary mb-2" > <i class="fas fa-list"></i> ประวัติรายการตรวจยอดสินค้าคงเหลือ </a> 
            </span>
        </div>
        <div class="col-lg-12">
        
      

<h4 class="text-center textshadow"> ประวัติรายการตรวจยอดสินค้าคงเหลือ </h4>

<div class="table-responsive">
<table id="tb1" class="table table-striped table-bordered table-sm ">
                        <thead>
                            <tr>
                                <th>เลขที่</th>
                                <th>วันที่</th>
                                <th>ชื่อผู้ตรวจ</th>
                                <th class='text-right'>จำนวนรายการ</th>
                                <th class='text-right'>รายการคงเหลือไม่ตรง</th>
                                <th class='text-center'>จัดการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        if($r->num_rows > 0 ) {
                            while ($result = $r->fetch_object()) {
                                $user_fullname =$result->fname." ".$result->lname;
                                if($result->user_id == 2000) {
                                    $qm = "SELECT * from tbl_admin limit 1";
                                    $rm = $con->query($qm) or die ($qm);
                                    $obm = $rm->fetch_object();
                                    $user_fullname = $obm->fname." ".$obm->lname;
                                }
                                ?>
                                <tr>
                                    <td><?php echo $result->stock_code; ?></td>
                                    <td><?php echo date_thai($result->stock_date); ?></td>
                                    <td><?php echo $user_fullname; ?></td>
                                    <td class='text-right'><?php echo comma($result->sum_list); ?></td>
                                    <td class='text-right'><?php echo comma($result->sum_cut); ?></td>
                                    <td class='text-center'>
                                        <a href="?page=stock_add&stock_code=<?php echo $result->stock_code;?>" class="text-info"><i class="fas fa-file-alt"></i></a>
                                        
                                    </td>
                                </tr>
                            <?php
                        }
                    }
                        $con->close();
                        ?>
                        </tbody>
                    </table>
                    </div>
        </div>
    </div>
</div>



<script>

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหาเลขที่ :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [0, "desc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": true,
    //"info": false,
    //"ordering": false,
    //"searching": false,
    //"paging":  false

});

</script>                    