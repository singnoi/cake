<div class="jumbotron text-center mt-5" style="background-color: #50D0D0;">
  <h1 class="kanit text-light textIT ">ลงชื่อเข้าใช้ระบบ</h1> 
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-4">
        <form id="login_bar" method="post" >
                
                    <div class="form-group">
                      
                      <div class="input-group mb-3">
                      <div class="input-group-prepend">
                            <span class="input-group-text"><i class="far fa-envelope"></i></span>
                        </div>
                      <input type="text" class="form-control" id="username" autocomplete="off" placeholder="ชื่อผู้ใช้" required >
                    </div>
                    </div>
                    <div class="form-group">
                      
                      <div class="input-group mb-3">
                            <div class="input-group-prepend">

                                  <span class="input-group-text"><i class="fa fa-key"></i></span>
                              </div>
                      <input type="password" class="form-control" id="password" placeholder="รหัสผ่าน" required>
                      </div>
                    </div>
                    
                    <div class="form-group text-right ">
                      <button type="submit" id="btn_login" class="btn btn-block btn-success "><i class="fas fa-sign-in-alt"></i> เข้าสู่ระบบ</button>
                    </div>
        </form>
                  <span id="check_login"></span>
      </div>
    </div>
  </div>
</div>
<div class="container">
  <div class="row">
    <div class="col-lg-12">
      <div class="alert alert-danger alert-dismissible fade show kanit">
        <button type="button" class="close" data-dismiss="alert">&times;</button>
        <i class="fas fa-radiation-alt fa-spin fa-2x mr-2"></i> <span class="mx-auto my-3"> <strong>Danger!</strong> เฉพาะผู้ดูแลระบบและพนักงานที่เกี่ยวข้องเท่านั้น ! </span>
      </div>
    </div>
  </div>
</div>

<script>
                    $('#login_bar').submit(function(e){
                      e.preventDefault();
                      var username = $("#username").val();
                      var password = $("#password").val();
 
                      $.post("login_action.php",{username: username, password: password},
                        function(data){
                          var obj = jQuery.parseJSON(data);
                          //$('#check_login').html(obj.status);

                            if(obj.ok=='ok'){

                              Swal.fire({
                                    title: "ลงชื่อเข้าใช้ระบบสำเร็จ",
                                    text: obj.person,
                                    closeOnClickOutside: false,
                                    type: "success"
                              }).then((result) => {
                                    if (result.value) {
                                      window.location = obj.url;
                                    }
                              });

                            } else {

                              Swal.fire({
                                title: "ลงชื่อเข้าใช้ ไม่สำเร็จ",
                                text: obj.warning,
                                closeOnClickOutside: false,
                                confirmButtonColor: '#f46b02',
                                type: "warning"
                                });

                            }

                      });

                    });
 
                    </script>