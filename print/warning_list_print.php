<?php
session_start();
$user_fullname = $_SESSION['user_fullname'];
include('../includes/db_connect.php');
$con = connect();
if(isset($_GET['bdate'])) {
    $set_date = $_GET['bdate'];

  } else {
    $set_date = $today_date;

  }
  
$show_date = " ณ ". date_thai_full($set_date);
// ชื่อที่อยู่ร้าน
$qs ="SELECT * from tbl_store limit 1";
$rs = $con->query($qs) or die ($qs);
$obs = $rs->fetch_object();

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>รายงานสินค้าใกล้หมด</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../node_modules//@fortawesome/fontawesome-free/css/all.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/">

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- <link rel="stylesheet" href="../css/print.css"> -->
  <style type="text/css">
    .table-bordered th,
    .table-bordered td {
      border: 1px solid #000 !important;
    }

body {
  background: rgb(204,204,204); 
}
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-top: 1cm;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
  margin-top: 1cm;
}
page[size="A4"][layout="landscape"] {
  width: 29.7cm;
  height: 21cm;  
}
page[size="A3"] {
  width: 29.7cm;
  height: 42cm;
}
page[size="A3"][layout="landscape"] {
  width: 42cm;
  height: 29.7cm;  
}
page[size="A5"] {
  width: 14.8cm;
  height: 21cm;
}
page[size="A5"][layout="landscape"] {
  width: 21cm;
  height: 14.8cm;  
}

@media all {
	.page-break	{ display: none; }
}

@media print {
  body, page {
    font-size:18px; 
    color: #000;
    margin: 0;
    box-shadow: 0;
  }
  .page-break	{ display: block; page-break-before: always; }
}    

</style>

</head>
<body>
<!-- <page size="A4" layout="portrait">A4 portrait</page> -->

<page size="A4">

<div class="container">

    <table width="970">
      <tr>
        <td width="80"><img src="../img/headlogo.png" class="rounded" alt="logo" width="60" height="45"> </td>
        <td >
          <h4 class="sarabunB"><?php echo $obs->store_name;?>
          <span class="font-weight-normal text-reset text18">
            <?php echo $obs->store_address;?>
          </span>
          </h4>
        </td>

      </tr>
    </table>
    <hr>

    <div class="row ">
      <!-- /.col -->
      <div class="col-sm-12 text-center ">
        <h4 class="sarabunB">รายงานสินค้าใกล้หมด </h4>
      </div>
      <div class="col-sm-12 text-center ">
        <span class="sarabunB"><?php echo $show_date;?> </span>
      </div>
   
    </div>
    <!-- /.row -->


  

    <div class="row">
      <div class="col-md-12 ">
      
<?php
      $q = "SELECT
      e.product_id,
      e.product_cat_id,
      ( IFNULL( (SELECT SUM(t2.transaction_qty) FROM tbl_transaction as t2 WHERE t2.product_id = e.product_id and t2.transaction_date <= '$set_date') ,0)  )  AS sum_stock,
      e.product_name,
      e.product_details,
      e.unit_cost,
      e.unit_price,
      e.unit_name,
      e.product_images,
      c.product_cat_name,
      e.min_stock 
      FROM
      tbl_product AS e
      LEFT JOIN tbl_product_cat AS c ON c.product_cat_id = e.product_cat_id
      
      WHERE  IFNULL( (SELECT SUM(t2.transaction_qty) FROM tbl_transaction as t2 WHERE t2.product_id = e.product_id and t2.transaction_date <= '$set_date') ,0)  <= e.min_stock AND e.min_stock <> 0 AND  IFNULL( (SELECT SUM(t2.transaction_qty) FROM tbl_transaction as t2 WHERE t2.product_id = e.product_id and t2.transaction_date <= '$set_date') ,0) <> 0 
      
      ORDER BY
      e.product_id ASC ";
$r = $con->query($q) or die ($q);
$n = $r->num_rows;


?>

        <p class="card-title">มีจำนวนทั้งหมด: <ins>  &nbsp;<?php echo comma($n);?> &nbsp; </ins>   &nbsp;&nbsp;รายการ  </p>

             
              <table class="table table-striped table-bordered table-inverse table-sm " id="tb1" width="970">
                  <thead class="thead-inverse">
                            <tr>
                                <th>รหัสสินค้า</th>
                                <th>ชื่อสินค้า</th>
                                <th>ชื่อหมวด</th>
                                <th class='text-center'>หน่วยนับ</th>
                                <th class='text-right'>จุดสั่งซื้อ</th>
                                <th class='text-right'>จำนวนคงเหลือ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        if($n > 0 ) {
                            while ($result = $r->fetch_object()) {
                                $cl = "";
                                if($result->sum_stock == NULL || $result->sum_stock == 0) {
                                    $cl = " class='text-danger' ";
                                } else {
                              
                                    if($result->sum_stock < $result->min_stock) {
                                        $cl = " class='text-primary' ";
                                    }

                                }

                                ?>
                                <tr <?php echo $cl;?> >
                                    <td><?php echo $result->product_id; ?></td>
                                    <td><?php echo $result->product_name; ?></td>
                                    <td><?php echo $result->product_cat_name; ?></td>
                                    <td class='text-center'><?php echo $result->unit_name; ?></td>
                                    <td class='text-right'><?php echo comma($result->min_stock);?></td>
                                    <th class='text-right'><?php echo comma($result->sum_stock);?></th>
                                </tr>
                            <?php
                        }
                    }
                        $con->close();
                        ?>
                        </tbody>
                    </table>

  </div>
</div>

<div class="row ">
  <div class="col-sm-12 text-center ">
  &nbsp;
            <br>
            <br>
  </div>
</div>

<div class="row ">


<div class="col-4">

    <p class="text-center">

  </p>
  </div>
  <!-- /.col -->

  <!-- /.col -->
  <div class="col-4 text-center ">
  <p class="text-center">
   
  </p>

  </div>
  <!-- /.col -->
  <div class="col-4 text-center ">
  <p class="text-center">
    ลงชื่อ .............................ผู้สั่งพิมพ์ <br>
    ( <?php echo $user_fullname;?> ) <br><br>
    <?php echo date_thai_full($today_date); ?>

  </p>
  </div>
  
</div>
<!-- /.row -->


</div>

</page>


 <!-- <script src="dist/js/app.min.js" type="text/javascript"></script> -->
	<script type="text/javascript">
   window.print();
   setTimeout("window.close();", 3000);
  </script>
</body>
</html>
