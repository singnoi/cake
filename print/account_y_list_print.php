<?php
session_start();
$user_fullname = $_SESSION['user_fullname'];
include('../includes/db_connect.php');
$con = connect();
if(isset($_GET['of_month'])) {
  $of_year = $_GET['of_year'];
} else {
  $of_year = $year_now;
}
  $year_name = $of_year +543;
  
  $show_date = " รายงานรายได้ประจำปี  พ.ศ.".$year_name;

// ชื่อที่อยู่ร้าน
$qs ="SELECT * from tbl_store limit 1";
$rs = $con->query($qs) or die ($qs);
$obs = $rs->fetch_object();

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>รายงานรายได้ประจำปี</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../node_modules//@fortawesome/fontawesome-free/css/all.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/">

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- <link rel="stylesheet" href="../css/print.css"> -->
  <style type="text/css">
    .table-bordered th,
    .table-bordered td {
      border: 1px solid #000 !important;
    }

body {
  background: rgb(204,204,204); 
}
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-top: 1cm;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
  margin-top: 1cm;
}
page[size="A4"][layout="landscape"] {
  width: 29.7cm;
  height: 21cm;  
}
page[size="A3"] {
  width: 29.7cm;
  height: 42cm;
}
page[size="A3"][layout="landscape"] {
  width: 42cm;
  height: 29.7cm;  
}
page[size="A5"] {
  width: 14.8cm;
  height: 21cm;
}
page[size="A5"][layout="landscape"] {
  width: 21cm;
  height: 14.8cm;  
}

@media all {
	.page-break	{ display: none; }
}

@media print {
  body, page {
    font-size:18px; 
    color: #000;
    margin: 0;
    box-shadow: 0;
  }
  .page-break	{ display: block; page-break-before: always; }
}    

</style>

</head>
<body>
<!-- <page size="A4" layout="portrait">A4 portrait</page> -->

<page size="A4">

<div class="container">

    <table width="970">
      <tr>
        <td width="80"><img src="../img/headlogo.png" class="rounded" alt="logo" width="60" height="45"> </td>
        <td >
          <h4 class="sarabunB"><?php echo $obs->store_name;?>
          <span class="font-weight-normal text-reset text18">
            <?php echo $obs->store_address;?>
          </span>
          </h4>
        </td>

      </tr>
    </table>
    <hr>

    <div class="row ">
      <!-- /.col -->
      <div class="col-sm-12 text-center ">
        <h4 class="sarabunB"> <?php echo $show_date;?></h4>
      </div>
   
    </div>
    <!-- /.row -->


  

    <div class="row">
      <div class="col-md-12 ">
      <table class="table table-striped table-bordered  table-sm " width="970" >
      <thead class="thead-light">
                  <tr>
                    <th>ลำดับ</th>
                    <th>ชื่อเดือน</th>
                    <th class='text-right'>จำนวนบิลซื้อ</th>
                    <th class='text-right'>จำนวนบิลขาย</th>
                    <th class='text-right' style="background-color: #f7c01a ;">รวมเงินจ่าย</th>
                    <th class='text-right' style="background-color: #00b935;">รวมเงินรับ</th>
                    <th class='text-right' style="background-color: #6DADAD;">ส่วนต่าง</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $total_price = 0.00;
                  $total_pay = 0.00;
                  foreach ($thai_month_arr as $of_month => $m_name) {
                    if($of_month > 0) {

                      $q = "SELECT
                        Sum(IF(t.transaction_qty < 0,t.transaction_sum_price,0)) as sum_price,
                        Sum(IF(t.transaction_qty > 0,t.transaction_sum_price,0)) as sum_pay,
                        Sum(t.transaction_qty) as qty,
                        Sum(t.transaction_sum_price) as price
                        FROM
                        tbl_transaction AS t
                        WHERE
                        month(t.transaction_date) = '$of_month' AND year(t.transaction_date) = '$of_year'
                        AND
                        t.cut_stock_qty = 'N'
                        ";
                      $r = $con->query($q) or die ($q);
                      if($r->num_rows > 0 ) {
                        $ob = $r->fetch_object();
                        $sum_pay = $ob->sum_pay;
                        $sum_price = $ob->sum_price;
                      } else {
                        $sum_pay = 0.00;
                        $sum_price = 0.00;
                      }

                      $total_price += $sum_price;
                      $total_pay += $sum_pay;

                      $in_code = 0;
                      $out_code = 0;

                      $q1 = "SELECT
                      t.transaction_code,
                      Sum(t.transaction_qty)  as sum_qty
                      FROM
                      tbl_transaction AS t
                      WHERE
                      month(t.transaction_date) = '$of_month' AND year(t.transaction_date) = '$of_year'
                      AND t.cut_stock_qty = 'N'
                      GROUP BY t.transaction_code
                      ORDER BY
                      t.transaction_id ASC";
                      $r1 = $con->query($q1) or die ($q1);
                      if($r1->num_rows > 0) {
                        while ($ob1 = $r1->fetch_object()) {
                          if($ob1->sum_qty < 0) $out_code++;
                          else $in_code++;
                        }
                      }

                      $sum_cal = $sum_price - $sum_pay;
                      echo "<tr>";
                      echo "<td scope='row'>".$of_month."</td>";
                      echo "<td> $m_name </td>";
                      echo "<td class='text-right'> ".comma_a($in_code)."</td>";
                      echo "<td class='text-right'> ".comma_a($out_code)."</td>";
                      echo "<td class='text-right' style='background-color: #FDEFC5 ;'> ".money_a($sum_pay)."</td>";
                      echo "<td class='text-right' style='background-color: #B8FFA5;'> ".money_a($sum_price)."</td>";
                      echo "<td class='text-right' style='background-color: #acd0d0;'> ".money_a($sum_cal)."</td>";
                      echo "</tr>";


                    }
                    
                  }
                  $total_money = $total_price - $total_pay;
                  ?>
                    
                  </tbody>
                  <tfoot>
                  <tr class="thead-light">
                    <th class="text-right" colspan="4"> รวมเงินทั้งสิ้น : </th>
                    <th class="text-right"  style="background-color: #f7c01a ;"> <?php echo money($total_pay);?></th>
                    <th class="text-right"  style="background-color: #00b935;"> <?php echo money($total_price);?></th>
                    <th class="text-right"  style="background-color: #6DADAD;"> <?php echo money($total_money);?></th>
                  </tr>
                 
                  </tfoot>
              </table>

  </div>
</div>

<div class="row ">
  <div class="col-sm-12 text-center ">
  &nbsp;
            <br>
            <br>
  </div>
</div>

<div class="row ">


<div class="col-4">

    <p class="text-center">

  </p>
  </div>
  <!-- /.col -->

  <!-- /.col -->
  <div class="col-4 text-center ">
  <p class="text-center">
   
  </p>

  </div>
  <!-- /.col -->
  <div class="col-4 text-center ">
  <p class="text-center">
    ลงชื่อ .............................ผู้สั่งพิมพ์ <br>
    ( <?php echo $user_fullname;?> ) <br><br>
    วันที่  <?php echo date_thai($today_date); ?>

  </p>
  </div>
  
</div>
<!-- /.row -->


</div>

</page>

<?php 

// สุดใบส่งของ 
$con->close();
?>

 <!-- <script src="dist/js/app.min.js" type="text/javascript"></script> -->
	<script type="text/javascript">
   window.print();
   setTimeout("window.close();", 3000);
  </script>
</body>
</html>
