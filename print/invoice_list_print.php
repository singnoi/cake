<?php
session_start();
$user_fullname = $_SESSION['user_fullname'];
include('../includes/db_connect.php');
$con = connect();
if(isset($_GET['bdate'])) {
    $bdate = $_GET['bdate'];
    $edate = $_GET['edate'];
  } else {
    $bdate = $today_date;
    $edate = $today_date;
  }
  $q = "SELECT
  b.bill_code,
  b.send_date,
  concat_ws(' ',c.fname,c.lname) as customer_name,
  c.address,
  concat_ws(' ',s.fname,s.lname) as staff_name,
  r.car_code,
  r.car_name
  FROM
  tbl_bill AS b
  LEFT OUTER JOIN tbl_customer AS c ON b.customer_id = c.customer_id
  LEFT OUTER JOIN tbl_staff AS s ON b.staff_id = s.staff_id
  LEFT OUTER JOIN tbl_car AS r ON b.car_id = r.car_id
  WHERE
  b.car_id IS NOT NULL AND
  b.car_id <> ''
  AND b.send_date BETWEEN '$bdate' AND '$edate'
  ORDER BY
  b.send_date ASC";
  $r = $con->query($q) or die ($q);
  if($bdate == $edate) $show_date = " ณ ". date_thai_full($bdate);
  else $show_date = "  ช่วง ".date_thai_full($bdate)." ถึง ".date_thai_full($edate);

// ชื่อที่อยู่ร้าน
$qs ="SELECT * from tbl_store limit 1";
$rs = $con->query($qs) or die ($qs);
$obs = $rs->fetch_object();

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>รายงานรถส่งสินค้า</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../node_modules//@fortawesome/fontawesome-free/css/all.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/">

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- <link rel="stylesheet" href="../css/print.css"> -->
  <style type="text/css">
    .table-bordered th,
    .table-bordered td {
      border: 1px solid #000 !important;
    }

body {
  background: rgb(204,204,204); 
}
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-top: 1cm;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
  margin-top: 1cm;
}
page[size="A4"][layout="landscape"] {
  width: 29.7cm;
  height: 21cm;  
}
page[size="A3"] {
  width: 29.7cm;
  height: 42cm;
}
page[size="A3"][layout="landscape"] {
  width: 42cm;
  height: 29.7cm;  
}
page[size="A5"] {
  width: 14.8cm;
  height: 21cm;
}
page[size="A5"][layout="landscape"] {
  width: 21cm;
  height: 14.8cm;  
}

@media all {
	.page-break	{ display: none; }
}

@media print {
  body, page {
    font-size:18px; 
    color: #000;
    margin: 0;
    box-shadow: 0;
  }
  .page-break	{ display: block; page-break-before: always; }
}    

</style>

</head>
<body>
<!-- <page size="A4" layout="portrait">A4 portrait</page> -->

<page size="A4">

<div class="container">

    <table width="970">
      <tr>
        <td width="80"><img src="../img/headlogo.png" class="rounded" alt="logo" width="60" height="45"> </td>
        <td >
          <h4 class="sarabunB"><?php echo $obs->store_name;?>
          <span class="font-weight-normal text-reset text18">
            <?php echo $obs->store_address;?>
          </span>
          </h4>
        </td>

      </tr>
    </table>
    <hr>

    <div class="row ">
      <!-- /.col -->
      <div class="col-sm-12 text-center ">
        <h4 class="sarabunB">รายงานรถส่งสินค้า </h4>
      </div>
      <div class="col-sm-12 text-center ">
        <span class="sarabunB"><?php echo $show_date;?> </span>
      </div>
   
    </div>
    <!-- /.row -->


  

    <div class="row">
      <div class="col-md-12 ">
      <table class="table table-striped  table-sm " width="970">
                <thead class="thead-inverse">
                  <tr>
                    <th>วันที่ส่ง</th>
                    <th>เลขทะเบียน</th>
                    <th>ชื่อรถ</th>
                    
                    <th class='text-left'>คนขับ</th>
                    <th class='text-left'>ชื่อลูกค้า</th>
                    <th class='text-left'>ส่งไปที่</th>
                    <th class='text-center'>เลขที่ใบเสร็จ</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  if($r->num_rows > 0) {
                    while ($ob = $r->fetch_object()) {
                      echo "<tr>";
                      echo "<td scope='row'> ".date_thai($ob->send_date) ."</td>";
                      echo "<td> $ob->car_code </td>";
                      
                      echo "<td class='text-left'> $ob->car_name </td>";
                      echo "<td class='text-left'> ".$ob->staff_name."</td>";
                      echo "<td class='text-left'> ".$ob->customer_name."</td>";
                      echo "<td class='text-left'> ".$ob->address."</td>";
                      echo "<td class='text-center'> ".$ob->bill_code."</td>";
                      echo "</tr>";
                    }
                  }

                  ?>
                    
                  </tbody>
              </table>

  </div>
</div>

<div class="row ">
  <div class="col-sm-12 text-center ">
  &nbsp;
            <br>
            <br>
  </div>
</div>

<div class="row ">


<div class="col-4">

    <p class="text-center">

  </p>
  </div>
  <!-- /.col -->

  <!-- /.col -->
  <div class="col-4 text-center ">
  <p class="text-center">
   
  </p>

  </div>
  <!-- /.col -->
  <div class="col-4 text-center ">
  <p class="text-center">
    ลงชื่อ .............................ผู้สั่งพิมพ์ <br>
    ( <?php echo $user_fullname;?> ) <br><br>
    วันที่  <?php echo date_thai($today_date); ?>

  </p>
  </div>
  
</div>
<!-- /.row -->


</div>

</page>

<?php 

// สุดใบส่งของ 
$con->close();
?>

 <!-- <script src="dist/js/app.min.js" type="text/javascript"></script> -->
	<script type="text/javascript">
   window.print();
   setTimeout("window.close();", 3000);
  </script>
</body>
</html>
