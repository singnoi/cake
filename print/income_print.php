<?php
session_start();
include('../includes/db_connect.php');
$con = connect();

if(isset($_GET['income_code'])) {
  $income_code = $_GET['income_code'];

    $qc = "SELECT *, o.user_id,s.address as shop_address, s.tel as shop_tel from tbl_income as o 
    left outer join tbl_user as u on u.user_id = o.user_id 
    left outer join tbl_shop as s on s.shop_id = o.shop_id 
    where o.income_code = '$income_code'";



    $rc = $con->query($qc) or die ($qc);
    $obc = $rc->fetch_object();
    $income_date = $obc->income_date;
    $income_note = $obc->income_note;
    $user_id = $obc->user_id;
    $user_fullname = $obc->fname." ".$obc->lname;
    $order_code = $obc->order_code;
    // ถ้าเป็นชื่อใน admin
    if($user_id == 2000) {
        $qm = "SELECT * from tbl_admin where user_id = '$user_id'";
        $rm = $con->query($qm) or die ($qm);
        $obm = $rm->fetch_object();
        $user_fullname = $obm->fname." ".$obm->lname;
    }
    $shop_id = $obc->shop_id;
    if($obc->income_status == 'Y') {
      $income_status = "บันทึกแล้ว";
      $show_status = "";
      $show_note = $obc->income_note;
    } else {
      $income_status = "ถูกยกเลิก";
      $show_status = " <span class='text-danger'> ( ใบรับสินค้านี้ ถูกยกเลิก )</span> ";
      $show_note = " <span class='text-danger'> ( $obc->income_note )</span> ";
    }

    $qs ="SELECT * from tbl_store limit 1";
    $rs = $con->query($qs) or die ($qs);
    $obs = $rs->fetch_object();

} else {
    echo "error";
    exit();
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ใบรับสินค้า</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../node_modules//@fortawesome/fontawesome-free/css/all.css">
    <link rel="stylesheet" href="../css/style.css">

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <style type="text/css">
    .table-bordered th,
    .table-bordered td {
      border: 1px solid #000 !important;
    }
    .invoice-1{float:left;width:25%}
    .invoice-center{float:center;width:50%}

    @media print {
      /*
      @page {
        size: 330mm 427mm;
        margin: 14mm;
      }
      */
      body{
       font-size:18px;
        margin: 0;
        color: #000;
        background-color: #fff;
        font-family: 'sarabun'; 
      }
      @page {
        size: auto;
        margin: 14mm;
      }
      .container {
        width: 768px;
      }
      

    }
  </style>
</head>
<body>
<page>
<div class="container">
  <!-- Main content -->
  <div class="row mt-2">
    <div class="col-lg-12">

    <table>
      <tr>
        <td width="120"><img src="../img/headlogo.png" class="rounded" alt="logo" width="100" height="80"> </td>
        <td width="700">
          <h3 class="sarabunB"><?php echo $obs->store_name;?></h3>
          <p>
            <?php echo $obs->store_address;?>
             <br>
            เบอร์โทร. <?php echo $obs->store_tel;?>
          </p>
        </td>
        <td width="240">
        <p><b>เลขที่ &nbsp;&nbsp;<?php echo $income_code;?></b>&nbsp;&nbsp;</p>
        <p><?php echo date_thai_full($income_date);?></p>
        </td>
      </tr>
    </table>

    <div class="row ">
      <!-- /.col -->
      <div class="col-sm-12 text-center ">
        <h3 class="sarabunB">ใบรับสินค้า <?php echo $show_status;?></h3>
      </div>
   
    </div>
    <!-- /.row -->
    <div class="row">
      <div class="col-md-12">
        บริษัท/ร้านผู้จำหน่าย : 
        <span class="sarabunB">
          <?php echo $obc->shop_name;?>
        </span>
        <span>&nbsp; ที่อยู่ : <?php echo $obc->shop_address;?> </span>
        <p>เบอร์โทร : <?php echo $obc->shop_tel;?> </p>

      </div>
      <!-- /.col -->
      <div class="col-md-12">
        จากเลขที่ใบสั่งซื้อ : 
        <span class="sarabunB">
          <?php echo $order_code;?>
        </span>
        <span>&nbsp;&nbsp; เลขที่ใบส่งของ : </span>
        <span class="sarabunB">
        <?php echo $obc->bill_code;?> 
        </span>

        <span>&nbsp;&nbsp; หมายเหตุ : </span>
        <span class="sarabunB">
        <?php echo $show_note;?> 
        </span>


      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 ">
        <table class="table table-bordered">
          <thead>
          <tr>
            <th class="text-center" width="60">ลำดับ</th>
            <th class="text-center" width="490">รายการ</th>
            <th class="text-center" width="90">หน่วยนับ</th>
            <th class="text-right" width="80">จำนวน</th>
            <th class="text-right" width="120">ราคา/หน่วย</th>
            <th class="text-right" width="120">รวมเป็นเงิน</th>
          </tr>
          </thead>
          <tbody>
            <?php
            $total_price = 0.00;
            $q = "SELECT * from tbl_income_list as l left join tbl_product as p on p.product_id = l.product_id where l.income_code = '$income_code'";
            $r = $con->query($q) or die ($q);
            if($r->num_rows > 0 ) {
              $i = 0;
                while ($obl = $r->fetch_object()) {
                  $i++;
                    $sum_price = $obl->income_price * $obl->income_qty;
                    $total_price += $sum_price;
                    echo "<tr>";
                    echo "<td class='text-center'> $i</td>";
                    echo "<td> $obl->product_name </td>";
                    echo "<td class='text-center'> $obl->unit_name </td>";
                    echo "<td class='text-right'> ".comma($obl->income_qty)."</td>";
                    echo "<td class='text-right'> ".money($obl->income_price)."</td>";
                    echo "<td class='text-right'> ".money($sum_price)."</td>";
                    echo "</tr>";
                }
            }
            ?>


          </tbody>
          <tfoot>
            <tr>
              <th colspan="5" class="text-right">รวมทั้งสิ้น ( <?php echo ThaiBaht($total_price);?> ) </th>
              <th class="text-right"><div id="show_tatal"><?php echo money($total_price);?></div></th>
            </tr>
          </tfoot>
        </table>

  </div>
</div>

<div class="row ">
  <div class="col-sm-12 text-center ">
  &nbsp;
            <br>
            <br>
  </div>
</div>

<div class="row ">


<div class="col-4">
    <?php
    $sqlm="select concat(fname,' ',lname) as owner_name from tbl_user where user_type_id = 1 limit 1 ";
    $rm = $con->query($sqlm) or die ($sqlm);
    $arm=$rm->fetch_object();
    ?>
    <p class="text-center">
    ลงชื่อ .............................ผู้ส่งสินค้า <br>
    ( &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ) <br><br>
    วันที่  .............................

    <?php //echo date("w",strtotime('2019-01-01')); ?>
  </p>
  </div>
  <!-- /.col -->

  <!-- /.col -->
  <div class="col-4 text-center ">
  <p class="text-center">
    ลงชื่อ .............................ผู้รับสินค้า <br>
    ( &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  ) <br><br>
    วันที่  .............................

    <?php //echo date("w",strtotime('2019-01-01')); ?>
  </p>

  </div>
  <!-- /.col -->
  <div class="col-4 text-center ">
  <p class="text-center">
    ลงชื่อ .............................ผู้ตรวจรับ <br>
    ( <?php echo $user_fullname;?> ) <br><br>
    วันที่  .............................

    <?php //echo date("w",strtotime('2019-01-01')); ?>
  </p>
  </div>
  
</div>
<!-- /.row -->

    </div>
  </div>
   <!-- /.content -->
 </div>
 <!-- ./wrapper -->

 </page>
<?php


$con->close();
?>

 <!-- <script src="dist/js/app.min.js" type="text/javascript"></script> -->
	<script type="text/javascript">
   window.print();
   setTimeout("window.close();", 3000);
  </script>
</body>
</html>
