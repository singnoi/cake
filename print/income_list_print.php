<?php
session_start();
$user_fullname = $_SESSION['user_fullname'];
include('../includes/db_connect.php');
$con = connect();
if(isset($_GET['bdate'])) {
    $bdate = $_GET['bdate'];
    $edate = $_GET['edate'];
  } else {
    $bdate = $today_date;
    $edate = $today_date;
  }
  $q = "SELECT
  t.product_id,
  t.transaction_qty,
  t.transaction_sum_price,
  t.transaction_sum_cost,
  p.product_name,
  p.unit_name,
  p.unit_price,
  t.transaction_code,
  t.transaction_date,
  t.transaction_price
  FROM
  tbl_transaction AS t
  LEFT OUTER JOIN tbl_product AS p ON t.product_id = p.product_id
  WHERE
  t.transaction_date BETWEEN '$bdate' AND '$edate' 
  AND
  t.transaction_qty > 0 AND
  t.cut_stock_qty = 'N'
  ORDER BY t.transaction_date ASC, t.update_date ASC";
  $r = $con->query($q) or die ($q);
  if($bdate == $edate) $show_date = "  ณ ". date_thai_full($bdate);
  else $show_date = "  ช่วง ".date_thai_full($bdate)." ถึง ".date_thai_full($edate);

// ชื่อที่อยู่ร้าน
$qs ="SELECT * from tbl_store limit 1";
$rs = $con->query($qs) or die ($qs);
$obs = $rs->fetch_object();

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>รายงานรับสินค้า</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../node_modules//@fortawesome/fontawesome-free/css/all.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/">

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- <link rel="stylesheet" href="../css/print.css"> -->
  <style type="text/css">
    .table-bordered th,
    .table-bordered td {
      border: 1px solid #000 !important;
    }

body {
  background: rgb(204,204,204); 
}
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-top: 1cm;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
  margin-top: 1cm;
}
page[size="A4"][layout="landscape"] {
  width: 29.7cm;
  height: 21cm;  
}
page[size="A3"] {
  width: 29.7cm;
  height: 42cm;
}
page[size="A3"][layout="landscape"] {
  width: 42cm;
  height: 29.7cm;  
}
page[size="A5"] {
  width: 14.8cm;
  height: 21cm;
}
page[size="A5"][layout="landscape"] {
  width: 21cm;
  height: 14.8cm;  
}

@media all {
	.page-break	{ display: none; }
}

@media print {
  body, page {
    font-size:18px; 
    color: #000;
    margin: 0;
    box-shadow: 0;
  }
  .page-break	{ display: block; page-break-before: always; }
}    

</style>

</head>
<body>
<!-- <page size="A4" layout="portrait">A4 portrait</page> -->

<page size="A4">

<div class="container">

    <table width="970">
      <tr>
        <td width="80"><img src="../img/headlogo.png" class="rounded" alt="logo" width="60" height="45"> </td>
        <td >
          <h4 class="sarabunB"><?php echo $obs->store_name;?>
          <span class="font-weight-normal text-reset text18">
            <?php echo $obs->store_address;?>
          </span>
          </h4>
        </td>

      </tr>
    </table>
    <hr>

    <div class="row ">
      <!-- /.col -->
      <div class="col-sm-12 text-center ">
        <h4 class="sarabunB">รายงานรับสินค้า </h4>
      </div>
      <div class="col-sm-12 text-center ">
        <span class="sarabunB"><?php echo $show_date;?> </span>
      </div>
   
    </div>
    <!-- /.row -->


  

    <div class="row">
      <div class="col-md-12 ">
      <table class="table table-striped  table-sm " width="970">
      <thead class="thead-inverse">
                  <tr>
                    <th>วันที่</th>
                    <th>เลขที่ใบรับ</th>
                    <th>รหัสสินค้า</th>
                    <th>ชื่อสินค้า</th>
                    
                    <th class='text-center'>หน่วยนับ</th>
                    <th class='text-right'>ราคาซื้อ</th>
                    <th class='text-right'>ยอดซื้อ</th>
                    <th class='text-right'>ยอดเงินจ่าย</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $total_price = 0.00;
                  if($r->num_rows > 0) {
                    while ($ob = $r->fetch_object()) {
                      $total_price += $ob->transaction_sum_price;
                      echo "<tr>";
                      echo "<td scope='row'>".date_thai($ob->transaction_date)."</td>";
                      echo "<td> $ob->transaction_code </td>";
                      echo "<td> $ob->product_id </td>";
                      echo "<td> $ob->product_name </td>";
                      
                      echo "<td class='text-center'> $ob->unit_name </td>";
                      echo "<td class='text-right'> ".money($ob->transaction_price)."</td>";
                      echo "<td class='text-right'> ".comma($ob->transaction_qty)."</td>";
                      echo "<td class='text-right'> ".money($ob->transaction_sum_price)."</td>";
                      echo "</tr>";
                    }
                  }

                  ?>
                    
                  </tbody>
                  <tfoot>
                    <tr>
                      <th class="text-right" colspan="7"> รวมเงินทั้งสิ้น : </th>
                      <th class="text-right"><?php echo money($total_price);?></th>
                    </tr>
                  
                  </tfoot>
              </table>

  </div>
</div>

<div class="row ">
  <div class="col-sm-12 text-center ">
  &nbsp;
            <br>
            <br>
  </div>
</div>

<div class="row ">


<div class="col-4">

    <p class="text-center">

  </p>
  </div>
  <!-- /.col -->

  <!-- /.col -->
  <div class="col-4 text-center ">
  <p class="text-center">
   
  </p>

  </div>
  <!-- /.col -->
  <div class="col-4 text-center ">
  <p class="text-center">
    ลงชื่อ .............................ผู้สั่งพิมพ์ <br>
    ( <?php echo $user_fullname;?> ) <br><br>
    วันที่  <?php echo date_thai($today_date); ?>

  </p>
  </div>
  
</div>
<!-- /.row -->


</div>

</page>

<?php 

// สุดใบส่งของ 
$con->close();
?>

 <!-- <script src="dist/js/app.min.js" type="text/javascript"></script> -->
	<script type="text/javascript">
   window.print();
   setTimeout("window.close();", 3000);
  </script>
</body>
</html>
