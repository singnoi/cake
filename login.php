<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ร้านขายอุปกรณ์ก่อสร้าง ถวัชชัย</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-icon.png">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="./node_modules/sweetalert2/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="./css/style.css">
    <!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'> -->
</head>

<body class="bg-dark">
    <br>
    <br>
    <br>
    <div class="sufee-login d-flex align-content-center flex-wrap">
        <div class="container">
            <div class="login-content">
                <div class="login-logo">
                    <a href="./index.php">
                        <h4 class="text-center text-white">ระบบบริหารจัดการขายอุปกรณ์วัสดุก่อสร้าง</h4>
                    </a>
                </div>
                <div class="login-form">
                <form class="form" role="form" method="post" accept-charset="UTF-8" id="login_bar">
                        <div class="form-group">
                            <label>ชื่อผู้ใช้</label>
                            <input type="text" class="form-control" placeholder="Username" name="username" id="username" value="<?php if(isset($_COOKIE["username"])) { echo $_COOKIE["username"]; } ?>" required >
                        </div>
                        <div class="form-group">
                            <label>รหัสผ่าน</label>
                            <input type="password" class="form-control" placeholder="Password" name="password" id="password" value="<?php if(isset($_COOKIE["password"])) { echo $_COOKIE["password"]; } ?>"  required >
                        </div>
                        <div class="checkbox">
                            <label><input type="checkbox" id="remember" name="remember" <?php if(isset($_COOKIE["username"])) { ?> checked <?php } ?> > จัดจำฉัน </label>
                        </div>
                        <button  type="submit" id="btn_submit" class="btn btn-success btn-flat m-b-30 m-t-30">เข้าใช้ระบบ</button>
                    </form>
                </div>
                <span id="show_log"></span>
            </div>
        </div>
    </div>
    <script src="./node_modules/jquery/dist/jquery.min.js"></script>
    <script src="./node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="./node_modules/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>


<script>

$('#login_bar').submit(function(e){
    e.preventDefault();
    //alert("s");
    
    $.post("login_action.php",$('#login_bar').serialize(),function(info){
        $('#show_log').html(info);
        var obj = JSON.parse(info);
        $('#show_log').html(obj.ok);
        if(obj.ok == 'ok'){
            if( obj.emtype == 1) {
                window.location = "page_owner/";
            }
            if( obj.emtype == 2) {
                window.location = "page_cashier/";
            }
            if( obj.emtype == 3) {
                window.location = "page_accounting/";
            }
            if( obj.emtype == 4) {
                window.location = "page_admin/";
            }

        } else {
            Swal.fire({
                type: 'error',
                title: 'ลงชื่อเข้าใช้ไม่ถูกต้อง',
                text: 'กรุณาลงชื่อเข้าใช้ใหม่อีกครั้ง'
            })
        }
        
    });
    
});
</script>

</body>

</html>