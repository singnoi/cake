<ul class="list-group list-group-flush">
  <a href="?page=main" class="list-group-item list-group-item-action"><span class="fas fa-chevron-right fa-fw mr-3"></span>รายงานยอดขายสินค้า</a>
  <a href="?page=invoice" class="list-group-item list-group-item-action"><span class="fas fa-chevron-right fa-fw mr-3"></span>รายงานรถส่งสินค้า</a>
  <a href="?page=order" class="list-group-item list-group-item-action"><span class="fas fa-chevron-right fa-fw mr-3"></span>รายงานสั่งซื้อสินค้า</a>
  <a href="?page=income" class="list-group-item list-group-item-action"><span class="fas fa-chevron-right fa-fw mr-3"></span>รายงานรับสินค้า</a>
  <a href="?page=stock" class="list-group-item list-group-item-action"><span class="fas fa-chevron-right fa-fw mr-3"></span>รายงานสินค้าคงเหลือ</a>
  <a href="?page=warning" class="list-group-item list-group-item-action"><span class="fas fa-chevron-right fa-fw mr-3"></span>รายงานสินค้าใกล้หมด</a>
  <a href="?page=account" class="list-group-item list-group-item-action"><span class="fas fa-chevron-right fa-fw mr-3"></span>รายงานรายได้ประจำวัน</a>
  <a href="?page=account_m" class="list-group-item list-group-item-action"><span class="fas fa-chevron-right fa-fw mr-3"></span>รายงานรายได้ประจำเดือน</a>
  <a href="?page=account_y" class="list-group-item list-group-item-action"><span class="fas fa-chevron-right fa-fw mr-3"></span>รายงานรายได้ประจำปี</a>
</ul>
