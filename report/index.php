<?php
session_start();
include("../includes/db_connect.php");
if (isset($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = "main";
}
  
if (isset($_SESSION['user_id'])) {
    $logined = true;
    $user_type_id = $_SESSION['user_type_id'];
    // if($user_type_id != 2 && $_SESSION['admin'] != 'Y' ) {
    //     header('location:../index.php?page=403');
    // }
} else {
    $logined = false;
    header('location:../index.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ระบบบริหารจัดการขายอุปกรณ์วัสดุก่อสร้าง ร้านถวัชชัย</title>
    <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../node_modules//@fortawesome/fontawesome-free/css/all.css">
    <link rel="stylesheet" href="../node_modules/sweetalert2/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="../node_modules/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="../node_modules/select2-bootstrap4-theme/dist/select2-bootstrap4.css">
    <link rel="stylesheet" href="../node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="../css/style.css">

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/popper.js/dist/umd/popper.min.js"></script>
    <script src="../node_modules/select2/dist/js/select2.min.js"></script>
    <script src="../node_modules/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="../node_modules/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="../node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
    

</head>
<body>

<?php
include '../includes/header.php';
?>
<div class="container-fluid">

<div class="row">
    <!-- Sidebar -->
    <div id="col-md-4">

    <?php include("menu.php"); ?>
    </div>
    <!-- sidebar-container END -->

    <!-- MAIN -->
    <div class="col">
    <?php include($page.".php"); ?>
    </div>
    <!-- Main Col END -->

</div>
<!-- body-row END -->
</div>

<script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

<script>
$('[data-toggle="tooltip"]').tooltip(); 
</script>

</body>
</html>