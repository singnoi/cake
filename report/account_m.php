<div class="container-fluid mt-2">
    <div class="row">
        <div class="col-lg-12">

         <div class="card bg-warning border-0">

           <div class="card-body">
             <h5 class="textshadow">รายงานรายได้ประจำเดือน</h5>
             <form class="form-inline text-center" id="form_date">
                <label for="of_month" class="mb-2 mr-sm-2"> ณ เดือน:</label>
                <select class="form-control mb-2" id="of_month" name="of_month" >
                <?php
                foreach ($thai_month_arr as $key => $value) {
                    $sl = ($key == $month_now ? " selected " : "");
                    echo "<option value='$key' $sl > $value </option> ";
                }
                ?>
                </select>
                <label for="of_year" class="mb-2 ml-3 mr-sm-2"> ปี พ.ศ.:</label>
                <select class="form-control mb-2" id="of_year" name="of_year" >
                <?php
                $con = connect();
                $q = "SELECT
                year(t.transaction_date) as y1
                FROM
                tbl_transaction AS t
                GROUP BY
                year(t.transaction_date)
                ORDER BY y1 DESC";
                $r = $con->query($q) or die ($q);
                $con->close();
                if($r->num_rows > 0) {
                    while ($ob = $r->fetch_object()) {
                        $year_name = $ob->y1 + 543;
                        $sl = ($ob->y1 == $year_now ? " selected " : "");
                        echo "<option value='$ob->y1' $sl > $year_name </option>";
                    }
                } else {
                    echo "<option value='$year_now' > $year_now_thai </option>";
                }
                ?>
                </select>

                
                
                <button type="submit" class="btn btn-primary mb-2 mr-2 ml-3">แสดงผล</button>
                <button type="button" class="btn btn-secondary mb-2" id="btn_print"> <i class="fas fa-print"></i> พิมพ์</button>
            </form>
           </div>
         </div>

        </div>
    </div>
</div>

<div class="container-fluid mt-2">
    <div class="row">
        <div class="col-lg-12">

         <div class="card">

           <div class="card-body" id="show_list">
           </div>

        </div>
    </div>
</div>

<script>

$('#show_list').load("account_m_list.php");

$('#form_date').submit(function(e){
    e.preventDefault();
    $.post("account_m_list.php",$('#form_date').serialize(),function(info){
        $('#show_list').html(info);
    });
});

$('#btn_print').click(function(){
    var of_month = $('#of_month').val();
    var of_year = $('#of_year').val();
    window.open("../print/account_m_list_print.php?of_month="+of_month+'&of_year='+of_year);
});
</script>