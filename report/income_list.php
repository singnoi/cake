<?php
include("../includes/db_connect.php");
$con = connect();
if(isset($_POST['bdate'])) {
  $bdate = $_POST['bdate'];
  $edate = $_POST['edate'];
} else {
  $bdate = $today_date;
  $edate = $today_date;
}
$q = "SELECT
t.product_id,
t.transaction_qty,
t.transaction_sum_price,
t.transaction_sum_cost,
p.product_name,
p.unit_name,
p.unit_price,
t.transaction_code,
t.transaction_date,
t.transaction_price
FROM
tbl_transaction AS t
LEFT OUTER JOIN tbl_product AS p ON t.product_id = p.product_id
WHERE
t.transaction_date BETWEEN '$bdate' AND '$edate' 
AND
t.transaction_qty > 0 AND
t.cut_stock_qty = 'N'
ORDER BY t.transaction_date ASC, t.update_date ASC

";
$r = $con->query($q) or die ($q);
if($bdate == $edate) $show_date = " รายงานรับสินค้า ณ  ". date_thai_full($bdate);
else $show_date = " รายงานรับสินค้า ช่วง ".date_thai_full($bdate)." ถึง ".date_thai_full($edate);
?>

             <h5 class="textshadow"><?php echo $show_date;?></h5>
             
              <table class="table table-striped table-inverse table-sm " id="tb1">
                <thead class="thead-inverse">
                  <tr>
                    <th>วันที่</th>
                    <th>เลขที่ใบรับ</th>
                    <th>รหัสสินค้า</th>
                    <th>ชื่อสินค้า</th>
                    
                    <th class='text-center'>หน่วยนับ</th>
                    <th class='text-right'>ราคาซื้อ</th>
                    <th class='text-right'>ยอดซื้อ</th>
                    <th class='text-right'>ยอดเงินจ่าย</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $total_price = 0.00;
                  if($r->num_rows > 0) {
                    while ($ob = $r->fetch_object()) {
                      $total_price += $ob->transaction_sum_price;
                      echo "<tr>";
                      echo "<td scope='row'>".date_thai($ob->transaction_date)."</td>";
                      echo "<td> $ob->transaction_code </td>";
                      echo "<td> $ob->product_id </td>";
                      echo "<td> $ob->product_name </td>";
                      
                      echo "<td class='text-center'> $ob->unit_name </td>";
                      echo "<td class='text-right'> ".money($ob->transaction_price)."</td>";
                      echo "<td class='text-right'> ".comma($ob->transaction_qty)."</td>";
                      echo "<td class='text-right'> ".money($ob->transaction_sum_price)."</td>";
                      echo "</tr>";
                    }
                  }

                  ?>
                    
                  </tbody>
                  <tfoot>
                    <tr>
                      <th class="text-right" colspan="7"> รวมเงินทั้งสิ้น : </th>
                      <th class="text-right"><?php echo money($total_price);?></th>
                    </tr>
                  
                  </tfoot>
              </table>

<script>              

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารหัสสินค้า :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [1, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 100,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": true,
    //"info": false,
    //"ordering": false,
    //"searching": false,
    //"paging":  false

});          

</script>
