<?php
include("../includes/db_connect.php");
$con = connect();
if(isset($_POST['bdate'])) {
  $bdate = $_POST['bdate'];

} else {
  $bdate = $today_date;

}
$q = "SELECT
t.product_id,
t.transaction_qty,
t.transaction_sum_price,
p.product_name,
p.unit_name,
t.transaction_code,
t.transaction_date,
t.transaction_price
FROM
tbl_transaction AS t
LEFT OUTER JOIN tbl_product AS p ON t.product_id = p.product_id
WHERE
t.transaction_date = '$bdate' 
AND
t.cut_stock_qty = 'N'
ORDER BY t.transaction_date ASC, t.update_date ASC
";
$r = $con->query($q) or die ($q);
$show_date = " รายงานรายได้ประจำวัน ณ ". date_thai_full($bdate);
?>

             <h5 class="textshadow"><?php echo $show_date;?></h5>
             
              <table class="table table-striped table-bordered table-inverse table-sm " id="tb1">
                <thead class="thead-inverse">
                  <tr>
                    <th>วันที่</th>
                    <th>เลขที่</th>
                    <th>รหัสสินค้า</th>
                    <th>ชื่อสินค้า</th>                   
                    <th class='text-center'>หน่วยนับ</th>
                    <th class='text-right'>จำนวนรับเข้า</th>
                    <th class='text-right'>จำนวนขาย</th>
                    <th class='text-right' style="background-color: #FDEFC5 ;">รวมเงินจ่าย</th>
                    <th class='text-right' style="background-color: #B8FFA5;">รวมเงินรับ</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $total_price = 0.00;
                  $total_pay = 0.00;
                  if($r->num_rows > 0) {
                    while ($ob = $r->fetch_object()) {
                      
                      if($ob->transaction_qty < 0 ) {
                        $out_qty = abs($ob->transaction_qty);
                        $in_qty = 0;
                        $sum_sale = $ob->transaction_sum_price;
                        $sum_pay = 0;
                      } else {
                        $in_qty = $ob->transaction_qty;
                        $out_qty = 0;
                        $sum_pay = $ob->transaction_sum_price;
                        $sum_sale = 0;
                      }

                      $total_price += $sum_sale;
                      $total_pay += $sum_pay;
                      echo "<tr>";
                      echo "<td scope='row'>".date_thai($ob->transaction_date)."</td>";
                      echo "<td> $ob->transaction_code </td>";
                      echo "<td> $ob->product_id </td>";
                      echo "<td> $ob->product_name </td>";
                      
                      echo "<td class='text-center'> $ob->unit_name </td>";
                      echo "<td class='text-right'> ".comma_a($in_qty)."</td>";
                      echo "<td class='text-right'> ".comma_a($out_qty)."</td>";
                      echo "<td class='text-right' style='background-color: #FDEFC5 ;'> ".money_a($sum_pay)."</td>";
                      echo "<td class='text-right' style='background-color: #B8FFA5;'> ".money_a($sum_sale)."</td>";
                      echo "</tr>";
                    }
                  }
                  $total_money = $total_price - $total_pay;
                  ?>
                    
                  </tbody>
                  <tfoot>
                  <tr class="thead-light">
                    <th class="text-right" colspan="7"> รวมเงินทั้งสิ้น : </th>
                    <th class="text-right"  style="background-color: #FDEFC5 ;"> <?php echo money($total_pay);?></th>
                    <th class="text-right"  style="background-color: #B8FFA5;"> <?php echo money($total_price);?></th>
                  </tr>
                  <tr class="thead-light">
                    <th class="text-right" colspan="8" >รวมเงินรับหักจากเงินจ่าย เป็นเงินทั้งสิ้น :</th>
                    <th class='text-right text-primary font-weight-bolder' style="background-color: #85E26C;"><?php echo money($total_money);?></th>
                  </tr>
                  </tfoot>
              </table>

              <script>              

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารหัสสินค้า :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [1, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 100,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": true,
    //"info": false,
    //"ordering": false,
    //"searching": false,
    //"paging":  false

});          

</script>
