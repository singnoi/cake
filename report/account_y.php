<div class="container-fluid mt-2">
    <div class="row">
        <div class="col-lg-12">

         <div class="card bg-warning border-0">

           <div class="card-body">
             <h5 class="textshadow">รายงานรายได้ประจำปี</h5>
             <form class="form-inline text-center" id="form_date">
                
                <label for="of_year" class="mb-2 ml-3 mr-sm-2"> ปี พ.ศ.:</label>
                <select class="form-control mb-2" id="of_year" name="of_year" >
                <?php
                $con = connect();
                $q = "SELECT
                year(t.transaction_date) as y1
                FROM
                tbl_transaction AS t
                GROUP BY
                year(t.transaction_date)
                ORDER BY y1 DESC";
                $r = $con->query($q) or die ($q);
                $con->close();
                if($r->num_rows > 0) {
                    while ($ob = $r->fetch_object()) {
                        $year_name = $ob->y1 + 543;
                        $sl = ($ob->y1 == $year_now ? " selected " : "");
                        echo "<option value='$ob->y1' $sl > $year_name </option>";
                    }
                } else {
                    echo "<option value='$year_now' > $year_now_thai </option>";
                }
                ?>
                </select>

                
                
                <button type="submit" class="btn btn-primary mb-2 mr-2 ml-3">แสดงผล</button>
                <button type="button" class="btn btn-secondary mb-2" id="btn_print"> <i class="fas fa-print"></i> พิมพ์</button>
            </form>
           </div>
         </div>

        </div>
    </div>
</div>

<div class="container-fluid mt-2">
    <div class="row">
        <div class="col-lg-12">

         <div class="card">

           <div class="card-body" id="show_list">
           </div>

        </div>
    </div>
</div>

<script>

$('#show_list').load("account_y_list.php");

$('#form_date').submit(function(e){
    e.preventDefault();
    $.post("account_y_list.php",$('#form_date').serialize(),function(info){
        $('#show_list').html(info);
    });
});

$('#btn_print').click(function(){
    var of_year = $('#of_year').val();
    window.open("../print/account_y_list_print.php?of_year="+of_year);
});
</script>