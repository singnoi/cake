<?php
include("../includes/db_connect.php");
$con = connect();
if(isset($_POST['bdate'])) {
  $bdate = $_POST['bdate'];
  $edate = $_POST['edate'];
} else {
  $bdate = $today_date;
  $edate = $today_date;
}
$q = "SELECT
t.product_id,
Sum(t.transaction_qty) as sum_q,
Sum(t.transaction_sum_price) as sum_p,
Sum(t.transaction_sum_cost) as sum_c,
p.product_name,
p.unit_name,
p.unit_price
FROM
tbl_transaction AS t
LEFT OUTER JOIN tbl_product AS p ON t.product_id = p.product_id
WHERE
t.transaction_date BETWEEN '$bdate' AND '$edate' 
AND
t.transaction_qty < 0 AND
t.cut_stock_qty = 'N'
GROUP BY
t.product_id
ORDER BY
sum_p DESC";
$r = $con->query($q) or die ($q);
if($bdate == $edate) $show_date = " รายงานยอดขาย ณ  ". date_thai_full($bdate);
else $show_date = " รายงานยอดขาย ช่วง ".date_thai_full($bdate)." ถึง ".date_thai_full($edate);
?>

             <h5 class="textshadow"><?php echo $show_date;?></h5>
             
              <table class="table table-striped table-inverse table-sm " id="tb1">
                <thead class="thead-inverse">
                  <tr>
                    <th>รหัสสินค้า</th>
                    <th>ชื่อสินค้า</th>
                    
                    <th class='text-center'>หน่วยนับ</th>
                    <th class='text-right'>ราคาขายล่าสุด</th>
                    <th class='text-right'>ยอดขาย</th>
                    <th class='text-right'>ต้นทุน</th>
                    <th class='text-right'>กำไร</th>
                    <th class='text-right'>รวมเป็นเงิน</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $total_price = 0.00;
                  $total_cost = 0.00;
                  $total_over = 0.00;
                  if($r->num_rows > 0) {
                    while ($ob = $r->fetch_object()) {
                      $total_price += $ob->sum_p;
                      $over = $ob->sum_p - $ob->sum_c;
                      $total_over += $over;
                      $total_cost += $ob->sum_c;
                      $qty = abs($ob->sum_q);
                      echo "<tr>";
                      echo "<td scope='row'> $ob->product_id </td>";
                      echo "<td> $ob->product_name </td>";
                      
                      echo "<td class='text-center'> $ob->unit_name </td>";
                      echo "<td class='text-right'> ".money($ob->unit_price)."</td>";
                      echo "<td class='text-right'> ".comma($qty)."</td>";
                      echo "<td class='text-right'> ".money($ob->sum_c)."</td>";
                      echo "<td class='text-right'> ".money($over)."</td>";
                      echo "<td class='text-right'> ".money($ob->sum_p)."</td>";
                      echo "</tr>";
                    }
                  }

                  ?>
                    
                  </tbody>
                  <tfoot>
                  <tr class="thead-light">
                    <th class="text-right" colspan="5"> รวมเงินทั้งสิ้น : </th>
                    <th class="text-right"> <?php echo money($total_cost);?></th>
                    <th class="text-right"> <?php echo money($total_over);?></th>
                    <th class="text-right"> <?php echo money($total_price);?></th>
                  </tr>
                  </tfoot>
              </table>

              <script>              

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารหัสสินค้า :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [1, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 100,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": true,
    //"info": false,
    //"ordering": false,
    //"searching": false,
    //"paging":  false

});          

</script>
