<?php
include("../includes/db_connect.php");
$con = connect();
if(isset($_POST['bdate'])) {
  $set_date = $_POST['bdate'];
} else {
  $set_date = $today_date;
}
$q = "SELECT
e.product_id,
e.product_cat_id,
( IFNULL( (SELECT SUM(t2.transaction_qty) FROM tbl_transaction as t2 WHERE t2.product_id = e.product_id and t2.transaction_date <= '$set_date') ,0)  )  AS sum_stock,
e.product_name,
e.product_details,
e.unit_cost,
e.unit_price,
e.unit_name,
e.product_images,
c.product_cat_name,
e.min_stock 
FROM
tbl_product AS e
LEFT JOIN tbl_product_cat AS c ON c.product_cat_id = e.product_cat_id

WHERE  IFNULL( (SELECT SUM(t2.transaction_qty) FROM tbl_transaction as t2 WHERE t2.product_id = e.product_id and t2.transaction_date <= '$set_date') ,0)  <= e.min_stock AND e.min_stock <> 0 AND  IFNULL( (SELECT SUM(t2.transaction_qty) FROM tbl_transaction as t2 WHERE t2.product_id = e.product_id and t2.transaction_date <= '$set_date') ,0) <> 0 

ORDER BY
e.product_id ASC ";
$r = $con->query($q) or die ($q);
$n = $r->num_rows;

$show_date = " รายงานสินค้าใกล้หมด ณ วันที่ ". date_thai($set_date);
?>

             <h5 class="textshadow"><?php echo $show_date;?></h5>

        <p class="card-title">มีจำนวนทั้งหมด: <ins>  &nbsp;<?php echo comma($n);?> &nbsp; </ins>   &nbsp;&nbsp;รายการ  </p>
<hr>
             
              <table class="table table-striped table-bordered table-inverse table-sm " id="tb1">
                  <thead class="thead-inverse">
                            <tr>
                                <th>รหัสสินค้า</th>
                                <th>ชื่อสินค้า</th>
                                <th>ชื่อหมวด</th>
                                <th class='text-center'>หน่วยนับ</th>
                                <th class='text-right'>เกณฑ์คงคลัง</th>
                                <th class='text-right'>จำนวนคงเหลือ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        if($n > 0 ) {
                            while ($result = $r->fetch_object()) {
                                $cl = "";
                                if($result->sum_stock == NULL || $result->sum_stock == 0) {
                                    $cl = " class='text-danger' ";
                                }

                                ?>
                                <tr <?php echo $cl;?> >
                                    <td><?php echo $result->product_id; ?></td>
                                    <td><?php echo $result->product_name; ?></td>
                                    <td><?php echo $result->product_cat_name; ?></td>
                                    <td class='text-center'><?php echo $result->unit_name; ?></td>
                                    <td class='text-right'><?php echo comma($result->min_stock);?></td>
                                    <th class='text-right'><?php echo comma($result->sum_stock);?></th>
                                </tr>
                            <?php
                        }
                    }
                        $con->close();
                        ?>
                        </tbody>
                    </table>

              <script>              

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารหัสสินค้า :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [1, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 100,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": true,
    //"info": false,
    //"ordering": false,
    //"searching": false,
    //"paging":  false

});          

</script>
