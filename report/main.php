<div class="container-fluid mt-2">
    <div class="row">
        <div class="col-lg-12">

         <div class="card bg-warning border-0">

           <div class="card-body">
             <h5 class="textshadow">รายงานยอดขายสินค้า</h5>
             <form class="form-inline text-center" id="form_date">
                <label for="bdate" class="mr-sm-2">ระหว่างวันที่:</label>
                <input type="date" class="form-control mb-2 mr-sm-2" id="bdate" name="bdate" value="<?php echo $today_date;?>" required >
                <label for="edate" class="mr-sm-2">ถึงวันที่:</label>
                <input type="date" class="form-control mb-2 mr-sm-2" id="edate" name="edate" value="<?php echo $today_date;?>" required >
                <button type="submit" class="btn btn-primary mb-2 mr-2">แสดงผล</button>
                <button type="button" class="btn btn-secondary mb-2" id="btn_print"> <i class="fas fa-print"></i> พิมพ์</button>
            </form>
           </div>
         </div>

        </div>
    </div>
</div>

<div class="container-fluid mt-2">
    <div class="row">
        <div class="col-lg-12">

         <div class="card">

           <div class="card-body" id="show_list">
           </div>

        </div>
    </div>
</div>

<script>

$('#show_list').load("sale_list.php");

$('#form_date').submit(function(e){
    e.preventDefault();
    $.post("sale_list.php",$('#form_date').serialize(),function(info){
        $('#show_list').html(info);
    });
});

$('#btn_print').click(function(){
    var bdate = $('#bdate').val();
    var edate = $('#edate').val();
    window.open("../print/sale_list_print.php?bdate="+bdate+"&edate="+edate);
});
</script>