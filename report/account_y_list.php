<?php
include("../includes/db_connect.php");
$con = connect();
if(isset($_POST['of_month'])) {
  $of_year = $_POST['of_year'];
} else {
  $of_year = $year_now; 
}
$year_name = $of_year +543;

$show_date = " รายงานรายได้ประจำปี  พ.ศ.".$year_name;
?>

             <h5 class="textshadow"><?php echo $show_date;?></h5>
             
              <table class="table table-striped table-bordered table-inverse table-sm " id="tb2">
                <thead class="thead-light">
                  <tr>
                    <th>ลำดับ</th>
                    <th>ชื่อเดือน</th>
                    <th class='text-right'>จำนวนบิลซื้อ</th>
                    <th class='text-right'>จำนวนบิลขาย</th>
                    <th class='text-right' style="background-color: #f7c01a ;">รวมเงินจ่าย</th>
                    <th class='text-right' style="background-color: #00b935;">รวมเงินรับ</th>
                    <th class='text-right' style="background-color: #6DADAD;">ส่วนต่าง</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $total_price = 0.00;
                  $total_pay = 0.00;
                  foreach ($thai_month_arr as $of_month => $m_name) {
                    if($of_month > 0) {

                      $q = "SELECT
                        Sum(IF(t.transaction_qty < 0,t.transaction_sum_price,0)) as sum_price,
                        Sum(IF(t.transaction_qty > 0,t.transaction_sum_price,0)) as sum_pay,
                        Sum(t.transaction_qty) as qty,
                        Sum(t.transaction_sum_price) as price
                        FROM
                        tbl_transaction AS t
                        WHERE
                        month(t.transaction_date) = '$of_month' AND year(t.transaction_date) = '$of_year'
                        AND
                        t.cut_stock_qty = 'N'
                        ";
                      $r = $con->query($q) or die ($q);
                      if($r->num_rows > 0 ) {
                        $ob = $r->fetch_object();
                        $sum_pay = $ob->sum_pay;
                        $sum_price = $ob->sum_price;
                      } else {
                        $sum_pay = 0.00;
                        $sum_price = 0.00;
                      }

                      $total_price += $sum_price;
                      $total_pay += $sum_pay;

                      $in_code = 0;
                      $out_code = 0;

                      $q1 = "SELECT
                      t.transaction_code,
                      Sum(t.transaction_qty)  as sum_qty
                      FROM
                      tbl_transaction AS t
                      WHERE
                      month(t.transaction_date) = '$of_month' AND year(t.transaction_date) = '$of_year'
                      AND t.cut_stock_qty = 'N'
                      GROUP BY t.transaction_code
                      ORDER BY
                      t.transaction_id ASC";
                      $r1 = $con->query($q1) or die ($q1);
                      if($r1->num_rows > 0) {
                        while ($ob1 = $r1->fetch_object()) {
                          if($ob1->sum_qty < 0) $out_code++;
                          else $in_code++;
                        }
                      }

                      $sum_cal = $sum_price - $sum_pay;
                      echo "<tr>";
                      echo "<td scope='row'>".$of_month."</td>";
                      echo "<td> $m_name </td>";
                      echo "<td class='text-right'> ".comma_a($in_code)."</td>";
                      echo "<td class='text-right'> ".comma_a($out_code)."</td>";
                      echo "<td class='text-right' style='background-color: #FDEFC5 ;'> ".money_a($sum_pay)."</td>";
                      echo "<td class='text-right' style='background-color: #B8FFA5;'> ".money_a($sum_price)."</td>";
                      echo "<td class='text-right' style='background-color: #acd0d0;'> ".money_a($sum_cal)."</td>";
                      echo "</tr>";


                    }
                    
                  }
                  $total_money = $total_price - $total_pay;
                  ?>
                    
                  </tbody>
                  <tfoot>
                  <tr class="thead-light">
                    <th class="text-right" colspan="4"> รวมเงินทั้งสิ้น : </th>
                    <th class="text-right"  style="background-color: #f7c01a ;"> <?php echo money($total_pay);?></th>
                    <th class="text-right"  style="background-color: #00b935;"> <?php echo money($total_price);?></th>
                    <th class="text-right"  style="background-color: #6DADAD;"> <?php echo money($total_money);?></th>
                  </tr>
                 
                  </tfoot>
              </table>

              <script>              

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารหัสสินค้า :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [1, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 100,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": true,
    //"info": false,
    //"ordering": false,
    //"searching": false,
    //"paging":  false

});          

</script>
