<?php
include("../includes/db_connect.php");
$con = connect();
if(isset($_POST['of_month'])) {
  $of_month = $_POST['of_month'];
  $of_year = $_POST['of_year'];

} else {
  $of_month = $month_now;
  $of_year = $year_now;

}
$year_name = $of_year +543;
$q = "SELECT
t.product_id,
Sum(IF(t.transaction_qty < 0,t.transaction_qty,0)) as out_qty,
Sum(IF(t.transaction_qty > 0,t.transaction_qty,0)) as in_qty,
Sum(IF(t.transaction_qty < 0,t.transaction_sum_price,0)) as sum_price,
Sum(IF(t.transaction_qty > 0,t.transaction_sum_price,0)) as sum_pay,
Sum(t.transaction_qty) as qty,
Sum(t.transaction_sum_price) as price,
p.product_name,
p.unit_name,
t.transaction_date
FROM
tbl_transaction AS t
LEFT OUTER JOIN tbl_product AS p ON t.product_id = p.product_id
WHERE
month(t.transaction_date) = '$of_month' AND year(t.transaction_date) = '$of_year'
AND
t.cut_stock_qty = 'N'
GROUP BY
t.transaction_date,
t.product_id
ORDER BY
t.transaction_date ASC,
t.update_date ASC

";
$r = $con->query($q) or die ($q);
$show_date = " รายงานรายได้ประจำเดือน ณ เดือน ". $thai_month_arr[$of_month]." ปี พ.ศ.".$year_name;
?>

             <h5 class="textshadow"><?php echo $show_date;?></h5>
             
              <table class="table table-striped table-bordered table-inverse table-sm " id="tb1">
                <thead class="thead-inverse">
                  <tr>
                    <th>วันที่</th>
                    <th>รหัสสินค้า</th>
                    <th>ชื่อสินค้า</th>                   
                    <th class='text-center'>หน่วยนับ</th>
                    <th class='text-right'>จำนวนรับเข้า</th>
                    <th class='text-right'>จำนวนขาย</th>
                    <th class='text-right' style="background-color: #FDEFC5 ;">รวมเงินจ่าย</th>
                    <th class='text-right' style="background-color: #B8FFA5;">รวมเงินรับ</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  $total_price = 0.00;
                  $total_pay = 0.00;
                  if($r->num_rows > 0) {
                    while ($ob = $r->fetch_object()) {
                      $out_qty = abs($ob->out_qty);
                      $total_price += $ob->sum_price;
                      $total_pay += $ob->sum_pay;
                      echo "<tr>";
                      echo "<td scope='row'>".date_thai($ob->transaction_date)."</td>";
                      echo "<td> $ob->product_id </td>";
                      echo "<td> $ob->product_name </td>";
                      echo "<td class='text-center'> $ob->unit_name </td>";
                      echo "<td class='text-right'> ".comma_a($ob->in_qty)."</td>";
                      echo "<td class='text-right'> ".comma_a($out_qty)."</td>";
                      echo "<td class='text-right' style='background-color: #FDEFC5 ;'> ".money_a($ob->sum_pay)."</td>";
                      echo "<td class='text-right' style='background-color: #B8FFA5;'> ".money_a($ob->sum_price)."</td>";
                      echo "</tr>";
                    }
                  }
                  $total_money = $total_price - $total_pay;
                  ?>
                    
                  </tbody>
                  <tfoot>
                  <tr class="thead-light">
                    <th class="text-right" colspan="6"> รวมเงินทั้งสิ้น : </th>
                    <th class="text-right"  style="background-color: #FDEFC5 ;"> <?php echo money($total_pay);?></th>
                    <th class="text-right"  style="background-color: #B8FFA5;"> <?php echo money($total_price);?></th>
                  </tr>
                  <tr class="thead-light">
                    <th class="text-right" colspan="7" >รวมเงินรับหักจากเงินจ่าย เป็นเงินทั้งสิ้น :</th>
                    <th class='text-right text-primary font-weight-bolder' style="background-color: #85E26C;"><?php echo money($total_money);?></th>
                  </tr>
                  </tfoot>
              </table>

              <script>              

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารหัสสินค้า :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [1, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 100,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": true,
    //"info": false,
    //"ordering": false,
    //"searching": false,
    //"paging":  false

});          

</script>
