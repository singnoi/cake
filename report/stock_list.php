<?php
include("../includes/db_connect.php");
$con = connect();
if(isset($_POST['bdate'])) {
  $set_date = $_POST['bdate'];
} else {
  $set_date = $today_date;
}
$q = "SELECT
e.product_id,
e.product_cat_id,
( IFNULL( (SELECT SUM(t2.transaction_qty) FROM tbl_transaction as t2 WHERE t2.product_id = e.product_id and t2.transaction_date <= '$set_date') ,0)  )  AS sum_stock,
e.product_name,
e.product_details,
e.unit_cost,
e.unit_price,
e.unit_name,
e.product_images,
c.product_cat_name,
e.min_stock 
FROM
tbl_product AS e
LEFT JOIN tbl_product_cat AS c ON c.product_cat_id = e.product_cat_id
ORDER BY
e.product_id ASC ";
$r = $con->query($q) or die ($q);
$show_date = " รายงานยอดสินค้าคงเหลือ ณ ". date_thai_full($set_date);
?>

             <h5 class="textshadow"><?php echo $show_date;?></h5>

<?php
             $sql1 = "SELECT
             COUNT(e.product_id) as totals,
             (SELECT COUNT(e1.product_id) FROM tbl_product as e1 where min_stock < (SELECT IFNULL(SUM(t2.transaction_qty),0)
              FROM tbl_transaction as t2 WHERE t2.product_id = e1.product_id and t2.transaction_date <= '$set_date' ) ) as sum_full,
             
             (SELECT COUNT(e1.product_id) FROM tbl_product as e1 where min_stock <> 0 AND min_stock >= (SELECT IFNULL(SUM(t2.transaction_qty),0)
              FROM tbl_transaction as t2 WHERE t2.product_id = e1.product_id and t2.transaction_date <= '$set_date' ) AND (SELECT IFNULL(SUM(t2.transaction_qty),0)
              FROM tbl_transaction as t2 WHERE t2.product_id = e1.product_id and t2.transaction_date <= '$set_date' ) <> 0  ) as sum_min,
             
             (SELECT COUNT(e1.product_id) FROM tbl_product as e1 where (SELECT IFNULL(SUM(t2.transaction_qty),0) FROM tbl_transaction as t2 WHERE t2.product_id = e1.product_id  and t2.transaction_date <= '$set_date' ) is NULL ||   (SELECT IFNULL(SUM(t2.transaction_qty),0) FROM tbl_transaction as t2 WHERE t2.product_id = e1.product_id  and t2.transaction_date <= '$set_date' ) = 0) as sum_emtry
             FROM
             tbl_product AS e
 ";
//echo $sql1;

$r1 = $con->query($sql1) or die ($sql1);
$ob1 = $r1->fetch_object();
?>
        <p class="card-title">จำนวนรายการ สินค้าทั้งหมด: <ins>  &nbsp;<?php echo comma($ob1->totals);?> &nbsp; </ins>   &nbsp;&nbsp; สินค้าพร้อมจำหน่าย: <ins>  &nbsp;<?php echo comma($ob1->sum_full);?> &nbsp; </ins>   &nbsp;&nbsp;  สินค้าเหลือน้อย: <ins>  &nbsp;<?php echo comma($ob1->sum_min);?> &nbsp; </ins>  &nbsp;&nbsp; สินค้าหมด: <ins> &nbsp; <?php echo comma($ob1->sum_emtry);?> &nbsp; </ins> </p>
<hr>
             
              <table class="table table-striped table-bordered table-inverse table-sm " id="tb1">
                  <thead class="thead-inverse">
                            <tr>
                                <th>รหัสสินค้า</th>
                                <th>ชื่อสินค้า</th>
                                <th>ชื่อหมวด</th>
                                <th class='text-center'>หน่วยนับ</th>
                                <th class='text-right'>เกณฑ์คงคลัง</th>
                                <th class='text-right'>จำนวนคงเหลือ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        if($r->num_rows > 0 ) {
                            while ($result = $r->fetch_object()) {
                                $cl = "";
                                if($result->sum_stock == NULL || $result->sum_stock == 0) {
                                    $cl = " class='text-danger' ";
                                } else {
                              
                                    if($result->sum_stock < $result->min_stock) {
                                        $cl = " class='text-primary' ";
                                    }

                                }

                                ?>
                                <tr <?php echo $cl;?> >
                                    <td><?php echo $result->product_id; ?></td>
                                    <td><?php echo $result->product_name; ?></td>
                                    <td><?php echo $result->product_cat_name; ?></td>
                                    <td class='text-center'><?php echo $result->unit_name; ?></td>
                                    <td class='text-right'><?php echo comma($result->min_stock);?></td>
                                    <th class='text-right'><?php echo comma($result->sum_stock);?></th>
                                </tr>
                            <?php
                        }
                    }
                        $con->close();
                        ?>
                        </tbody>
                    </table>

              <script>              

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารหัสสินค้า :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [1, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 100,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": true,
    //"info": false,
    //"ordering": false,
    //"searching": false,
    //"paging":  false

});          

</script>
