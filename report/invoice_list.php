<?php
include("../includes/db_connect.php");
$con = connect();
if(isset($_POST['bdate'])) {
  $bdate = $_POST['bdate'];
  $edate = $_POST['edate'];
} else {
  $bdate = $today_date;
  $edate = $today_date;
}
$q = "SELECT
b.bill_code,
b.send_date,
concat_ws(' ',c.fname,c.lname) as customer_name,
c.address,
concat_ws(' ',s.fname,s.lname) as staff_name,
r.car_code,
r.car_name
FROM
tbl_bill AS b
LEFT OUTER JOIN tbl_customer AS c ON b.customer_id = c.customer_id
LEFT OUTER JOIN tbl_staff AS s ON b.staff_id = s.staff_id
LEFT OUTER JOIN tbl_car AS r ON b.car_id = r.car_id
WHERE
b.car_id IS NOT NULL AND
b.car_id <> ''
AND b.send_date BETWEEN '$bdate' AND '$edate'
ORDER BY
b.send_date ASC";
$r = $con->query($q) or die ($q);
if($bdate == $edate) $show_date = " รายงานรถส่งสินค้า ณ  ". date_thai_full($bdate);
else $show_date = " รายงานรถส่งสินค้า ช่วง ".date_thai_full($bdate)." ถึง ".date_thai_full($edate);
?>

             <h5 class="textshadow"><?php echo $show_date;?></h5>
             
              <table class="table table-striped table-inverse table-sm ">
                <thead class="thead-inverse">
                  <tr>
                    <th>วันที่ส่ง</th>
                    <th>เลขทะเบียน</th>
                    <th>ชื่อรถ</th>
                    
                    <th class='text-left'>คนขับ</th>
                    <th class='text-left'>ชื่อลูกค้า</th>
                    <th class='text-left'>ส่งไปที่</th>
                    <th class='text-center'>เลขที่ใบเสร็จ</th>
                  </tr>
                  </thead>
                  <tbody>
                  <?php
                  if($r->num_rows > 0) {
                    while ($ob = $r->fetch_object()) {
                      echo "<tr>";
                      echo "<td scope='row'> ".date_thai($ob->send_date) ."</td>";
                      echo "<td> $ob->car_code </td>";
                      
                      echo "<td class='text-left'> $ob->car_name </td>";
                      echo "<td class='text-left'> ".$ob->staff_name."</td>";
                      echo "<td class='text-left'> ".$ob->customer_name."</td>";
                      echo "<td class='text-left'> ".$ob->address."</td>";
                      echo "<td class='text-center'> ".$ob->bill_code."</td>";
                      echo "</tr>";
                    }
                  }

                  ?>
                    
                  </tbody>
              </table>
