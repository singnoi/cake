<?php
//include("../includes/db_connect.php");
$con = connect();
$user_type_id = $_SESSION['user_type_id'];
if( isset($_SESSION['bill_code'])) unset($_SESSION['bill_code']);

$q = "SELECT
b.bill_code,
b.bill_date,
CONCAT_WS(' ',u.fname,u.lname) as cashier_name,
CONCAT_WS(' ',c.fname,c.lname) as customer_name,
CONCAT_WS(' ',s.fname,s.lname) as staff_name,
c.address,
c.tel,
b.bill_note,
b.user_id,
b.send_date,
a.car_name,
a.car_code,
a.car_color,
b.car_id,
s.tel as staff_tel,
b.bill_status,
ifnull(b.sum_qty,0) as sum_qty,
ifnull(b.sum_price,0) as sum_price
FROM
tbl_bill AS b
LEFT OUTER JOIN tbl_customer AS c ON b.customer_id = c.customer_id
LEFT OUTER JOIN tbl_user AS u ON b.user_id = u.user_id
LEFT OUTER JOIN tbl_car AS a ON b.car_id = a.car_id
LEFT OUTER JOIN tbl_staff AS s ON b.staff_id = s.staff_id
ORDER BY
b.bill_code DESC
";

$r = $con->query($q) or die ($q);

?>
<div class="container-fluid">
    <div class="row">

        <div class="col-lg-12">
            <a href="?page=bill" class="btn btn-outline-success mb-2" > <i class="fas fa-plus"></i> ขายสินค้า </a>
        </div>
        <div class="col-lg-12">
        
      

<h4 class="text-center textshadow"> ประวัติรายการใบเสร็จ </h4>

<div class="table-responsive">
<table id="tb1" class="table table-striped table-bordered table-sm ">
                        <thead>
                            <tr>
                                <th>เลขที่</th>
                                <th>วันที่</th>
                                <th>ลูกค้า</th>
                                <th>ฝ่ายขาย</th>
                                <th>จัดส่ง</th>
                                <th>คนขับ</th>
                                <th class='text-right'>จำนวนรายการ</th>
                                <th class='text-right'>รวมเป็นเงิน</th>
                                <th class='text-center'>แสดง</th>
                                <th class='text-center'>สถานะ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        if($r->num_rows > 0 ) {
                            while ($result = $r->fetch_object()) {
                                if($result->car_id == 0) $show_car = "ไม่จัดส่ง";
                                else $show_car = $result->car_name." /".$result->car_code;
                                if($result->bill_status == 'Y') $cl = "";
                                else $cl = "class='bg-warning'";
                                
                                ?>
                                <tr <?php echo $cl;?> >
                                    <td><?php echo $result->bill_code; ?></td>
                                    <td><?php echo date_thai($result->bill_date); ?></td>
                                    <td><?php 
                                    if($result->customer_name == '' || $result->customer_name == NULL){
                                        echo "ผู้ดูแลระบบ";
                                    } else {
                                        echo $result->customer_name; 
                                    }
                                    
                                    ?></td>
                                    <td>
                                    <?php 
                                    if($result->cashier_name == '' || $result->cashier_name == NULL){
                                        echo "<code>ผู้ดูแลระบบ</code>";
                                    } else {
                                        echo $result->cashier_name; 
                                    }
                                    
                                    ?>
                                    </td>
                                    <td><?php echo $show_car; ?></td>
                                    <td><?php echo $result->staff_name; ?></td>
                                    <td class='text-right'><?php echo comma($result->sum_qty); ?></td>
                                    <td class='text-right'><?php echo money($result->sum_price); ?></td>
                                    <td class='text-center'>
                                    <?php
                                    if($result->bill_status == 'Y') {
                                    ?>
                                        <a href="?page=bill_cart&bill_code=<?php echo $result->bill_code;?>" class="text-info"><i class="far fa-file-alt"></i></a>
                                    <?php } else {
                                    ?>
                                        <a href="?page=bill&bill_code=<?php echo $result->bill_code;?>" class="text-danger"><i class="fas fa-edit"></i></a>
                                    <?php    
                                    }
                                    ?>
                                    </td>
                                    <td class='text-center'>
                                    <?php
                                    echo show_status($result->bill_status);
                                    ?>
                                    </td>
                                </tr>
                            <?php
                        }
                    }
                        $con->close();
                        ?>
                        </tbody>
                    </table>
                    </div>
        </div>
    </div>
</div>


<script>

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหาเลขที่ใบเสร็จ :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [0, "desc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": true,
    //"info": false,
    //"ordering": false,
    //"searching": false,
    //"paging":  false

});

</script>                    