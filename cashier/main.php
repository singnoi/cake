<div class="jumbotron text-center mt-5" style="background-color: TOMATO;">
  <h1 class="kanit text-light textB ">ระบบงานขาย</h1> 
  <p class="text-light">ร้านถวัชชัย </p> 
</div>
<div class="container mb-5">
  <div class="row">
    <div class="col-lg-12 text-center">

    <a href="../../cake/cashier/?page=bill" class="btn btn-dark shadow text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-cash-register fa-fw mr-3"></span>
                    <span class="menu-collapsed">ขายสินค้า  </span>
                </div>
            </a>
            <a href="../../cake/cashier/?page=bill_list" class="btn btn-dark shadow text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-credit-card fa-fw mr-3"></span>
                    <span class="menu-collapsed">ประวัติรายการใบเสร็จ  </span>
                </div>
            </a>

            <a href="../../cake/report/?page=main" class="btn btn-dark shadow text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-tags fa-fw mr-3"></span>
                    <span class="menu-collapsed">รายงานยอดขาย </span>
                </div>
            </a>

      
    </div>
  </div>
</div>
<?php
include("../profile.php");
?>