<?php
$con = connect();
//$user_id = $_SESSION['user_id'];
$bill_code = $_GET['bill_code'];
$total_price = 0.00;

    $qc = "SELECT
    b.bill_code,
    b.bill_date,
    CONCAT_WS(' ',u.fname,u.lname) as cashier_name,
    CONCAT_WS(' ',cm.fname,cm.lname) as customer_name,
    CONCAT_WS(' ',s.fname,s.lname) as staff_name,
    cm.address,
    cm.tel,
    c.car_id,
    c.car_name,
    c.car_code,
    c.car_color,
    s.tel as staff_tel,
    b.user_id,
    b.bill_note,
    b.send_date,
    b.bill_status 
    FROM
    tbl_bill AS b
    LEFT OUTER JOIN tbl_user AS u ON b.user_id = u.user_id
    LEFT OUTER JOIN tbl_customer AS cm ON b.customer_id = cm.customer_id
    LEFT OUTER JOIN tbl_car AS c ON b.car_id = c.car_id
    LEFT OUTER JOIN tbl_staff AS s ON b.staff_id = s.staff_id
     where b.bill_code = '$bill_code'";

    $rc = $con->query($qc) or die ($qc);
    $obc = $rc->fetch_object();
    $bill_date = $obc->bill_date;
    $bill_note = $obc->bill_note;
    $user_id = $obc->user_id;
    $cashier_name = $obc->cashier_name;
    $customer_name = $obc->customer_name;
    $staff_name = $obc->staff_name;
    $bill_code = $obc->bill_code;
    $send_date = $obc->send_date;
    $staff_tel = $obc->staff_tel;
    $car_id = $obc->car_id;
    $address = $obc->address;
    $tel = $obc->tel;
    $car_name = $obc->car_name;
    $car_code = $obc->car_code;
    $car_color = $obc->car_color;
    $bill_status = $obc->bill_status;
    // ถ้าเป็นชื่อใน admin
    if($user_id == 2000) {
        $qm = "SELECT * from tbl_admin where user_id = '$user_id'";
        $rm = $con->query($qm) or die ($qm);
        $obm = $rm->fetch_object();
        $cashier_name = $obm->fname." ".$obm->lname;
    }

    if($car_id == 0) {
        $show_car = "ไม่จัดส่ง";
    } else {
        $show_car = $car_name." /".$car_code." /".$car_color;
    }

    //$bill_status = ($obc->bill_status == 'Y' ? "<span class='font-weight-bolder bg-success text-white'> บันทึกแล้ว </span>" : "<span class='font-weight-bolder bg-danger text-white'> ถูกยกเลิก </span>");
 
?>
<h4 class="text-center textshadow"> ใบเสร็จ </h4>
<hr>

<div class="container-fluid border border-info">
    <div class="row my-2">

        <div class="col-lg-3 ">
            <div class="form-group bg-light">
                <label for="bill_code">เลขที่: <code class="text18"><?php echo $bill_code;?></code></label>
            </div>
            
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="update_date">วันที่: <code><?php echo date_thai($bill_date);?></code></label>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="user_id">ผู้บันทึกข้อมูล: <code><?php echo $cashier_name;?></code></label>
            </div>
            
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="bill_status">สถานะใบเสร็จ: <code><?php echo show_status($bill_status);?></code></label>
            </div>
            
        </div>


    </div>
    <div class="row">

    <div class="col-lg-3 ">

        <div class="form-group">
            <label for="order_code">ชื่อที่อยู่ลูกค้า: <code><?php echo $customer_name." /".$address." /โทร:".$tel;?></code> </label>
        </div>

    </div>
        
        <div class="col-lg-3">
            <div class="form-group">
                <label for="bill_date">วันที่นัดส่งของ: <code> <?php echo date_thai($send_date);?> </code></label>
                
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="bill_code">รถส่งของ: <code><?php echo $show_car;?></code> </label>
                
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="bill_code">ผู้ขับ: <code><?php echo $staff_name;?></code> </label>
                
            </div>
        </div>
        
    <div class="col-lg-12">
        <div class="form-group">
                <label for="bill_date">หมายเหตุ: <code> <?php echo $bill_note;?> </code></label>
               
            </div>
    </div>


    </div>
</div>



<div class="container-fluid border border-dark my-2">
    <div class="row my-2">
        <div class="col-lg-12">
        <table class="table  table-bordered table-striped table-sm">
            <thead class="thead-light">
                <tr>
                    <th width="80">รหัสสินค้า</th>
                    <th>ชื่อสินค้า</th>
                    <th width="120" class="text-center">หน่วยนับ</th>
                    <th width="100" class="text-right">จำนวน</th>
                    <th width="140" class="text-right">ราคา/หน่วย</th>
                    <th width="140" class="text-right">รวมเป็นเงิน</th>
                </tr>
            </thead>
            <tbody id="product_list">
                <?php 

                    $q = "SELECT * from tbl_bill_list as l left join tbl_product as p on p.product_id = l.product_id where l.bill_code = '$bill_code'";
                    $r = $con->query($q) or die ($q);
                    if($r->num_rows > 0 ) {
                        while ($obl = $r->fetch_object()) {
                            $sum_price = $obl->bill_price * $obl->bill_qty;
                            $total_price += $sum_price;

                            echo "<tr>";
                            echo "<td> $obl->product_id </td>";
                            echo "<td> $obl->product_name </td>";
                            echo "<td class='text-center'> $obl->unit_name </td>";
                            echo "<td class='text-right'> ".comma($obl->bill_qty)."</td>";
                            echo "<td class='text-right'> ".money($obl->bill_price)."</td>";
                            echo "<td class='text-right'> ".money($sum_price)."</td>";
                            echo "</tr>";
                        }
                    }
                
                ?>
            </tbody>
            <tfoot>

            
                <tr class=" bg-dark text-white">
                    <th colspan="5" class="text-right">รวมเงินทั้งสิ้น</th>
                    <th class="text-right" id="total_price"><?php echo money($total_price);?></th>
                
                </tr>
            </tfoot>
        </table>


        </div>

        <div class="col-lg-12 text-right my-3">
        <a href="?page=bill_list" class="btn btn-success float-left"  id="btn_home" > <i class="fas fa-home"></i> กลับหน้าหลัก </a>
        <span class="text-success font-weight-bold" id="show_saved"></span>

            <a href="#" class="btn btn-secondary" id="btn_print" onclick="print_cart('<?php echo $bill_code;?>')"> <i class="fas fa-print"></i> สั่งพิมพ์ </a>

        </div>

    </div>
</div>
<?php 

$con->close();
?>

<script>

function print_cart(id){
    //alert("test");
    window.open("bill_print.php?bill_code="+id);
}

</script>

