<?php
$con = connect();
$user_id = $_SESSION['user_id'];
$edit = false;
$total_price = 0.00;
if( isset($_GET['bill_code']) ) {
    $edit = true;
    $bill_code = $_GET['bill_code'];
}  

if( isset($_SESSION['bill_code']) ) {
    $edit = true; 
    $bill_code = $_SESSION['bill_code'];
}

if( $edit ) {

    $qc = "SELECT *, o.user_id from tbl_bill as o left outer join tbl_user as u on u.user_id = o.user_id where o.bill_code = '$bill_code'";
    $rc = $con->query($qc) or die ($qc);
    $obc = $rc->fetch_object();
    $bill_date = $obc->bill_date;
  
    $bill_note = $obc->bill_note;
    $user_id = $obc->user_id;
    $user_fullname = $obc->fname." ".$obc->lname;
    $car_id = $obc->car_id;
    $staff_id = $obc->staff_id;
    $send_date = $obc->send_date;
    // ถ้าเป็นชื่อใน admin
    if($_SESSION['user_type_id'] == 0) {
        $qm = "SELECT * from tbl_admin where user_id = '$user_id'";
        $rm = $con->query($qm) or die ($qm);
        $obm = $rm->fetch_object();
        $user_fullname = $obm->fname." ".$obm->lname;
    }
    $customer_id = $obc->customer_id;

    $bill_status = show_status($obc->bill_status);

    $_SESSION['bill_code'] = $bill_code;

    session_write_close();
    
} else {
    $bill_date = $today_date;
    $user_fullname = $_SESSION['user_fullname'];
    $user_id = $_SESSION['user_id'];
    $bill_status = "รอบันทึก";
    $customer_id = 0;
    $bill_note = "";
    $car_id = 0;
    $staff_id = 0;
    $send_date = $today_date;
    

    // ----- ออกเลขที่ bill_code
    $qo = "SELECT bill_code from tbl_bill order by bill_code DESC limit 1";
    $ro = $con->query($qo) or die ($qo);
    if( $ro->num_rows > 0 ) {
        $max_code =  $ro->fetch_object()->bill_code;
        $code_arr = explode("-",$max_code);
        $m = substr($code_arr[0],-2);
        if($m == $month_now) {
            $next_code = intval($code_arr[1]) + 1;
        } else {
            $next_code = 1;
        }
        
    } else {
        $next_code = 1;
    }
    $bill_code = "BL".substr($year_now_thai,-2).$month_now."-".sprintf("%04d",$next_code);
    
    // --------------

}

if( $bill_date == '0000-00-00' || $bill_date == NULL || $bill_date == '' ) {
    $bill_date = $today_date;
}
?>
<h4 class="text-center textshadow"> ขายสินค้า </h4>
<hr>
<form id="form_bill" >

<div class="container-fluid border border-info">
    <div class="row my-2">

        <div class="col-lg-3 ">
            <div class="form-group bg-light">
                <label for="bill_code">เลขที่ใบเสร็จ: <code class="text18"><?php echo $bill_code;?></code></label>
                <input type="hidden" class="form-control" id="bill_code" name="bill_code" value="<?php echo $bill_code;?>"  >
            </div>
            
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="bill_date">วันที่: <code><?php echo date_thai($bill_date);?></code></label>
                <input type="hidden" class="form-control" id="bill_date" name="bill_date" value="<?php echo $bill_date;?>"  >
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="inname">ผู้บันทึกข้อมูล: <code><?php echo $user_fullname;?></code></label>
                <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $user_id;?>"  >
            </div>
            
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="inname">สถานะใบเสร็จ: <code><?php echo $bill_status;?></code></label>
            </div>
            
        </div>


    </div>
    <div class="row">

        <div class="col-lg-3 bg-light border border-light border-right-0 ">
            <div class="form-group">
                <label for="send_date">วันที่นัดส่งของ: <code>*</code></label>
                <input type="date" class="form-control" id="send_date" name="send_date" value="<?php echo $send_date;?>" min="<?php echo $send_date;?>" required >
            </div>
        </div>
        <div class="col-lg-4 bg-light border border-light border-right-0">

            <div class="form-group">
                <label for="company_id">ชื่อลูกค้า/เบอร์โทร: <code>*</code>
                        
                    
                </label>
                <div class="input-group mb-3">
                    <select id="customer_id" name="customer_id" class="form-control select2" required >
                        
                        <?php
                        $qs = "SELECT * from tbl_customer order by customer_id ASC";
                        $rs = $con->query($qs) or die ($qs);
                        $ns = $rs->num_rows;
                        if( $ns > 0 ) {
                            while ($obs = $rs->fetch_object()) {
                                $sl = ($customer_id == $obs->customer_id ? "selected" : "");
                                echo "<option value='$obs->customer_id' $sl > ".$obs->fname." ".$obs->lname." /".$obs->tel." </option> ";
                            }
                        }
                        ?>
                    </select>
                    
                </div>

            </div>

    </div>
    <div class="col-lg-1 ml-n4 bg-light border border-light border-left-0"> <button class="btn btn-outline-secondary btn-sm" type="button" onclick="customer_add();">  <i class="fas fa-plus"></i> เพิ่มลูกค้าใหม่</button> </div>

    <div class="col-lg-4">
        <div class="form-group">
                <label for="bill_note">หมายเหตุ: </label>
                <input type="text" class="form-control" id="bill_note" name="bill_note" value="<?php echo $bill_note;?>"  >
            </div>
    </div>


    </div>

    <div class="row ">

        <div class="col-lg-4 bg-light border border-light border-right-0 ">

        <div class="form-group">
            <label for="company_id">รถส่งของ/ทะเบียน/สี: 
                    
                
            </label>
            <div class="input-group mb-3">
                <select id="car_id" name="car_id" class="form-control select2">
                    <option value="0" > ไม่จัดส่ง</option>
                    <?php
                    $qs = "SELECT * from tbl_car order by car_name ASC";
                    $rs = $con->query($qs) or die ($qs);
                    $ns = $rs->num_rows;
                    if( $ns > 0 ) {
                        while ($obs = $rs->fetch_object()) {
                            $sl = ($car_id == $obs->car_id ? "selected" : "");
                            echo "<option value='$obs->car_id' $sl > ".$obs->car_name." /".$obs->car_code." /".$obs->car_color." </option> ";
                        }
                    }
                    ?>
                </select>
                
            </div>

        </div>

        </div>
        <div class="col-lg-4 bg-light border border-light border-right-0">

            <div class="form-group" id="div_staff_none" >
                <label for="staff">ชื่อคนขับ:</label>
                <div class="input-group mb-3">
                    <select  class="form-control " id="staff" disabled >
                        <option value="0"> ไม่ระบุ </option>
                    </select>                    
                </div>
            </div>


            <div class="form-group" id="div_staff" style="display:none;" >
                <label for="staff_id">ชื่อคนขับ:</label>
                <div class="input-group mb-3">
                    <select id="staff_id" name="staff_id" class="form-control select2" style="width:100%" >
                        <option value="0"> ไม่ระบุ </option>
                        <?php
                        $qs = "SELECT * from tbl_staff order by fname ASC";
                        $rs = $con->query($qs) or die ($qs);
                        $ns = $rs->num_rows;
                        if( $ns > 0 ) {
                            while ($obs = $rs->fetch_object()) {
                                $sl = ($staff_id == $obs->staff_id ? "selected" : "");
                                echo "<option value='$obs->staff_id' $sl > ".$obs->fname." ".$obs->lname." </option> ";
                            }
                        }
                        ?>
                    </select>
                    
                </div>

            </div>

    </div>
  

    <div class="col-lg-4">
    <label for="bill_submit">... </label>
        <button type="submit" class="btn btn-info btn-block" >  <i class="fas fa-plus"></i> เพิ่มรายการสินค้า </button>
        <span id="show_err" class="text-danger"></span>
    </div>

    </div>


</div>

</form>


<!-- The Modal -->
<div class="modal" id="md3">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">เพิ่มลูกค้าใหม่</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="md3_body">
        Modal body..
      </div>

    </div>
  </div>
</div>


<div class="container-fluid border border-dark  my-2">
    <div class="row">
        <div class="col-lg-12 my-2">
        <table class="table  table-bordered table-striped table-sm">
            <thead class="thead-light">
                <tr>
                    <th width="80">รหัสสินค้า</th>
                    <th>ชื่อสินค้า</th>
                    <th width="120" class="text-center">หน่วยนับ</th>   
                    <th width="140" class="text-right">ราคา/หน่วย</th>
                    <th width="100" class="text-right">จำนวน</th>
                    <th width="40"></th>
                    <th width="120" class="text-right">รวมเป็นเงิน</th>
                    
                </tr>
            </thead>
            <tbody id="product_list">
                <?php 
                if($edit) {
                    $q = "SELECT * from tbl_bill_list as l left join tbl_product as p on p.product_id = l.product_id where l.bill_code = '$bill_code'";
                    $r = $con->query($q) or die ($q);
                    if($r->num_rows > 0 ) {
                        while ($obl = $r->fetch_object()) {
                            $sum_price = $obl->bill_price * $obl->bill_qty;
                            $total_price += $sum_price;
                            echo "<tr>";
                            echo "<td> $obl->product_id </td>";
                            echo "<td> $obl->product_name </td>";
                            echo "<td class='text-center'> $obl->unit_name </td>";
                            echo "<td class='text-right'> ".money($obl->bill_price)."</td>";
                            echo "<td class='text-right'> ".comma($obl->bill_qty)."</td>";
                            
                            echo "<td class='text-center'>";
                            ?>
                            <a href="#" class="text-danger" onclick="del_list(<?php echo $obl->bill_list_id;?>);" > <i class="far fa-trash-alt"></i> </a>
                            <?php 
                            echo "</td>";
                            echo "<td class='text-right'> ".money($sum_price)."</td>";
                            echo "</tr>";
                        }
                    }
                }
                ?>
            </tbody>
            <tfoot>

            <form id="form_product">   
                <tr class="bg-light" style="display: none;" id="bill_list_add">
                    <td colspan="2">
                        
                        <select class="form-control select2" id="product_id" name="product_id" style="width:100%" onchange="show_val()" required >
                            <option value=""></option>
                        <?php
                        $q = "SELECT
                        e.product_id,
                        ( IFNULL((SELECT SUM(t2.transaction_qty) FROM tbl_transaction as t2 WHERE t2.product_id = e.product_id AND t2.transaction_id >= IFNULL(ts.transaction_id,0) ),0) + IFNULL(ts2.cut_stock_qty,0) )  AS sum_stock,
                        e.product_name,
                        e.unit_price,
                        e.unit_name
                        FROM
                        tbl_product AS e
                        LEFT OUTER JOIN 
                        (SELECT t.product_id, max(t.transaction_id) as transaction_id  FROM tbl_transaction as t WHERE  t.stock_type = 'Y'  GROUP BY t.product_id  ) as ts
                        ON ts.product_id = e.product_id
                        LEFT OUTER JOIN
                        tbl_transaction as ts2 ON ts2.transaction_id = ts.transaction_id 
                        ORDER BY
                        e.product_id ASC ";
                        $r = $con->query($q) or die ($q);
                        if($r->num_rows > 0) {
                            while ($ob = $r->fetch_object()) {
                                echo "<option value='$ob->product_id' > (".$ob->product_id.") ".$ob->product_name." [".comma($ob->sum_stock)."] </option> ";
                            }
                        }
                        ?>
                        </select>
                    </td>
                    <td class="text-center" id="unit"></td>
                    
                    
                    
                    <td>
                        <input type="number" min="1" step="0.01" name="bill_price" id="bill_price" value="" class="form-control text-right" readonly  >
                    </td>

                    <td>
                        <input type="number" min="1" name="bill_qty" id="bill_qty" value="" class="form-control text-right" required >
                    </td>
                    <td>
                    <input type="hidden" name="sum_stock" id="sum_stock" value="" >
                    <button type="submit" class="btn btn-primary btn-block" > <i class="fas fa-cart-plus"></i> </button>
                    </td>
                    <td class="text-right">
                        
                    </td>
                </tr>
            </form>

                <tr class=" bg-dark text-white">
                    <th colspan="6" class="text-right">รวมเงินทั้งสิ้น</th>
                    <th class="text-right" id="total_price"><?php echo money($total_price);?></th>

                </tr>
            </tfoot>
        </table>


        </div>

        <div class="col-lg-6 text-right my-3">
        

        <span class="text-success font-weight-bold" id="show_saved"> จำนวนเงินสดที่ลูกค้ายื่นชำระ :  </span>

        </div>

        <div class="col-lg-6 text-right my-3">

        <form class="form-inline" id="form_save">
        <div class="input-group mb-3">
            <input type="number" min="1" step="0.01" id="pay_over" class="form-control form-control-lg text-right" placeholder="บาท" required >
            <input type="hidden" id="sum_price" value="<?php echo $total_price;?>" >
            <div class="input-group-append">
                <button type="submit" class="btn btn-primary btn-lg" id="btn_save" > <i class="fas fa-money-bill"></i> ชำระเงิน </button>

                <button type="button" class="btn btn-danger float-left" id="btn_del" onclick="cancel_cart('<?php echo $bill_code;?>');" > <i class="fas fa-eraser"></i> ยกเลิกใบเสร็จ </button>

            </div>
        </div>
        </form>
            
        </div>

    </div>
</div>



<!-- The Modal -->
<div class="modal" id="md4">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">ชำระเงิน</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="md4_body">
        Modal body..
      </div>

    </div>
  </div>
</div>


<?php 

$con->close();
?>

<script>

var car_id = $('#car_id').val();
if(car_id == 0) {
    $('#div_staff_none').show();
    $('#div_staff').hide();
    $('#staff_id').val(0).trigger('change');
}
$('#car_id').change(function(){
    var car_id = $('#car_id').val();
    if(car_id == 0) {
        $('#div_staff_none').show();
        $('#div_staff').hide();
        $('#staff_id').val(0).trigger('change');
    } else {
        $('#div_staff_none').hide();
        $('#div_staff').show();
    }
});

function del_list(id) {
    Swal.fire({
        title: 'ลบรายการสินค้า ?',
        text: "กดยืนยันหากต้องการลบ",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.post("bill_list_del.php",{bill_list_id: id},function(info){
                if(info == 'ok') {
                    window.location = "?page=bill";
                } else {
                    alert(info);
                }
            });
        }
    });    
}

$('.select2').select2({ 
    theme: "bootstrap4"
});

function show_val() {
    var product_id = $('#product_id').val();
    if(product_id == ''){
        $('#unit').html("");
        $('#bill_qty').val("");
        $('#bill_price').val("");
    } else {
    //alert(product_id);
        $.post("bill_js.php",{product_id: product_id},function(info){
            //alert(info);
            var obj = JSON.parse(info);
            $('#unit').html(obj.unit_name);
            $('#bill_qty').val(1);
            $('#bill_price').val(obj.unit_price);
            $('#sum_stock').val(obj.sum_stock);
            if(obj.sum_stock < 1) {
                // ส่งค่า 0 ถ้าหมดสต๊อก
                $('#bill_qty').val(0);
                $("#bill_qty").attr({
                    "max" : obj.sum_stock,
                    "min" : 0
                });
            } else {
                // ค่าห้ามเกินสต๊อก
                $("#bill_qty").attr({
                    "max" : obj.sum_stock
                });
            }
            
        });
    }
}

function customer_add() {
    $("#md3").modal();
    $("#md3_body").load("customer_add.php");
}

$('#form_bill').submit(function(e){
    e.preventDefault();
    
    var bill_code = $('#bill_code').val();
    //alert(bill_code);
    $.post("bill_add_action.php",$('#form_bill').serialize(),function(info){
        
        if(info == 'ok') {
            $('#bill_list_add').show();
        } else {
            $('#show_err').html(info);
        }
        
    });
});
$('#form_save').submit(function(e){
//$('#btn_save').click(function(){
    e.preventDefault();
    var customer_id = $('#customer_id').val();
    if(customer_id == '') {
        alert("กรุณาเลือกลูกค้าก่อนบันทึก !");
        return false;
    }

    var pay_over = $('#pay_over').val();
    var sum_price = $('#sum_price').val();


    if(parseInt(pay_over) < parseInt(sum_price)) {
        //alert(pay_over);
        Swal.fire(
            ' เงินชำระไม่พอ ?',
            'กรุณา ตรวจสอบยอดเงินชำระใหม่อีกครั้ง !'+pay_over+' < '+sum_price,
            'warning'
        )
        
        return false;
    }

    $.post("bill_add_action.php",$('#form_bill').serialize(),function(info){
        
        if(info == 'ok') {
            $('#bill_list_add').hide();
            $('#show_saved').html("บันทึกสำเร็จ");
            //$('#btn_home').show();

            // จ่ายเงิน
            $('#md4').modal("show");
            $('#md4_body').load("bill_pay.php?pay_over="+pay_over);

        } else {
            $('#show_err').html(info);
        }
        
    });
});

$('#btn_print').click(function(){
    var customer_id = $('#customer_id').val();

    if(customer_id == '') {
        alert("กรุณาเลือกผู้จำหน่ายก่อนบันทึก !");
        return false;
    }
    $.post("bill_add_action.php",$('#form_bill').serialize(),function(info){
        
        if(info == 'ok') {
            $('#bill_list_add').hide();
            window.open("../print/bill_print.php?bill_code="+bill_code);

        } else {
            $('#show_err').html(info);
        }
        
    });
});

$('#form_product').submit(function(e){
    e.preventDefault();
    var bill_code = $('#bill_code').val();
    $.post("bill_list_add_action.php",$('#form_product').serialize(),function(info){
        //$('#show_err').html(info);
        if(info == 'ok') {
            $('#unit').html("");
            $('#product_id').val(null).trigger('change');
            $('#bill_qty').val("");
            $('#bill_price').val("");
            //$('#bill_list_add').show();
            $('#product_list').load("bill_list_add.php?bill_code="+bill_code);
            $.post("bill_total.php",{bill_code: bill_code},function(data){
                var ob1 = JSON.parse(data);
                $('#total_price').html(ob1.show_sum);
                $('#sum_price').val(ob1.sum_price);
            });
        } else {
            $('#show_err').html(info);
            Swal.fire(
                'ไม่บันทึก!',
                info,
                'error'
            );
        }
        
    });
});

function cancel_cart(id){

var bill_note = $('#bill_note').val();
if(bill_note == '') {
    Swal.fire(
        'เหตุผล ?',
        'กรุณา ชี้แจงเหตุผล ในช่องหมายเหตุ เพื่อยกเลิกใบเสร็จ !',
        'warning'
    )
    //alert("กรุณา ชี้แจงเหตุผล ในช่องหมายเหตุ ก่อนกดปุ่มยกเลิก !");
    //$('#bill_note').focus();
   
    $("#form_bill input[name=bill_note]").focus();
    return false;
    
}

Swal.fire({
    title: 'ยกเลิกใบเสร็จ ?',
    text: "กดยืนยันหากต้องการยกเลิกการขาย",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'ยืนยัน',
    cancelButtonText: 'ยกเลิก'
}).then((result) => {
    if (result.value) {
        $.post("bill_cancel.php",{bill_code: id, bill_note: bill_note},function(info){
            if(info == 'ok') {
                window.location = "?page=bill_cart&bill_code="+id;
            } else {
                alert(info);
            }
        });
    }
});        
}
</script>

