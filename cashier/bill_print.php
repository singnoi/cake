<?php
session_start();
include('../includes/db_connect.php');
$con = connect();

if(isset($_GET['bill_code'])) {
  $bill_code = $_GET['bill_code'];

  $q = "SELECT
  b.bill_code,
  b.bill_date,
  CONCAT_WS(' ',u.fname,u.lname) as cashier_name,
  CONCAT_WS(' ',c.fname,c.lname) as customer_name,
  CONCAT_WS(' ',s.fname,s.lname) as staff_name,
  c.address,
  c.tel,
  b.bill_note,
  b.user_id,
  b.send_date,
  a.car_name,
  a.car_code,
  a.car_color,
  b.car_id,
  s.tel as staff_tel,
  b.bill_status,
  Count(l.bill_list_id) AS sum_qty,
  Sum(l.bill_price * l.bill_qty) AS sum_price
  FROM
  tbl_bill AS b
  LEFT OUTER JOIN tbl_customer AS c ON b.customer_id = c.customer_id
  LEFT OUTER JOIN tbl_user AS u ON b.user_id = u.user_id
  LEFT OUTER JOIN tbl_car AS a ON b.car_id = a.car_id
  LEFT OUTER JOIN tbl_staff AS s ON b.staff_id = s.staff_id
  LEFT OUTER JOIN tbl_bill_list AS l ON b.bill_code = l.bill_code
  WHERE b.bill_code = '$bill_code'
  GROUP BY
  b.bill_code
  ";
  //echo $q;
  $r = $con->query($q) or die ($q);
  $obc = $r->fetch_object();
  
      $bill_date = $obc->bill_date;
      $bill_note = $obc->bill_note;
      $user_id = $obc->user_id;
      $cashier_name = $obc->cashier_name;
      $customer_name = $obc->customer_name;
      $staff_name = $obc->staff_name;
      $bill_code = $obc->bill_code;
      $send_date = $obc->send_date;
      $staff_tel = $obc->staff_tel;
      $car_id = $obc->car_id;
      $address = $obc->address;
      $tel = $obc->tel;
      $car_name = $obc->car_name;
      $car_code = $obc->car_code;
      $car_color = $obc->car_color;
      $bill_status = $obc->bill_status;
      $sum_qty = $obc->sum_qty;
      $sum_price = $obc->sum_price;
  
   // ถ้าเป็นชื่อใน admin
   if($user_id == 2000) {
      $qm = "SELECT * from tbl_admin where user_id = '$user_id'";
      $rm = $con->query($qm) or die ($qm);
      $obm = $rm->fetch_object();
      $cashier_name = $obm->fname." ".$obm->lname;
  }
  if($car_id == 0) $show_car = "ไม่มีการจัดส่ง";
  else $show_car = $car_name." /".$car_code." /".$car_color;
  
    $qs ="SELECT * from tbl_store limit 1";
    $rs = $con->query($qs) or die ($qs);
    $obs = $rs->fetch_object();

} else {
    echo "error";
    exit();
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ใบเสร็จ</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../node_modules//@fortawesome/fontawesome-free/css/all.css">
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../node_modules/bootstrap/dist/css/">

    <script src="../node_modules/jquery/dist/jquery.min.js"></script>
    <script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- <link rel="stylesheet" href="../css/print.css"> -->
  <style type="text/css">
    .table-bordered th,
    .table-bordered td {
      border: 1px solid #000 !important;
    }

body {
  background: rgb(204,204,204); 
}
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-top: 1cm;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
  margin-top: 1cm;
}
page[size="A4"][layout="landscape"] {
  width: 29.7cm;
  height: 21cm;  
}
page[size="A3"] {
  width: 29.7cm;
  height: 42cm;
}
page[size="A3"][layout="landscape"] {
  width: 42cm;
  height: 29.7cm;  
}
page[size="A5"] {
  width: 14.8cm;
  height: 21cm;
}
page[size="A5"][layout="landscape"] {
  width: 21cm;
  height: 14.8cm;  
}

@media all {
	.page-break	{ display: none; }
}

@media print {
  body, page {
    font-size:18px; 
    color: #000;
    margin: 0;
    box-shadow: 0;
  }
  .page-break	{ display: block; page-break-before: always; }
}    

</style>

</head>
<body>
<!-- <page size="A4" layout="portrait">A4 portrait</page> -->

<page size="A4">

<div class="container">

    <table width="970">
      <tr>
        <td width="120"><img src="../img/headlogo.png" class="rounded" alt="logo" width="100" height="80"> </td>
        <td >
          <h3 class="sarabunB"><?php echo $obs->store_name;?></h3>
          <p>
            <?php echo $obs->store_address;?>
             <br>
            เบอร์โทร. <?php echo $obs->store_tel;?>
          </p>
        </td>
        <td width="240">
        <p><b>เลขที่ &nbsp;&nbsp;<?php echo $bill_code;?></b>&nbsp;&nbsp;</p>
        <p><?php echo date_thai_full($bill_date);?></p>
        </td>
      </tr>
    </table>

    <div class="row ">
      <!-- /.col -->
      <div class="col-sm-12 text-center ">
        <h1 class="sarabunB">ใบเสร็จ </h1>
      </div>
   
    </div>
    <!-- /.row -->
    <div class="row">
      <div class="col-md-12">
        ชื่อลูกค้า : 
        <span class="sarabunB">
          <?php echo $customer_name;?>
        </span>
        <span>&nbsp; ที่อยู่ : <?php echo $address;?> </span>
        <p>เบอร์โทร : <?php echo $tel;?> </p>

      </div>
      <!-- /.col -->
      <div class="col-md-12">
        วันที่นัดส่งสินค้า : 
        <span class="sarabunB">
          <?php echo date_thai_full($send_date);?>
        </span>
        <span>&nbsp;&nbsp; รถ : </span>
        <span class="sarabunB">
        <?php echo $show_car;?> 
        </span>

        


      </div>
    </div>

  

    <div class="row">
      <div class="col-xs-12 ">
        <table class="table table-bordered"  width="970" >
          <thead>
          <tr>
            <th class="text-center" width="60">ลำดับ</th>
            <th class="text-center" width="490">รายการ</th>
            <th class="text-center" width="100">หน่วยนับ</th>
            <th class="text-right" width="80">จำนวน</th>
            <th class="text-right" width="130">ราคา/หน่วย</th>
            <th class="text-right" width="120">รวมเป็นเงิน</th>
          </tr>
          </thead>
          <tbody>
            <?php
            $total_price = 0.00;
            $q = "SELECT * from tbl_bill_list as l left join tbl_product as p on p.product_id = l.product_id where l.bill_code = '$bill_code'";
            $r = $con->query($q) or die ($q);
            if($r->num_rows > 0 ) {
              $i = 0;
                while ($obl = $r->fetch_object()) {
                  $i++;
                    $sum_price = $obl->bill_price * $obl->bill_qty;
                    $total_price += $sum_price;
                    echo "<tr>";
                    echo "<td class='text-center'> $i</td>";
                    echo "<td> $obl->product_name </td>";
                    echo "<td class='text-center'> $obl->unit_name </td>";
                    echo "<td class='text-right'> ".comma($obl->bill_qty)."</td>";
                    echo "<td class='text-right'> ".money($obl->bill_price)."</td>";
                    echo "<td class='text-right'> ".money($sum_price)."</td>";
                    echo "</tr>";
                }
            }
            ?>


          </tbody>
          <tfoot>
            <tr>
              <th colspan="5" class="text-right">รวมทั้งสิ้น ( <?php echo ThaiBaht($total_price);?> ) </th>
              <th class="text-right"><div id="show_tatal"><?php echo money($total_price);?></div></th>
            </tr>
          </tfoot>
        </table>
        <p>
        <span>&nbsp;&nbsp; หมายเหตุ : </span>
        <span class="sarabunB">
        <?php echo $bill_note;?> 
        </span>
        </p>
  </div>
</div>

<div class="row ">
  <div class="col-sm-12 text-center ">
  &nbsp;
            <br>
            <br>
  </div>
</div>

<div class="row ">


<div class="col-4">

    <p class="text-center">

  </p>
  </div>
  <!-- /.col -->

  <!-- /.col -->
  <div class="col-4 text-center ">
  <p class="text-center">
   
  </p>

  </div>
  <!-- /.col -->
  <div class="col-4 text-center ">
  <p class="text-center">
    ลงชื่อ .............................ผู้รับเงิน <br>
    ( <?php echo $cashier_name;?> ) <br><br>
    <?php echo date_thai_full($bill_date); ?>

  </p>
  </div>
  
</div>
<!-- /.row -->


</div>

</page>
<div class="page-break">&nbsp;</div>



<?php

// ออกใบส่งสินค้า
if($car_id > 0 ) {

?>

<page size="A4">
<div class="container">
  <!-- Main content -->
  <div class="row mt-2">
    <div class="col-lg-12">

    <table width="980">
      <tr>
        <td width="120"><img src="../img/headlogo.png" class="rounded" alt="logo" width="100" height="80"> </td>
        <td >
          <h3 class="sarabunB"><?php echo $obs->store_name;?></h3>
          <p>
            <?php echo $obs->store_address;?>
             <br>
            เบอร์โทร. <?php echo $obs->store_tel;?>
          </p>
        </td>
        <td width="400" class="text-right">
        <p class="text-right"><b>จากใบเสร็จเลขที่ &nbsp;&nbsp;<?php echo $bill_code;?></b>&nbsp;&nbsp;</p>
        <p class="text-right"><?php echo date_thai_full($bill_date);?></p>
        </td>
      </tr>
    </table>

    <div class="row ">
      <!-- /.col -->
      <div class="col-sm-12 text-center ">
        <h1 class="sarabunB">ใบส่งสินค้า </h1>
      </div>
   
    </div>
    <!-- /.row -->
    <div class="row">
      <div class="col-md-12">
      <span class="sarabunB text18 textshadow ">ส่งสินค้าไปที่ </span> ชื่อลูกค้า : 
        <span class="sarabunB">
          <?php echo $customer_name;?>
        </span>
        <span>&nbsp; ที่อยู่ : <?php echo $address;?> </span>
        <p>เบอร์โทร : <?php echo $tel;?> </p>

      </div>
      <!-- /.col -->
      <div class="col-md-12 border border-secondary p-2 my-2">
      <span class="sarabunB text18 textshadow "> วันที่นัดส่งสินค้า : </span>
        <span class="sarabunB">
          <?php echo date_thai_full($send_date);?>
        </span>
        <span>&nbsp;&nbsp; รถ : </span>
        <span class="sarabunB">
        <?php echo $show_car;?> 
        </span>

        <span>&nbsp;&nbsp; คนขับรถ : </span>
        <span class="sarabunB">
        <?php echo $staff_name;?> 
        </span>

        <span>&nbsp;&nbsp; โทร : </span>
        <span class="sarabunB">
        <?php echo $staff_tel;?> 
        </span>


      </div>
    </div>

    <div class="row">
      <div class="col-xs-12 ">
        <table class="table table-bordered" width="970">
          <thead>
          <tr>
            <th class="text-center" width="60">ลำดับ</th>
            <th class="text-center" width="490">รายการ</th>
            <th class="text-center" width="100">หน่วยนับ</th>
            <th class="text-right" width="80">จำนวน</th>
            <th class="text-right" width="130">ราคา/หน่วย</th>
            <th class="text-right" width="120">รวมเป็นเงิน</th>
          </tr>
          </thead>
          <tbody>
            <?php
            $total_price = 0.00;
            $q = "SELECT * from tbl_bill_list as l left join tbl_product as p on p.product_id = l.product_id where l.bill_code = '$bill_code'";
            $r = $con->query($q) or die ($q);
            if($r->num_rows > 0 ) {
              $i = 0;
                while ($obl = $r->fetch_object()) {
                  $i++;
                    $sum_price = $obl->bill_price * $obl->bill_qty;
                    $total_price += $sum_price;
                    echo "<tr>";
                    echo "<td class='text-center'> $i</td>";
                    echo "<td> $obl->product_name </td>";
                    echo "<td class='text-center'> $obl->unit_name </td>";
                    echo "<td class='text-right'> ".comma($obl->bill_qty)."</td>";
                    echo "<td class='text-right'> ".money($obl->bill_price)."</td>";
                    echo "<td class='text-right'> ".money($sum_price)."</td>";
                    echo "</tr>";
                }
            }
            ?>


          </tbody>
          <tfoot>
            <tr>
              <th colspan="5" class="text-right">รวมทั้งสิ้น ( <?php echo ThaiBaht($total_price);?> ) </th>
              <th class="text-right"><div id="show_tatal"><?php echo money($total_price);?></div></th>
            </tr>
          </tfoot>
        </table>
        <p>
        <span>&nbsp;&nbsp; หมายเหตุ : </span>
        <span class="sarabunB">
        <?php echo $bill_note;?> 
        </span>
        </p>

  </div>
</div>

<div class="row ">
  <div class="col-sm-12 text-center ">
  &nbsp;
            <br>
            <br>
  </div>
</div>

<div class="row ">


<div class="col-4">
 
    <p class="text-center">
    ลงชื่อ .............................ผู้ส่งสินค้า <br>
    ( &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; ) <br><br>
    วันที่  .............................

    <?php //echo date("w",strtotime('2019-01-01')); ?>
  </p>
  </div>
  <!-- /.col -->

  <!-- /.col -->
  <div class="col-4 text-center ">
  <p class="text-center">
    ลงชื่อ .............................ผู้รับสินค้า <br>
    ( &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;  ) <br><br>
    วันที่  .............................

    <?php //echo date("w",strtotime('2019-01-01')); ?>
  </p>

  </div>
  <!-- /.col -->
  <div class="col-4 text-center ">
  <p class="text-center">
    ลงชื่อ .............................ผู้จัดทำ <br>
    ( <?php echo $cashier_name;?> ) <br><br>
    วันที่  .............................

    <?php //echo date("w",strtotime('2019-01-01')); ?>
  </p>
  </div>
  
</div>
<!-- /.row -->

    </div>
  </div>
   <!-- /.content -->

</div>

 </page>



<?php 
}
// สุดใบส่งของ 
$con->close();
?>

 <!-- <script src="dist/js/app.min.js" type="text/javascript"></script> -->
	<script type="text/javascript">
   window.print();
   setTimeout("window.close();", 3000);
  </script>
</body>
</html>
