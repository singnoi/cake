<?php
session_start();
include("../includes/db_connect.php");
$con = connect();
$bill_code = $_SESSION['bill_code'];
$pay_over = $_GET['pay_over'];
$q = "SELECT
b.bill_code,
b.bill_date,
CONCAT_WS(' ',u.fname,u.lname) as cashier_name,
CONCAT_WS(' ',c.fname,c.lname) as customer_name,
CONCAT_WS(' ',s.fname,s.lname) as staff_name,
c.address,
c.tel,
b.bill_note,
b.user_id,
b.send_date,
a.car_name,
a.car_code,
a.car_color,
b.car_id,
s.tel as staff_tel,
b.bill_status,
Count(l.bill_list_id) AS sum_qty,
Sum(l.bill_price * l.bill_qty) AS sum_price
FROM
tbl_bill AS b
LEFT OUTER JOIN tbl_customer AS c ON b.customer_id = c.customer_id
LEFT OUTER JOIN tbl_user AS u ON b.user_id = u.user_id
LEFT OUTER JOIN tbl_car AS a ON b.car_id = a.car_id
LEFT OUTER JOIN tbl_staff AS s ON b.staff_id = s.staff_id
LEFT OUTER JOIN tbl_bill_list AS l ON b.bill_code = l.bill_code
WHERE b.bill_code = '$bill_code'
GROUP BY
b.bill_code
";
//echo $q;
$r = $con->query($q) or die ($q);
$obc = $r->fetch_object();

    $bill_date = $obc->bill_date;
    $bill_note = $obc->bill_note;
    $user_id = $obc->user_id;
    $cashier_name = $obc->cashier_name;
    $customer_name = $obc->customer_name;
    $staff_name = $obc->staff_name;
    $bill_code = $obc->bill_code;
    $send_date = $obc->send_date;
    $staff_tel = $obc->staff_tel;
    $car_id = $obc->car_id;
    $address = $obc->address;
    $tel = $obc->tel;
    $car_name = $obc->car_name;
    $car_code = $obc->car_code;
    $car_color = $obc->car_color;
    $bill_status = $obc->bill_status;
    $sum_qty = $obc->sum_qty;
    $sum_price = $obc->sum_price;

 // ถ้าเป็นชื่อใน admin
 if($user_id == 2000) {
    $qm = "SELECT * from tbl_admin where user_id = '$user_id'";
    $rm = $con->query($qm) or die ($qm);
    $obm = $rm->fetch_object();
    $cashier_name = $obm->fname." ".$obm->lname;
}
if($car_id == 0) $show_car = "ไม่มีการจัดส่ง";
else $show_car = $car_name." /".$car_code." /".$car_color;

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
        <form id="form_pay">
        
        <table class="table table-sm table-light">
            <tbody>
                <tr>
                    <th>เลขที่ใบเสร็จ: <?php echo $bill_code;?></th>
                    <th>วันที่: <?php echo date_thai($bill_date);?></th>
                    <th>ลูกค้า: <?php echo $customer_name;?></th>
                    <th>โทร: <?php echo $tel;?></th>
                </tr>
                <tr>
                    <td>วันที่จัดส่ง: <?php echo date_thai($send_date);?></td>
                    <td>รถ: <?php echo $show_car;?></td>
                    <td>คนขับ: <?php echo $staff_name;?></td>
                    <td>โทร: <?php echo $staff_tel;?></td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th>รายการสินค้า: <?php echo comma($sum_qty);?></th>
                    <th>รวมราคาทั้งสิ้น: <?php echo money($sum_price);?></th>
                    <th></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>
        
        </form>
        
        </div>
    </div>

<div class="container border border-success p-4">
    

    <div class="row ">

        <div class="col-md-4 text-right ">
            <h3 class="text-right ">ได้รับเงิน</h3>
        </div>
        <div class="col-md-4 text-center">
            <button class="btn btn-info btn-block btn-lg" ><?php echo money($pay_over);?> </button>
        </div>
    </div>

    <div class="row mt-2 ">

        <div class="col-md-4 text-right ">
            <h3 class="text-right ">รวมราคา</h3>
        </div>
        <div class="col-md-4 text-center">
            <button class="btn btn-primary btn-block btn-lg" ><?php echo money($sum_price);?> </button>
        </div>
    
    </div>

    <div class="row mt-2 ">

        <div class="col-md-4 text-right ">
            <h3 class="text-right ">เงินทอน</h3>
        </div>
        <div class="col-md-4 text-center">
        <?php 
        $money_change = $pay_over - $sum_price;
        ?>
            <button class="btn btn-warning btn-block btn-lg" ><?php echo money($money_change);?> </button>
        </div>
    
    </div>

    <form id="form_bill_save">
    <div class="row mt-2 justify-content-center ">
        <input type="hidden" name="bill_code" value="<?php echo $bill_code;?>" >
        <input type="hidden" name="sum_qty" value="<?php echo $sum_qty;?>" >
        <input type="hidden" name="sum_price" value="<?php echo $sum_price;?>" >
        <input type="hidden" name="pay_over" value="<?php echo $pay_over;?>" >
        <input type="hidden" name="money_change" value="<?php echo $money_change;?>" >

        <div class="col-md-6 text-right ">
        <button type="submit" class="btn btn-success btn-lg btn-block"> <i class="fas fa-print fa-4x"></i> บันทึกและพิมพ์ใบเสร็จ </button>
        </div>
    </div>
    </form>

</div>
<span id="show_print"></span>

</div>

<?php $con->close(); ?>

<script>
$('#form_bill_save').submit(function(e){
    e.preventDefault();
    //alert("ss");
    var bill_code = $('#bill_code').val();
    $.post("bill_pay_action.php",$('#form_bill_save').serialize(),function(info){
        $('#show_print').html(info);
        if(info == 'ok') {
           // window.location = "?page=bill";
            window.open("bill_print.php?bill_code="+bill_code);
            window.location = "?page=bill";

        } else {
            alert(info);
        }
    });
});
</script>