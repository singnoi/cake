
<div class="card">
    <div class="row">
      <div class="col-lg-3 col-md-1">
      </div>
      <div class="col-lg-10 col-md-10">
        <div class="card-body card-block">

        <form id="form_add" method="post">
         
          <table class="table table-borderless">
            <tbody>
              <tr>
                <td align="right">ชื่อลูกค้า:<code>*</code></td>
                <td style="color:#878787;"><input class="form-control" name="fname" type="text" required ></td>
              </tr>
              <tr>
                <td align="right">นามสกุล:<code>*</code></td>
                <td style="color:#878787;"><input class="form-control" name="lname" type="text"  required ></td>
              </tr>
              <tr>
                <td align="right">ที่อยู่:<code>*</code></td>
                <td style="color:#878787;"><input class="form-control" name="address" type="text" required ></td>
              </tr>
              <tr>
                <td align="right">เบอร์โทรศํพท์:<code>*</code></td>
                <td style="color:#878787;"><input class="form-control" name="tel" type="text"  required ></td>
              </tr>
              <tr>
                <td align="right">อีเมล์: </td>
                <td style="color:#878787;"><input class="form-control" name="email" type="text"  ></td>
              </tr>
              
              
              <tr>
                <td></td>
                <td>
                <div id="btn_save">
                <button type="submit" class="btn btn-primary">บันทึก</button>
                </div>

                <div id="btn_finish" style="display:none">
                <button type="button" data-dismiss="modal" class="btn btn-success">บันทึกสำเร็จ ปิดหน้าต่าง</button>
                </div>
                </td>
              </tr>
            </tbody>
          </table>

         </form>
        </div>
      </div>
    </div>
  </div>
  
<script>

$('#form_add').submit(function(e){
    e.preventDefault();
    //alert("s");
    $.post("customer_add_action.php",$('#form_add').serialize(),function(info){
        var obj = jQuery.parseJSON(info);
        if(obj.status == 'ok') {
           $('#show_save').hide();
            $('#customer_id').load("customer_select.php?customer_id="+obj.customer_id);
            $('#btn_save').hide();
            $('#btn_finish').show();
            
        } else {
          $('#show_save').html(obj.status);
        }
    });
});
</script>