<?php
//include("../includes/db_connect.php");
$con = connect();
$user_type_id = $_SESSION['user_type_id'];
if($user_type_id == 0 || $user_type_id == 4) {
    $sql = "SELECT * FROM tbl_user as e left join tbl_user_type as t on t.user_type_id = e.user_type_id where e.user_type_id <> 1 ORDER BY e.user_id ASC ";
} else {
    $sql = "SELECT * FROM tbl_user as e left join tbl_user_type as t on t.user_type_id = e.user_type_id where e.user_type_id not in (1,4) ORDER BY e.user_id ASC ";
}
$r = $con->query($sql) or die ($sql);

?>
<h3 class="text-center textshadow"> ข้อมูลพนักงาน </h3>
<hr>
<span> <a href="?page=user_add" class="btn btn-outline-success mb-2" > <i class="fas fa-plus"></i> เพิ่มพนักงาน </a> </span>
<table id="tb1" class="table table-striped table-bordered table-sm">
                        <thead>
                            <tr>
                                <th>รหัส</th>
                                <th>ชื่อ</th>
                                <th>นามสกุล</th>
                                <th>ที่อยู่</th>
                                <th>เบอร์โทรศัพท์</th>
                                <th>อีเมล์</th>
                                <th>สถานะ</th>
                                <th>การกระทำ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        if($r->num_rows > 0 ) {
                            while ($result = $r->fetch_object()) {

                                ?>
                                <tr>
                                    <td><?php echo $result->user_id; ?></td>
                                    <td><?php echo $result->fname; ?></td>
                                    <td><?php echo $result->lname; ?></td>
                                    <td><div class="contentbox"><?php echo $result->address; ?></div></td>
                                    <td><?php echo $result->tel; ?></td>
                                    <td><div class="contentbox"><?php echo $result->email; ?></div></td>
                                    <td>
                                        <?php
                                        echo $result->user_type_name;
                                        ?>
                                    </td>
                                    <td>
                                        <a href="?page=user_edit&user_id=<?php echo $result->user_id;?>" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> แก้ไข</a>
                                        <a href="#" onclick="del_user('<?php echo $result->user_id;?>');" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i> ลบ </a>
                                    </td>
                                </tr>
                            <?php
                        }
                    }
                        $con->close();
                        ?>
                        </tbody>
                    </table>


                    <script>

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารายชื่อ :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [0, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": true,
    //"info": false,
    //"ordering": false,
    //"searching": false,
    //"paging":  false

});

function del_user(user_id) {
    Swal.fire({
        title: 'ลบผู้ใช้ระบบ ?',
        text: "กดยืนยันหากต้องการลบ",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.post("user_del.php",{user_id: user_id},function(info){
                if(info == 'ok') {
                    window.location = "?page=main";
                } else {
                    alert(info);
                }
            });
        }
    });
}
</script>                    