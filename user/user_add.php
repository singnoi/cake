<?php
//include("../includes/db_connect.php");
$con = connect();
$user_type_id = $_SESSION['user_type_id'];
//echo $user_type_id;
?>
<h3 class="text-center textshadow"> เพิ่มพนักงาน </h3>

<div class="card">
    <div class="row">
      <div class="col-lg-3 col-md-1">
      </div>
      <div class="col-lg-6 col-md-10">
        <div class="card-body card-block">

        <form id="form_add" method="post">
         
          <table class="table table-borderless">
            <tbody>
              <tr>
                <td align="right">ชื่อ:</td>
                <td style="color:#878787;"><input class="form-control" name="fname" type="text" required ></td>
              </tr>
              <tr>
                <td align="right">นามสกุล:</td>
                <td style="color:#878787;"><input class="form-control" name="lname" type="text"  required ></td>
              </tr>
              <tr>
                <td align="right">ที่อยู่:</td>
                <td style="color:#878787;"><input class="form-control" name="address" type="text" required ></td>
              </tr>
              <tr>
                <td align="right">เบอร์โทรศํพท์:</td>
                <td style="color:#878787;"><input class="form-control" name="tel" type="text"  required ></td>
              </tr>
              <tr>
                <td align="right">อีเมล์:</td>
                <td style="color:#878787;"><input class="form-control" name="email" type="text"  required ></td>
              </tr>
              <tr>
                <td align="right">ชื่อผู้ใช้:</td>
                <td style="color:#878787;"><input class="form-control" name="user_name" type="text"  required ></td>
              </tr>
              <tr>
                <td align="right">รหัสผ่าน:</td>
                <td style="color:#878787;"><input class="form-control" id="pass_word" type="text" name="pass_word"  required></td>
              </tr>
              
              <tr>
                <td align="right">สถานะ:</td>
                <td style="color:#878787;">
                    <?php
                    if($user_type_id == 0 || $user_type_id == 4) {
                        $qu = "SELECT * from tbl_user_type where user_type_id <> 1";
                    } else {
                        $qu = "SELECT * from tbl_user_type where user_type_id not in (1,4)";
                    }
                    $ru = $con->query($qu) or die ($qu);
                    ?>
                  <select name="user_type_id" class="form-control" >
                    <?php
                                     
                    
                    while ($obu = $ru->fetch_object()) {
                     
                        echo "<option value='$obu->user_type_id'  > $obu->user_type_name </option> ";
                    }
                    ?>
                  </select>

                  </td>
              </tr>
              <tr>
                <td></td>
                <td>
                <button name="submit" type="submit" class="btn btn-success btn-sm">
                                            <i class="fa fa-save"></i> บันทึก
                                        </button>
                </td>
              </tr>
            </tbody>
          </table>

         </form>
        </div>
      </div>
    </div>
  </div>
  
<script>
var check = function () {
    if (document.getElementById('pass_word').value ==
        document.getElementById('confirm_pass_word').value) {
        document.getElementById('message').style.color = 'green';
        document.getElementById('message').innerHTML = 'ถูกต้อง';
    } else {
        document.getElementById('message').style.color = 'red';
        document.getElementById('message').innerHTML = 'กรอกรหัสไม่ตรงกัน';
    }
}

$('#form_add').submit(function(e){
    e.preventDefault();
    //alert("s");
    $.post("user_add_action.php",$('#form_add').serialize(),function(info){
        if(info == 'ok') {
            window.location = "?page=main";
        } else {
            $('#message').html(info);
        }
    });
});
</script>