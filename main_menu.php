        <!-- Bootstrap List Group -->
        <ul class="list-group sticky-top sticky-offset">

            <li class="list-group-item sidebar-separator menu-collapsed"></li>
            <!-- Separator with title -->
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <span class="fas fa-home fa-fw mr-1"></span>
                <a class="navbar-brand" href="./"><img src="img/logo.png" alt="Logo"></a>
            </li>
            <!-- /END Separator -->
            
            <a href="?page=profile" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-user fa-fw mr-3"></span>
                    <span class="menu-collapsed">ข้อมูลส่วนตัว</span>
                </div>
            </a>

            <a href="report/" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-tasks fa-fw mr-3"></span>
                    <span class="menu-collapsed">รายงานยอดขาย</span>
                </div>
            </a>
            <!-- Separator with title -->
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small>งานบันทึกข้อมูล</small>
            </li>
            <!-- /END Separator -->
            <a href="account/" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-chevron-right fa-fw mr-3"></span>
                    <span class="menu-collapsed">ระบบงานบัญชี</span>
                </div>
            </a>
            <a href="cashier/" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-chevron-right fa-fw mr-3"></span>
                    <span class="menu-collapsed">ระบบงานขาย </span>
                </div>
            </a>
            <!-- Separator without title -->
            <li class="list-group-item sidebar-separator menu-collapsed"></li>
        <?php if($user_type_id == 0 || $user_type_id == 4 ) { ?>

            <a href="admin/" class="bg-dark list-group-item list-group-item-action text-warning">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-cogs fa-fw mr-3 text-danger"></span>
                    <span class="menu-collapsed">ผู้ดูแลระบบ </span>
                </div>
            </a>
        <?php } ?>
        </ul>
     