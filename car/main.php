<?php
//include("../includes/db_connect.php");
$con = connect();
$user_type_id = $_SESSION['user_type_id'];

$sql = "SELECT * FROM tbl_car as e order by car_id DESC";

$r = $con->query($sql) or die ($sql);

?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
        
      

<h3 class="text-center textshadow"> ข้อมูลรถส่งของ </h3>
<hr>
<span> <a href="?page=car_add" class="btn btn-outline-success mb-2" > <i class="fas fa-plus"></i> เพิ่มรถ </a> </span>
<div class="table-responsive">
<table id="tb1" class="table table-striped table-bordered table-sm ">
                        <thead>
                            <tr>
                                <th>รหัส</th>
                                <th>เลขทะเบียน</th>
                                <th width="300">ชื่อรถ</th>
                                <th>สี</th>
                                <th>สถานะ</th>
                                <th>การกระทำ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        if($r->num_rows > 0 ) {
                            while ($result = $r->fetch_object()) {
                                if($result->car_status == 'Y') $status = "พร้อมใช้";
                                else $status = "<span class='text-danger'>งดใช้</span>";
                                ?>
                                <tr>
                                    <td><?php echo $result->car_id; ?></td>
                                    <td><?php echo $result->car_code; ?></td>
                                    <td width="300"><?php echo $result->car_name; ?></td>
                                    <td><?php echo $result->car_color; ?></td>
                                    <td><?php echo $status; ?></td>
                                    <td>
                                        <a href="?page=car_edit&car_id=<?php echo $result->car_id;?>" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> แก้ไข</a>
                                        <a href="#" onclick="del_car('<?php echo $result->car_id;?>');" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i> ลบ </a>
                                    </td>
                                </tr>
                            <?php
                        }
                    }
                        $con->close();
                        ?>
                        </tbody>
                    </table>
                    </div>
        </div>
    </div>
</div>

<script>

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหารายชื่อ :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [1, "asc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": true,
    //"info": false,
    //"ordering": false,
    //"searching": false,
    //"paging":  false

});

function del_car(c_id) {
    Swal.fire({
        title: 'ลบผู้ใช้ระบบ ?',
        text: "กดยืนยันหากต้องการลบ",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.post("car_del.php",{car_id: c_id},function(info){
                if(info == 'ok') {
                    window.location = "?page=main";
                } else {
                    alert(info);
                }
            });
        }
    });
}
</script>                    