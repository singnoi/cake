<?php
//include("../includes/db_connect.php");
$con = connect();
$car_id = $_GET['car_id'];
$user_type_id = $_SESSION['user_type_id'];

$q = "SELECT * from tbl_car as u where u.car_id = '$car_id' ";
$r = $con->query($q) or die ($q);
$ob = $r->fetch_object();

?>
<h3 class="text-center textshadow"> แก้ไขข้อมูลรถ </h3>

<div class="card">
    <div class="row">
      <div class="col-lg-3 col-md-1">
      </div>
      <div class="col-lg-6 col-md-10">
        <div class="card-body card-block">

        <form id="form_edit" method="post">
            <input type="hidden" name="car_id" value="<?php echo $ob->car_id;?>" >
          <table class="table table-borderless">
            <tbody>
             
              <tr>
                <td align="right">เลขทะเบียน:</td>
                <td style="color:#878787;"><input class="form-control" name="car_code" type="text" value="<?php echo $ob->car_code; ?>"></td>
              </tr>
              <tr>
                <td align="right">ชื่อรถ:</td>
                <td style="color:#878787;"><input class="form-control" name="car_name" type="text" value="<?php echo $ob->car_name; ?>"></td>
              </tr>
              <tr>
                <td align="right">สี:</td>
                <td style="color:#878787;"><input class="form-control" name="car_color" type="text" value="<?php echo $ob->car_color; ?>"></td>
              </tr>
              <tr>
                <td align="right">สถานะ:</td>
                <td style="color:#878787;">
                <select class="form-control" name="car_status" >
                  <option value="Y" <?php if($ob->car_status=='Y') echo "selected";?> > พร้อมใช้ </option>
                  <option value="N" <?php if($ob->car_status=='N') echo "selected";?> > งดใช้ </option>
                </select>
                </td>
              </tr>

              
              
              <tr>
                <td id="message" class="text-danger"></td>
                <td>
                <button name="submit" type="submit" class="btn btn-success btn-sm">
                                            <i class="fa fa-save"></i> บันทึก
                                        </button>
                </td>
              </tr>
            </tbody>
          </table>

         </form>
        </div>
      </div>
    </div>
  </div>
  
<script>

$('#form_edit').submit(function(e){
    e.preventDefault();
    //alert("s");
    $.post("car_edit_action.php",$('#form_edit').serialize(),function(info){
        if(info == 'ok') {
            window.location = "?page=main";
        } else {
            $('#message').html(info);
        }
    });
});
</script>