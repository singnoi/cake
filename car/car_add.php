<h3 class="text-center textshadow"> เพิ่มรถ </h3>

<div class="card">
    <div class="row">
      <div class="col-lg-3 col-md-1">
      </div>
      <div class="col-lg-6 col-md-10">
        <div class="card-body card-block">

        <form id="form_add" method="post">
         
          <table class="table table-borderless">
            <tbody>
              <tr>
                <td align="right">เลขทะเบียน:</td>
                <td style="color:#878787;"><input class="form-control" name="car_code" type="text" required ></td>
              </tr>
              <tr>
                <td align="right">ชื่อรถ:</td>
                <td style="color:#878787;"><input class="form-control" name="car_name" type="text"  required ></td>
              </tr>
              <tr>
                <td align="right">สี:</td>
                <td style="color:#878787;"><input class="form-control" name="car_color" type="text" required ></td>
              </tr>
              <tr>
                <td align="right">สถานะ:</td>
                <td style="color:#878787;">
                <select class="form-control" name="car_status" >
                  <option value="Y" selected > พร้อมใช้ </option>
                  <option value="N" > งดใช้ </option>
                </select>
                </td>
              </tr>
             
              
              
              <tr>
                <td id="message" class="text-danger"></td>
                <td>
                <button name="submit" type="submit" class="btn btn-success btn-sm">
                                            <i class="fa fa-save"></i> บันทึก
                                        </button>
                </td>
              </tr>
            </tbody>
          </table>

         </form>
        </div>
      </div>
    </div>
  </div>
  
<script>

$('#form_add').submit(function(e){
    e.preventDefault();
    //alert("s");
    $.post("car_add_action.php",$('#form_add').serialize(),function(info){
        if(info == 'ok') {
            window.location = "?page=main";
        } else {
            $('#message').html(info);
        }
    });
});
</script>