<?php
//include("../includes/db_connect.php");
$con = connect();
$staff_id = $_GET['staff_id'];
$user_type_id = $_SESSION['user_type_id'];

$q = "SELECT * from tbl_staff as u where u.staff_id = '$staff_id' ";
$r = $con->query($q) or die ($q);
$ob = $r->fetch_object();

?>
<h3 class="text-center textshadow"> แก้ไขข้อมูลลูกค้า </h3>

<div class="card">
    <div class="row">
      <div class="col-lg-3 col-md-1">
      </div>
      <div class="col-lg-6 col-md-10">
        <div class="card-body card-block">

        <form id="form_edit" method="post">
            <input type="hidden" name="staff_id" value="<?php echo $ob->staff_id;?>" >
          <table class="table table-borderless">
            <tbody>
              <tr>
                <td align="right">รหัส: </td>
                <td style="color:#878787;"><?php echo $ob->staff_id; ?></td>
              </tr>
              <tr>
                <td align="right">ชื่อ:</td>
                <td style="color:#878787;"><input class="form-control" name="fname" type="text" value="<?php echo $ob->fname; ?>"></td>
              </tr>
              <tr>
                <td align="right">นามสกุล:</td>
                <td style="color:#878787;"><input class="form-control" name="lname" type="text" value="<?php echo $ob->lname; ?>"></td>
              </tr>
              <tr>
                <td align="right">ที่อยู่:</td>
                <td style="color:#878787;"><input class="form-control" name="address" type="text" value="<?php echo $ob->address; ?>"></td>
              </tr>
              <tr>
                <td align="right">เบอร์โทรศํพท์:</td>
                <td style="color:#878787;"><input class="form-control" name="tel" type="text" value="<?php echo $ob->tel; ?>"></td>
              </tr>
              <tr>
                <td align="right">ตำแหน่งงาน:</td>
                <td style="color:#878787;"><input class="form-control" name="staff_occ" type="text" value="<?php echo $ob->staff_occ; ?>"></td>
              </tr>
              
              
              <tr>
                <td></td>
                <td>
                <button name="submit" type="submit" class="btn btn-success btn-sm">
                                            <i class="fa fa-save"></i> บันทึก
                                        </button>
                </td>
              </tr>
            </tbody>
          </table>

         </form>
        </div>
      </div>
    </div>
  </div>
  
<script>

$('#form_edit').submit(function(e){
    e.preventDefault();
    //alert("s");
    $.post("staff_edit_action.php",$('#form_edit').serialize(),function(info){
        if(info == 'ok') {
            window.location = "?page=main";
        } else {
            $('#message').html(info);
        }
    });
});
</script>