<form id="form_shop">
  <div class="form-group">
    <label for="cname">ชื่อบริษัท:</label>
    <input type="text" class="form-control" id="shop_name" name="shop_name" required >
  </div>
  <div class="form-group">
    <label for="address">ที่อยู่:</label>
    <input type="text" class="form-control" id="address" name="address" required >
  </div>
  <div class="form-group">
    <label for="tel">เบอร์โทร:</label>
    <input type="text" class="form-control" id="tel" name="tel" required >
  </div>
 <div id="btn_save">
  <button type="submit" class="btn btn-primary">บันทึก</button>
  </div>

  <div id="btn_finish" style="display:none">
  <button type="button" data-dismiss="modal" class="btn btn-success">บันทึกสำเร็จ ปิดหน้าต่าง</button>
  </div>
</form>
<span id="show_save"></span>

<script>
$('#form_shop').submit(function(e){
    e.preventDefault();
    $.post("shop_add_action.php",$('#form_shop').serialize(),function(info){
        //$('#show_save').html(info);
        var obj = jQuery.parseJSON(info);
        if(obj.status == 'ok') {
           $('#show_save').hide();
            $('#shop_id').load("shop_select.php?shop_id="+obj.shop_id);
            $('#btn_save').hide();
            $('#btn_finish').show();
            
        } else {
          $('#show_save').html(obj.status);
        }
    });
});

</script>