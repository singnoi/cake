<?php
$con = connect();
$user_id = $_SESSION['user_id'];
$edit = false;
$total_price = 0.00;
if( isset($_GET['order_code']) ) {
    $edit = true;
    $order_code = $_GET['order_code'];
}  

if( isset($_SESSION['order_code']) ) {
    $edit = true; 
    $order_code = $_SESSION['order_code'];
}

if( $edit ) {

    $qc = "SELECT *, o.user_id from tbl_order as o left outer join tbl_user as u on u.user_id = o.user_id where o.order_code = '$order_code'";
    $rc = $con->query($qc) or die ($qc);
    $obc = $rc->fetch_object();
    $order_date = $obc->order_date;
    $order_note = $obc->order_note;
    $user_id = $obc->user_id;
    $user_fullname = $obc->fname." ".$obc->lname;
    // ถ้าเป็นชื่อใน admin
    if($user_id == 2000) {
        $qm = "SELECT * from tbl_admin where user_id = '$user_id'";
        $rm = $con->query($qm) or die ($qm);
        $obm = $rm->fetch_object();
        $user_fullname = $obm->fname." ".$obm->lname;
    }
    $shop_id = $obc->shop_id;
    $order_status = ($obc->order_status == 'Y' ? "บันทึกแล้ว" : "ถูกยกเลิก");

    $_SESSION['order_code'] = $order_code;
    session_write_close();
    
} else {
    $order_date = $today_date;
    $user_fullname = $_SESSION['user_fullname'];
    $user_id = $_SESSION['user_id'];
    $order_status = "รอบันทึก";
    $shop_id = 0;
    $order_note = "";
    

    // ----- ออกเลขที่ order_code
    $qo = "SELECT order_code from tbl_order order by order_code DESC limit 1";
    $ro = $con->query($qo) or die ($qo);
    if( $ro->num_rows > 0 ) {
        $max_code =  $ro->fetch_object()->order_code;
        $code_arr = explode("-",$max_code);
        $m = substr($code_arr[0],-2);
        if($m == $month_now) {
            $next_code = intval($code_arr[1]) + 1;
        } else {
            $next_code = 1;
        }
        
    } else {
        $next_code = 1;
    }
    $order_code = "OR".substr($year_now_thai,-2).$month_now."-".sprintf("%04d",$next_code);
    
    // --------------

}
?>
<h4 class="text-center textshadow"> ใบสั่งซื้อสินค้า </h4>
<hr>
<form id="form_order" >

<div class="container-fluid border border-info">
    <div class="row my-2">

        <div class="col-lg-3 ">
            <div class="form-group bg-light">
                <label for="bill_code">เลขที่ใบส่งของ: <code class="text18"><?php echo $order_code;?></code></label>
                <input type="hidden" class="form-control" id="order_code" name="order_code" value="<?php echo $order_code;?>"  >
            </div>
            
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="bill_date">วันที่ใบสั่งซื้อ: <code><?php echo date_thai($order_date);?></code></label>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="inname">ผู้บันทึกข้อมูล: <code><?php echo $user_fullname;?></code></label>
                <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $user_id;?>"  >
            </div>
            
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="inname">สถานะใบสั่งซื้อ: <code><?php echo $order_status;?></code></label>
            </div>
            
        </div>


    </div>
    <div class="row">

        <div class="col-lg-3">
            <div class="form-group">
                <label for="bill_date">วันที่ใบสั่งซื้อ: <code>*</code></label>
                <input type="date" class="form-control" id="order_date1" name="order_date1" value="<?php echo $order_date;?>" required disabled >
                <input type="hidden" d="order_date" name="order_date" value="<?php echo $order_date;?>" >
            </div>
        </div>
        <div class="col-lg-4 bg-light border border-light border-right-0">

            <div class="form-group">
                <label for="company_id">บริษัท/ร้านผู้จำหน่าย: <code>*</code>
                        
                    
                </label>
                <div class="input-group mb-3">
                    <select id="shop_id" name="shop_id" class="form-control select2" required >
                        <option></option>
                        <?php
                        $qs = "SELECT * from tbl_shop order by shop_name ASC";
                        $rs = $con->query($qs) or die ($qs);
                        $ns = $rs->num_rows;
                        if( $ns > 0 ) {
                            while ($obs = $rs->fetch_object()) {
                                $sl = ($shop_id == $obs->shop_id ? "selected" : "");
                                echo "<option value='$obs->shop_id' $sl > $obs->shop_name </option> ";
                            }
                        }
                        ?>
                    </select>
                    
                </div>

            </div>

    </div>
    <div class="col-lg-1 ml-n4 bg-light border border-light border-left-0"> <button class="btn btn-outline-secondary btn-sm" type="button" onclick="shop_add();">  <i class="fas fa-plus"></i> เพิ่มรายการบริษัท</button> </div>

    <div class="col-lg-4">
        <div class="form-group">
                <label for="bill_date">หมายเหตุ: </label>
                <input type="text" class="form-control" id="order_note" name="order_note" value="<?php echo $order_note;?>"  >
            </div>
    </div>

    <div class="col-lg-12">
        <button type="submit" class="btn btn-info btn-sm" >  <i class="fas fa-plus"></i> เพิ่มรายการสินค้า </button>
        <span id="show_err"></span>
    </div>

    </div>
</div>

</form>


<!-- The Modal -->
<div class="modal" id="md3">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">เพิ่มบริษัท/ร้าน ใหม่</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="md3_body">
        Modal body..
      </div>

    </div>
  </div>
</div>


<div class="container-fluid border border-dark border-top-0">
    <div class="row">
        <div class="col-lg-12">
        <table class="table  table-bordered table-striped table-sm">
            <thead class="thead-light">
                <tr>
                    <th width="80">รหัสสินค้า</th>
                    <th>ชื่อสินค้า</th>
                    <th width="120" class="text-center">หน่วยนับ</th>
                    <th width="100" class="text-right">จำนวน</th>
                    <th width="140" class="text-right">ราคา/หน่วย</th>
                    <th width="140" class="text-right">รวมเป็นเงิน</th>
                </tr>
            </thead>
            <tbody id="product_list">
                <?php 
                if($edit) {
                    $q = "SELECT * from tbl_order_list as l left join tbl_product as p on p.product_id = l.product_id where l.order_code = '$order_code'";
                    $r = $con->query($q) or die ($q);
                    if($r->num_rows > 0 ) {
                        while ($obl = $r->fetch_object()) {
                            $sum_price = $obl->order_price * $obl->order_qty;
                            $total_price += $sum_price;
                            echo "<tr>";
                            echo "<td> $obl->product_id </td>";
                            echo "<td> $obl->product_name </td>";
                            echo "<td class='text-center'> $obl->unit_name </td>";
                            echo "<td class='text-right'> ".comma($obl->order_qty)."</td>";
                            echo "<td class='text-right'> ".money($obl->order_price)."</td>";
                            echo "<td class='text-right'> ".money($sum_price)."</td>";
                            echo "</tr>";
                        }
                    }
                }
                ?>
            </tbody>
            <tfoot>

            <form id="form_product">   
                <tr class="bg-light" style="display: none;" id="order_list_add">
                    <td colspan="2">
                        
                        <select class="form-control select2" id="product_id" name="product_id" style="width:100%" onchange="show_val()" >
                            <option value=""></option>
                        <?php
                        $q = "SELECT * from tbl_product order by product_id ASC";
                        $r = $con->query($q) or die ($q);
                        if($r->num_rows > 0) {
                            while ($ob = $r->fetch_object()) {
                                echo "<option value='$ob->product_id' > (".$ob->product_id.") ".$ob->product_name." </option> ";
                            }
                        }
                        ?>
                        </select>
                    </td>
                    <td class="text-center" id="unit"></td>
                    
                    <td>
                        <input type="number" min="1" name="order_qty" id="order_qty" value="" class="form-control text-right" required >
                    </td>
                    
                    <td>
                        <input type="number" min="1" step="0.01" name="order_price" id="order_price" value="" class="form-control text-right" readonly  >
                    </td>
                    <td class="text-right">
                        <button type="submit" class="btn btn-primary btn-sm" > <i class="fas fa-save"></i> </button>
                    </td>
                </tr>
            </form>

                <tr class=" bg-dark text-white">
                    <th colspan="5" class="text-right">รวมเงินทั้งสิ้น</th>
                    <th class="text-right" id="total_price"><?php echo money($total_price);?></th>
                </tr>
            </tfoot>
        </table>


        </div>

        <div class="col-lg-12 text-right my-3">
        <a href="?page=order" class="btn btn-success float-left" style="display:none" id="btn_home" > <i class="fas fa-home"></i> กลับหน้าหลัก </a>
        <span class="text-success font-weight-bold" id="show_saved"></span>
            <button type="button" class="btn btn-primary" id="btn_save"> <i class="fas fa-save"></i> บันทึกใบสั่งซื้อ </button>
            <button type="button" class="btn btn-danger" id="btn_del" onclick="cancel_order('<?php echo $order_code;?>');"> <i class="fas fa-eraser"></i> ยกเลิกใบสั่งซื้อ </button>
            <a href="#" class="btn btn-secondary" id="btn_print"> <i class="fas fa-print"></i> สั่งพิมพ์ </a>
            <!-- <a href="../print/order_print.php?order_code=<?php echo $order_code;?>" target="_blank" class="btn btn-secondary" id="btn_print"> <i class="fas fa-print"></i> สั่งพิมพ์ </a> -->
        </div>

    </div>
</div>
<?php 
//$_SESSION['order_code'] = $order_code;
//session_write_close();
$con->close();
?>

<script>
$('.select2').select2({ 
    theme: "bootstrap4"
});


function cancel_order(id){

var order_note = $('#order_note').val();
if(order_note == '') {
    alert("กรุณา ชี้แจงเหตุผล ในช่องหมายเหตุ ก่อนกดปุ่มยกเลิก !");
    //$('#income_note').focus();
    $("#form_order input[name=order_note]").focus();
    return false;
}

Swal.fire({
    title: 'ยกเลิกใบสั่งซื้อ ?',
    text: "กดยืนยันหากต้องการยกเลิกใบสั่งซื้อ",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'ยืนยัน',
    cancelButtonText: 'ยกเลิก'
}).then((result) => {
    if (result.value) {
        $.post("order_cancel.php",{order_code: id, order_note: order_note},function(info){
            if(info == 'ok') {
                window.location = "?page=order";
            } else {
                alert(info);
            }
        });
    }
});        
}


function show_val() {
    var product_id = $('#product_id').val();
    if(product_id == ''){
        $('#unit').html("");
        $('#order_qty').val("");
        $('#order_price').val("");
    } else {
    //alert(product_id);
        $.post("order_js.php",{product_id: product_id},function(info){
            var obj = JSON.parse(info);
            $('#unit').html(obj.unit_name);
            $('#order_qty').val(1);
            $('#order_price').val(obj.unit_cost);
        });
    }
}

function shop_add() {
    $("#md3").modal();
    $("#md3_body").load("shop_add.php");
}

$('#form_order').submit(function(e){
    e.preventDefault();
    
    var order_code = $('#order_code').val();
    //alert(order_code);
    $.post("order_add_action.php",$('#form_order').serialize(),function(info){
        
        if(info == 'ok') {
            $('#order_list_add').show();
        } else {
            $('#show_err').html(info);
        }
        
    });
});

$('#btn_save').click(function(){
    var shop_id = $('#shop_id').val();
    if(shop_id == '') {
        alert("กรุณาเลือกผู้จำหน่ายก่อนบันทึก !");
        return false;
    }
    $.post("order_add_action.php",$('#form_order').serialize(),function(info){
        
        if(info == 'ok') {
            $('#order_list_add').hide();
            $('#show_saved').html("บันทึกสำเร็จ");
            $('#btn_home').show();
        } else {
            $('#show_err').html(info);
        }
        
    });
});

$('#btn_print').click(function(){
    var order_code = $('#order_code').val();
    var shop_id = $('#shop_id').val();

    if(shop_id == '') {
        alert("กรุณาเลือกผู้จำหน่ายก่อนบันทึก !");
        return false;
    }
    $.post("order_add_action.php",$('#form_order').serialize(),function(info){
        
        if(info == 'ok') {
            $('#order_list_add').hide();
            window.open("../print/order_print.php?order_code="+order_code);

        } else {
            $('#show_err').html(info);
        }
        
    });
});

$('#form_product').submit(function(e){
    e.preventDefault();
    var order_code = $('#order_code').val();
    $.post("order_list_add_action.php",$('#form_product').serialize(),function(info){
        //$('#show_err').html(info);
        if(info == 'ok') {
            $('#unit').html("");
            $('#product_id').val(null).trigger('change');
            $('#order_qty').val("");
            $('#order_price').val("");
            //$('#order_list_add').show();
            $('#product_list').load("order_list.php?order_code="+order_code);
            $.post("order_total.php",{order_code: order_code},function(data){
                $('#total_price').html(data);
            });
        } else {
            $('#show_err').html(info);
        }
        
    });
});
</script>

