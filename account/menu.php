<?php
$con = connect();
$q = "SELECT Count(u.user_id) as c_id FROM tbl_user AS u WHERE u.user_status = 'Y'";
$sum_user = $con->query($q)->fetch_object()->c_id;

$q2 = "SELECT Count(u.customer_id) as c_id FROM tbl_customer AS u";
$sum_customer = $con->query($q2)->fetch_object()->c_id;

$q3 = "SELECT Count(u.product_id) as c_id FROM tbl_product AS u ";
$sum_product = $con->query($q3)->fetch_object()->c_id;

$q4 = "SELECT Count(u.staff_id) as c_id FROM tbl_staff AS u WHERE u.staff_status = 'Y'";
$sum_staff = $con->query($q4)->fetch_object()->c_id;

$sql1 = "SELECT
COUNT(e.product_id) as totals,
(SELECT COUNT(e1.product_id) FROM tbl_product as e1 where (SELECT SUM(t2.transaction_qty)+IFNULL(ts.cut_stock_qty,0) FROM tbl_transaction as t2 WHERE t2.product_id = e1.product_id AND t2.transaction_id > IFNULL(ts.transaction_id,0) ) is NULL ||   (SELECT SUM(t2.transaction_qty)+IFNULL(ts.cut_stock_qty,0) FROM tbl_transaction as t2 WHERE t2.product_id = e1.product_id AND t2.transaction_id > IFNULL(ts.transaction_id,0) ) = 0) as sum_emtry
FROM
tbl_product AS e
LEFT JOIN tbl_product_cat AS c ON c.product_cat_id = e.product_cat_id
LEFT OUTER JOIN 
(SELECT t.product_id,t.transaction_id,t.cut_stock_qty FROM tbl_transaction as t WHERE  t.transaction_code = '0'  ORDER BY t.transaction_date DESC LIMIT 1 ) as ts
ON ts.product_id = e.product_id
 ";
 //echo $sql1;
$sum_stock = $con->query($sql1)->fetch_object()->sum_emtry;

$con->close();
?>
        <!-- Bootstrap List Group -->
        <ul class="list-group sticky-top sticky-offset">

            <li class="list-group-item sidebar-separator menu-collapsed"></li>
            <!-- Separator with title -->
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
            <span class="fas fa-home fa-fw mr-1"></span>
            <a class="navbar-brand" href="../../cake/"><img src="../../cake/img/logo.png" alt="Logo"></a>
            </li>
            <!-- /END Separator -->
            
            <a href="../../cake/account/?page=main" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-user fa-fw mr-3"></span>
                    <span class="menu-collapsed">ข้อมูลส่วนตัว</span>
                </div>
            </a>

            
            <!-- Separator with title -->
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small>งานประจำ (ฝ่ายบัญชี)</small>
            </li>
            <!-- /END Separator -->
            <a href="../../cake/account/?page=order" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-cart-plus fa-fw mr-3"></span>
                    <span class="menu-collapsed">สั่งซื้อสินค้า   </span>
                </div>
            </a>
            <a href="../../cake/account/?page=income" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-cart-arrow-down fa-fw mr-3"></span>
                    <span class="menu-collapsed">รับสินค้า </span>
                </div>
            </a>

            <a href="../../cake/stock/" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-tags fa-fw mr-3"></span>
                    <span class="menu-collapsed">สินค้าคงเหลือ <span class="badge badge-pill badge-danger ml-2"><?php echo comma($sum_stock);?></span></span>
                </div>
            </a>

            <a href="../../cake/report/" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-tasks fa-fw mr-3"></span>
                    <span class="menu-collapsed">รายงานยอดขาย</span>
                </div>
            </a>

            <!-- Separator with title -->
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <small>งานบันทึกข้อมูล</small>
            </li>
            <!-- /END Separator -->

            <a href="../../cake/product/" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-chevron-right fa-fw mr-3"></span>
                    <span class="menu-collapsed">ข้อมูลสินค้า <span class="badge badge-pill badge-primary ml-2"><?php echo comma($sum_product);?></span></span>
                </div>
            </a>

            <a href="../../cake/customer/" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-chevron-right fa-fw mr-3"></span>
                    <span class="menu-collapsed">ข้อมูลลูกค้า <span class="badge badge-pill badge-primary ml-2"><?php echo comma($sum_customer);?></span></span>
                </div>
            </a>

            <a href="../../cake/user/" class="bg-dark list-group-item list-group-item-action text-info">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-users-cog fa-fw mr-3"></span>
                    <span class="menu-collapsed">พนักงานฝ่าย  <span class="badge badge-pill badge-primary ml-2"><?php echo comma($sum_user);?></span> </span>
                </div>
            </a>

            <a href="../../cake/staff/" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-users-cog fa-fw mr-3"></span>
                    <span class="menu-collapsed">พนักงานทั่วไป  <span class="badge badge-pill badge-primary ml-2"><?php echo comma($sum_staff);?></span> </span>
                </div>
            </a>
            <a href="../../cake/car/" class="bg-dark list-group-item list-group-item-action text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-truck-pickup fa-fw mr-3"></span>
                    <span class="menu-collapsed">ข้อมูลรถส่งของ <span class="badge badge-pill badge-primary ml-2"><?php echo comma($sum_customer);?></span></span>
                </div>
            </a>

            <!-- Separator without title -->
            <li class="list-group-item sidebar-separator menu-collapsed"></li>
            
        </ul>
     