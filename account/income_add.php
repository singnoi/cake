<?php
$con = connect();
$user_id = $_SESSION['user_id'];
$edit = false;
$total_price = 0.00;
if( isset($_GET['income_code']) ) {
    $edit = true;
    $income_code = $_GET['income_code'];
}  

if( isset($_SESSION['income_code']) ) {
    $edit = true; 
    $income_code = $_SESSION['income_code'];
}

if( $edit ) {

    $qc = "SELECT *, o.user_id from tbl_income as o left outer join tbl_user as u on u.user_id = o.user_id where o.income_code = '$income_code'";
    $rc = $con->query($qc) or die ($qc);
    $obc = $rc->fetch_object();
    $income_date = $obc->income_date;
    $income_note = $obc->income_note;
    $user_id = $obc->user_id;
    $user_fullname = $obc->fname." ".$obc->lname;
    $bill_code = $obc->bill_code;
    $bill_date = $obc->bill_date;
    $order_code = $obc->order_code;
    // ถ้าเป็นชื่อใน admin
    if($user_id == 2000) {
        $qm = "SELECT * from tbl_admin where user_id = '$user_id'";
        $rm = $con->query($qm) or die ($qm);
        $obm = $rm->fetch_object();
        $user_fullname = $obm->fname." ".$obm->lname;
    }
    $shop_id = $obc->shop_id;
    $income_status = ($obc->income_status == 'Y' ? "บันทึกแล้ว" : "ถูกยกเลิก");
    if($obc->income_status == 'N') {
        ?>
    <script>window.location = "?page=income_cart&income_code=<?php echo $income_code;?>";</script>
        <?php 
    }

    $_SESSION['income_code'] = $income_code;
    $_SESSION['order_code'] = $order_code;
    session_write_close();
    
} else {
    $income_date = $today_date;
    $user_fullname = $_SESSION['user_fullname'];
    $user_id = $_SESSION['user_id'];
    $income_status = "รอบันทึก";
    $shop_id = 0;
    $income_note = "";
    $bill_code = "";
    $bill_date = $today_date;
    $order_code = "";

    // ----- ออกเลขที่ income_code
    $qo = "SELECT income_code from tbl_income order by income_code DESC limit 1";
    $ro = $con->query($qo) or die ($qo);
    if( $ro->num_rows > 0 ) {
        $max_code =  $ro->fetch_object()->income_code;
        $code_arr = explode("-",$max_code);
        $m = substr($code_arr[0],-2);
        if($m == $month_now) {
            $next_code = intval($code_arr[1]) + 1;
        } else {
            $next_code = 1;
        }
        
    } else {
        $next_code = 1;
    }
    $income_code = "IN".substr($year_now_thai,-2).$month_now."-".sprintf("%04d",$next_code);
    
    // --------------

}
?>
<h4 class="text-center textshadow"> รับสินค้า </h4>

<form id="form_income" name="form_income" >

<div class="container-fluid border border-info">
    <div class="row my-2">

        <div class="col-lg-3 ">
            <div class="form-group bg-light">
                <label for="income_code">เลขที่รับ: <code class="text18"><?php echo $income_code;?></code></label>
                <input type="hidden" class="form-control" id="income_code" name="income_code" value="<?php echo $income_code;?>"  >
            </div>
            
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="income_date">วันที่บันทึก: <code><?php echo date_thai($income_date);?></code></label>
                <input type="hidden" id="income_date" name="income_date" value="<?php echo $income_date;?>"  >
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="user_id">ผู้บันทึกข้อมูล: <code><?php echo $user_fullname;?></code></label>
                <input type="hidden" class="form-control" id="user_id" name="user_id" value="<?php echo $user_id;?>"  >
            </div>
            
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="income_status">สถานะใบรับ: <code><?php echo $income_status;?></code></label>
            </div>
            
        </div>


    </div>
    <div class="row">

    <div class="col-lg-6 bg-light border border-light border-right-0">

        <div class="form-group">
            <label for="order_code">จากเลขที่ใบสั่งซื้อ: <code>*</code> </label>
            <div class="input-group mb-3">
                <select id="order_code" name="order_code" class="form-control select2" required <?php if($edit) echo "disabled";?> >
                    <option></option>
                    <?php
                    $qs = "SELECT o.order_code, s.shop_name,  count(l.order_list_id) as cl  from tbl_order as o left join tbl_shop as s on s.shop_id = o.shop_id left outer join tbl_order_list as l on l.order_code = o.order_code where o.order_status = 'Y' group by o.order_code order by o.order_code DESC";
                    $rs = $con->query($qs) or die ($qs);
                    $ns = $rs->num_rows;
                    if( $ns > 0 ) {
                        while ($obs = $rs->fetch_object()) {
                            if($obs->cl > 0) {
                            $sl = ($order_code == $obs->order_code ? "selected" : "");
                            echo "<option value='$obs->order_code' $sl > $obs->order_code ( $obs->shop_name ) </option> ";
                            }
                        }
                    }
                    ?>
                </select>
                
            </div>

        </div>

    </div>
        
        <div class="col-lg-3">
            <div class="form-group">
                <label for="bill_code">เลขที่ใบส่งของ: <code>*</code> </label>
                <input type="text" class="form-control" id="bill_code" name="bill_code" value="<?php echo $bill_code;?>" autocomplete="off" required >
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="bill_date">วันที่ใบส่งของ: <code>* <?php //echo date_thai($bill_date);?> </code></label>
                <input type="date" class="form-control" id="bill_date" name="bill_date" value="<?php echo $bill_date;?>" required >
            </div>
        </div>
        
    <div class="col-lg-12">
        <div class="form-group">
                <label for="income_note">หมายเหตุ: </label>
                <input type="text" class="form-control" id="income_note" name="income_note" value="<?php echo $income_note;?>"  >
            </div>
    </div>

    <div class="col-lg-12">
        <button type="submit" class="btn btn-info" >  <i class="fas fa-search"></i> แสดงรายการสินค้าจากเลขที่ใบสั่งซื้อ </button>
        <span id="show_err3" class="font-weight-bolder text-danger"></span>
    </div>

    </div>
</div>

</form>



<div class="container-fluid border border-dark border-top-0">
    <div class="row">
        <div class="col-lg-12">
        <table class="table  table-bordered table-striped table-sm">
            <thead class="thead-light">
                <tr>
                    <th width="80">รหัสสินค้า</th>
                    <th>ชื่อสินค้า</th>
                    <th width="120" class="text-center">หน่วยนับ</th>
                    <th width="100" class="text-right">จำนวน</th>
                    <th width="140" class="text-right">ราคา/หน่วย</th>
                    <th width="140" class="text-right">รวมเป็นเงิน</th>
                    <th width="40"></th>
                </tr>
            </thead>
            <tbody id="product_list">
                <?php 
                if($edit) {
                    $q = "SELECT * from tbl_income_list as l left join tbl_product as p on p.product_id = l.product_id where l.income_code = '$income_code'";
                    $r = $con->query($q) or die ($q);
                    if($r->num_rows > 0 ) {
                        while ($obl = $r->fetch_object()) {
                            $sum_price = $obl->income_price * $obl->income_qty;
                            $total_price += $sum_price;

                            echo "<tr>";
                            echo "<td> $obl->product_id </td>";
                            echo "<td> $obl->product_name </td>";
                            echo "<td class='text-center'> $obl->unit_name </td>";
                            echo "<td class='text-right'> ".comma($obl->income_qty)."</td>";
                            echo "<td class='text-right'> ".money($obl->income_price)."</td>";
                            echo "<td class='text-right'> ".money($sum_price)."</td>";
                            echo "<td class='text-right'>";
                            ?>
                            <a href="#" class="text-danger" onclick="del_list(<?php echo $obl->income_list_id;?>);" > <i class="far fa-trash-alt"></i> </a>
                            <?php 
                            echo "</td>";
                            echo "</tr>";
                        }
                    }
                }
                ?>
            </tbody>
            <tfoot>

            <form id="form_product">   
                <tr class="bg-light" style="display: none;" id="income_list_add">
                    <td colspan="2">
                        
                        <select class="form-control select2" id="product_id" name="product_id" style="width:100%" onchange="show_val()" >
                            <!-- <option value=""></option> -->
                        <?php

                        // $q = "SELECT * from tbl_product order by product_id ASC";
                        // $r = $con->query($q) or die ($q);
                        // if($r->num_rows > 0) {
                        //     while ($ob = $r->fetch_object()) {
                        //         echo "<option value='$ob->product_id' > (".$ob->product_id.") ".$ob->product_name." </option> ";
                        //     }
                        // }
                        ?>
                        </select>
                    </td>
                    <td class="text-center" id="unit"></td>
                    
                    <td>
                        <input type="number" min="1" name="income_qty" id="income_qty" value="" class="form-control text-right" required >
                    </td>
                    
                    <td>
                        <input type="number" min="1" step="0.01" name="income_price" id="income_price" value="" class="form-control text-right" required readonly >
                        
                    </td>
                    <td>
                    <span id="show_err1"></span>
                    </td>
                    <td class="text-right">
                        <button type="submit" class="btn btn-primary btn-sm" > <i class="fas fa-save"></i> </button>
                    </td>
                </tr>
            </form>

                <tr class=" bg-dark text-white">
                    <th colspan="5" class="text-right">รวมเงินทั้งสิ้น</th>
                    <th class="text-right" id="total_price"><?php echo money($total_price);?></th>
                    <th></th>
                </tr>
            </tfoot>
        </table>


        </div>

        <div class="col-lg-12 text-right my-3">
        <a href="?page=income" class="btn btn-success float-left" style="display:none" id="btn_home" > <i class="fas fa-home"></i> กลับหน้าหลัก </a>
        <span class="text-success font-weight-bold" id="show_saved"></span>
            <button type="button" class="btn btn-primary" id="btn_save"> <i class="fas fa-save"></i> บันทึกรับสินค้า </button>
            <button type="button" class="btn btn-danger" id="btn_del" onclick="cancel_cart('<?php echo $income_code;?>');" > <i class="fas fa-eraser"></i> ยกเลิกใบรับสินค้า </button>
            <a href="#" class="btn btn-secondary" id="btn_print"> <i class="fas fa-print"></i> สั่งพิมพ์ </a>

        </div>

    </div>
</div>
<?php 

$con->close();
?>

<script>
function del_list(id) {
    Swal.fire({
        title: 'ลบรายการสินค้า ?',
        text: "กดยืนยันหากต้องการลบ",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.post("income_list_del.php",{income_list_id: id},function(info){
                if(info == 'ok') {
                    window.location = "?page=income_add";
                } else {
                    alert(info);
                }
            });
        }
    });    
}

$('.select2').select2({ 
    theme: "bootstrap4"
});

function show_val() {
    var product_id = $('#product_id').val();
    if(product_id == ''){
        $('#unit').html("");
        $('#income_qty').val("");
        $('#income_price').val("");
    } else {
    //alert(product_id);
        $.post("income_js.php",{product_id: product_id},function(info){
            var obj = JSON.parse(info);
            $('#unit').html(obj.unit_name);
            $('#income_qty').val(1);
            $('#income_price').val(obj.unit_cost);
        });
    }
}


$('#form_income').submit(function(e){
    e.preventDefault();
    
    var income_code = $('#income_code').val();
    var order_code = $('#order_code').val();
    //alert(income_code);
    $.post("income_add_action.php",$('#form_income').serialize(),function(info){
        
        if(info == 'ok') {
            $('#show_err3').hide();
            //$('#income_list_add').show();
            //$('#product_id').load("income_product_select.php");

            // แก้ไข+++++
            $.post("income_add_order.php",{income_code: income_code, order_code: order_code},function(data){
                $('#show_err3').show();
                if(data == 'ok') {
                    $('#product_list').load("income_list.php?income_code="+income_code);
                } else {
                    $('#show_err3').html(data);
                }
            });
            // ++++++++++++
        } else {
            $('#show_err3').html(info);
            $('#bill_code').focus();
        }
        
    });
});

function cancel_cart(id){

    var income_note = $('#income_note').val();
    if(income_note == '') {
        alert("กรุณา ชี้แจงเหตุผล ในช่องหมายเหตุ ก่อนกดปุ่มยกเลิก !");
        //$('#income_note').focus();
        $("#form_income input[name=income_note]").focus();
        return false;
    }

    Swal.fire({
        title: 'ยกเลิกใบรับสินค้า ?',
        text: "กดยืนยันหากต้องการยกเลิกการรับสินค้า",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ยืนยัน',
        cancelButtonText: 'ยกเลิก'
    }).then((result) => {
        if (result.value) {
            $.post("income_cancel.php",{income_code: id, income_note: income_note},function(info){
                if(info == 'ok') {
                    window.location = "?page=income_cart&income_code="+id;
                } else {
                    alert(info);
                }
            });
        }
    });        
}

$('#btn_save').click(function(){

    var income_code = $('#income_code').val();

    var order_code = $('#order_code').val();
    if(order_code == '') {
        alert("กรุณาเลือกใบสั่งซื้อ ก่อนบันทึก !");
        return false;
    }
    var bill_code = $('#bill_code').val();
    if(bill_code == '') {
        alert("กรุณากรอบเลขที่ใบเสร็จ ก่อนบันทึก !");
        return false;
    }
    $.post("income_add_action.php",$('#form_income').serialize(),function(info){
        
        if(info == 'ok') {
            $('#income_list_add').hide();
            $('#show_saved').html("บันทึกสำเร็จ");
            $('#btn_home').show();
            window.location = "?page=income_cart&income_code="+income_code;
        } else {
            $('#show_err').html(info);
        }
        
    });
});

$('#btn_print').click(function(){
    var income_code = $('#income_code').val();

    var order_code = $('#order_code').val();
    if(order_code == '') {
        alert("กรุณาเลือกใบสั่งซื้อ ก่อนบันทึก !");
        return false;
    }
    var bill_code = $('#bill_code').val();
    if(bill_code == '') {
        alert("กรุณากรอบเลขที่ใบเสร็จ ก่อนบันทึก !");
        return false;
    }

    $.post("income_add_action.php",$('#form_income').serialize(),function(info){
        
        if(info == 'ok') {
            $('#income_list_add').hide();
            window.open("../print/income_print.php?income_code="+income_code);

        } else {
            $('#show_err').html(info);
        }
        
    });
});

$('#form_product').submit(function(e){
    e.preventDefault();
    var income_code = $('#income_code').val();
    //alert(income_code);
    //$('#show_err1').html(income_code);
    $.post("income_list_add_action.php",$('#form_product').serialize(),function(info){
        //$('#show_err1').html(info);
        if(info == 'ok') {
            $('#unit').html("");
            $('#product_id').val(null).trigger('change');
            $('#income_qty').val("");
            $('#income_price').val("");
            //$('#income_list_add').show();
            $('#product_list').load("income_list.php?income_code="+income_code);
            $('#product_id').load("income_product_select.php");

            $.post("income_total.php",{income_code: income_code},function(data){
                $('#total_price').html(data);
            });
        } else {
            $('#show_err1').html(info);
        }
        
    });
});

</script>

