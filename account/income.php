<?php
//include("../includes/db_connect.php");
$con = connect();
$user_type_id = $_SESSION['user_type_id'];
if( isset($_SESSION['income_code'])) unset($_SESSION['income_code']);
if( isset($_SESSION['order_code'])) unset($_SESSION['order_code']);

$sql = "SELECT *,
( SELECT sum(l.income_qty * l.income_price) from tbl_income_list as l where l.income_code = e.income_code ) as sum_price,
( SELECT count(l2.income_list_id) from tbl_income_list as l2 where l2.income_code = e.income_code ) as sum_list
FROM tbl_income as e 
LEFT OUTER JOIN tbl_shop as s on s.shop_id = e.shop_id
ORDER BY e.income_date DESC ";

$r = $con->query($sql) or die ($sql);

?>
<div class="container-fluid">
    <div class="row">

        <div class="col-lg-12">
            <a href="?page=income_add" class="btn btn-outline-success mb-2" > <i class="fas fa-plus"></i> รับสิ้นค้า </a>
        </div>
        <div class="col-lg-12">
        
      

<h4 class="text-center textshadow"> ประวัติรายการรับสินค้า </h4>

<div class="table-responsive">
<table id="tb1" class="table table-striped table-bordered table-sm ">
                        <thead>
                            <tr>
                                <th>เลขที่</th>
                                <th>วันที่</th>
                                <th>ชื่อร้าน</th>
                                <th>เลขที่สั่งซื้อ</th>
                                <th>ใบส่งของ</th>
                                <th class='text-right'>จำนวนรายการ</th>
                                <th class='text-right'>รวมเป็นเงิน</th>
                                <th class='text-center'>จัดการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        if($r->num_rows > 0 ) {
                            while ($result = $r->fetch_object()) {
                                if( $result->income_status == 'N') {
                                    $cl = "class='text-muted' style='text-decoration: line-through;' ";
                                } else {
                                    $cl = "";
                                }
                                ?>
                                <tr <?php echo $cl;?> >
                                    <td><?php echo $result->income_code; ?></td>
                                    <td><?php echo date_thai($result->income_date); ?></td>
                                    <td><?php echo $result->shop_name; ?></td>
                                    <td><?php echo $result->order_code; ?></td>
                                    <td><?php echo $result->bill_code; ?></td>
                                    <td class='text-right'><?php echo comma($result->sum_list); ?></td>
                                    <td class='text-right'><?php echo money($result->sum_price); ?></td>
                                    <td class='text-center'>
                                        <a href="?page=income_add&income_code=<?php echo $result->income_code;?>" class="text-info"><i class="fas fa-file-alt"></i></a>
                                        
                                    </td>
                                </tr>
                            <?php
                        }
                    }
                        $con->close();
                        ?>
                        </tbody>
                    </table>
                    </div>
        </div>
    </div>
</div>



<script>

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหาเลขที่ใบสั่งซื้อ :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [0, "desc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": true,
    //"info": false,
    //"ordering": false,
    //"searching": false,
    //"paging":  false

});

</script>                    