<?php
//include("../includes/db_connect.php");
$con = connect();
$user_type_id = $_SESSION['user_type_id'];
if( isset($_SESSION['order_code'])) unset($_SESSION['order_code']);

$sql = "SELECT *,
( SELECT sum(l.order_qty * l.order_price) from tbl_order_list as l where l.order_code = e.order_code ) as sum_price,
( SELECT count(l2.order_list_id) from tbl_order_list as l2 where l2.order_code = e.order_code ) as sum_list
FROM tbl_order as e 
LEFT OUTER JOIN tbl_shop as s on s.shop_id = e.shop_id
ORDER BY e.order_date DESC ";

$r = $con->query($sql) or die ($sql);

?>
<div class="container-fluid">
    <div class="row">

        <div class="col-lg-12">
            <a href="?page=order_add" class="btn btn-outline-success mb-2" > <i class="fas fa-plus"></i> ออกใบสั่งซื้อสิ้นค้า </a>
        </div>
        <div class="col-lg-12">
        
      

<h4 class="text-center textshadow"> ประวัติรายการสั่งซื้อสินค้า </h4>

<div class="table-responsive">
<table id="tb1" class="table table-striped table-bordered table-sm ">
                        <thead>
                            <tr>
                                <th>เลขที่</th>
                                <th>วันที่</th>
                                <th>ชื่อร้าน</th>
                                <th class='text-right'>จำนวนรายการ</th>
                                <th class='text-right'>รวมเป็นเงิน</th>
                                <th class='text-center'>จัดการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        if($r->num_rows > 0 ) {
                            while ($result = $r->fetch_object()) {

                                if( $result->order_status == 'N') {
                                    $cl = "class='text-muted' style='text-decoration: line-through;' ";
                                } else {
                                    $cl = "";
                                }
                                ?>
                                <tr <?php echo $cl;?> >
                                    <td><?php echo $result->order_code; ?></td>
                                    <td><?php echo date_thai($result->order_date); ?></td>
                                    <td><?php echo $result->shop_name; ?></td>
                                    <td class='text-right'><?php echo comma($result->sum_list); ?></td>
                                    <td class='text-right'><?php echo money($result->sum_price); ?></td>
                                    <td class='text-center'>
                                        <a href="?page=order_add&order_code=<?php echo $result->order_code;?>" class="text-info"><i class="fas fa-file-alt"></i></a>
                                        
                                    </td>
                                </tr>
                            <?php
                        }
                    }
                        $con->close();
                        ?>
                        </tbody>
                    </table>
                    </div>
        </div>
    </div>
</div>



<!-- The Modal -->
<div class="modal" id="md2">
  <div class="modal-dialog">
    <div class="modal-content">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">แก้ไขหมวดสินค้า</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="md_body2">

        

      </div>


    </div>
  </div>
</div>
<!-- The Modal -->

<script>

$('#tb1').DataTable({
    oLanguage: {
        "sLengthMenu": "แสดง _MENU_ รายการ ต่อหน้า",
        "sZeroRecords": "ไม่เจอข้อมูลที่ค้นหา",
        "sInfo": "แสดง _START_ ถึง _END_ ของ _TOTAL_ รายการ",
        "sInfoEmpty": "แสดง 0 ถึง 0 ของ 0 รายการ",
        "sInfoFiltered": "(จากรายการทั้งหมด _MAX_ รายการ)",
        "sEmptyTable": "ไม่มีข้อมูล",
        "sSearch": "ค้นหาเลขที่ใบสั่งซื้อ :",
        "oPaginate": {
            "sPrevious": "ก่อนหน้า :",
            "sNext": "ถัดไป",
            "sLast": "ท้ายสุด",
            "sFirst": "แรกสุด"
        }
    },
    "order": [0, "desc"], // จัดการ  Order by
    "aLengthMenu": [
        [10, 25, 50, 100, 200, 250, 500, -1],
        [10, 25, 50, 100, 200, 250, 500, "All"]
    ],
    "iDisplayLength": 10,  // จัดการ  จำนวนแสดงเริ่มต้น

    "bSort": true,
    //responsive: true,
    bProcessing: true,
    bSortable: false,
    "lengthChange": true,
    //"info": false,
    //"ordering": false,
    //"searching": false,
    //"paging":  false

});

</script>                    