<div class="jumbotron text-center mt-5" style="background-color: MEDIUMSEAGREEN;">
  <h1 class="kanit text-light textB ">ระบบงานบัญชี</h1> 
  <p class="text-light">ร้านถวัชชัย </p> 
</div>
<div class="container mb-5">
  <div class="row">
    <div class="col-lg-12 text-center">

    <a href="../../cake/account/?page=order" class="btn btn-dark  text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-cart-plus fa-fw mr-3"></span>
                    <span class="menu-collapsed">สั่งซื้อสินค้า   </span>
                </div>
            </a>
            <a href="../../cake/account/?page=income" class="btn btn-dark  text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-cart-arrow-down fa-fw mr-3"></span>
                    <span class="menu-collapsed">รับสินค้า </span>
                </div>
            </a>

            <a href="../../cake/stock/" class="btn btn-dark  text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fas fa-tags fa-fw mr-3"></span>
                    <span class="menu-collapsed">สินค้าคงเหลือ <span class="badge badge-pill badge-danger ml-2"><?php echo comma($sum_stock);?></span></span>
                </div>
            </a>

            <a href="../../cake/report/" class="btn btn-dark  text-white">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-tasks fa-fw mr-3"></span>
                    <span class="menu-collapsed">รายงานยอดขาย</span>
                </div>
            </a>


      
    </div>
  </div>
</div>
<?php
include("../profile.php");
?>