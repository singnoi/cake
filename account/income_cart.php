<?php
$con = connect();
//$user_id = $_SESSION['user_id'];
$income_code = $_GET['income_code'];
$total_price = 0.00;

    $qc = "SELECT *, o.user_id from tbl_income as o left outer join tbl_user as u on u.user_id = o.user_id where o.income_code = '$income_code'";
    $rc = $con->query($qc) or die ($qc);
    $obc = $rc->fetch_object();
    $income_date = $obc->income_date;
    $income_note = $obc->income_note;
    $user_id = $obc->user_id;
    $user_fullname = $obc->fname." ".$obc->lname;
    $bill_code = $obc->bill_code;
    $bill_date = $obc->bill_date;
    $order_code = $obc->order_code;
    // ถ้าเป็นชื่อใน admin
    if($user_id == 2000) {
        $qm = "SELECT * from tbl_admin where user_id = '$user_id'";
        $rm = $con->query($qm) or die ($qm);
        $obm = $rm->fetch_object();
        $user_fullname = $obm->fname." ".$obm->lname;
    }
    $shop_id = $obc->shop_id;
    $income_status = ($obc->income_status == 'Y' ? "<span class='font-weight-bolder bg-success text-white'> บันทึกแล้ว </span>" : "<span class='font-weight-bolder bg-danger text-white'> ถูกยกเลิก </span>");
 
?>
<h4 class="text-center textshadow"> ใบรับสินค้า </h4>
<hr>

<div class="container-fluid border border-info">
    <div class="row my-2">

        <div class="col-lg-3 ">
            <div class="form-group bg-light">
                <label for="income_code">เลขที่รับ: <code class="text18"><?php echo $income_code;?></code></label>
            </div>
            
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="bill_date">วันที่บันทึก: <code><?php echo date_thai($income_date);?></code></label>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="user_id">ผู้บันทึกข้อมูล: <code><?php echo $user_fullname;?></code></label>
            </div>
            
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="income_status">สถานะใบรับ: <code><?php echo $income_status;?></code></label>
            </div>
            
        </div>


    </div>
    <div class="row">

    <div class="col-lg-3 ">

        <div class="form-group">
            <label for="order_code">จากเลขที่ใบสั่งซื้อ: <code><?php echo $order_code;?></code> </label>
        </div>

    </div>
        
        <div class="col-lg-3">
            <div class="form-group">
                <label for="bill_code">เลขที่ใบส่งของ: <code><?php echo $bill_code;?></code> </label>
                
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label for="income_date">วันที่ใบส่งของ: <code> <?php echo date_thai($bill_date);?> </code></label>
                
            </div>
        </div>
        
    <div class="col-lg-12">
        <div class="form-group">
                <label for="bill_date">หมายเหตุ: <code> <?php echo $income_note;?> </code></label>
               
            </div>
    </div>


    </div>
</div>



<div class="container-fluid border border-dark my-2">
    <div class="row my-2">
        <div class="col-lg-12">
        <table class="table  table-bordered table-striped table-sm">
            <thead class="thead-light">
                <tr>
                    <th width="80">รหัสสินค้า</th>
                    <th>ชื่อสินค้า</th>
                    <th width="120" class="text-center">หน่วยนับ</th>
                    <th width="100" class="text-right">จำนวน</th>
                    <th width="140" class="text-right">ราคา/หน่วย</th>
                    <th width="140" class="text-right">รวมเป็นเงิน</th>
                </tr>
            </thead>
            <tbody id="product_list">
                <?php 

                    $q = "SELECT * from tbl_income_list as l left join tbl_product as p on p.product_id = l.product_id where l.income_code = '$income_code'";
                    $r = $con->query($q) or die ($q);
                    if($r->num_rows > 0 ) {
                        while ($obl = $r->fetch_object()) {
                            $sum_price = $obl->income_price * $obl->income_qty;
                            $total_price += $sum_price;

                            echo "<tr>";
                            echo "<td> $obl->product_id </td>";
                            echo "<td> $obl->product_name </td>";
                            echo "<td class='text-center'> $obl->unit_name </td>";
                            echo "<td class='text-right'> ".comma($obl->income_qty)."</td>";
                            echo "<td class='text-right'> ".money($obl->income_price)."</td>";
                            echo "<td class='text-right'> ".money($sum_price)."</td>";
                            echo "</tr>";
                        }
                    }
                
                ?>
            </tbody>
            <tfoot>

            
                <tr class=" bg-dark text-white">
                    <th colspan="5" class="text-right">รวมเงินทั้งสิ้น</th>
                    <th class="text-right" id="total_price"><?php echo money($total_price);?></th>
                
                </tr>
            </tfoot>
        </table>


        </div>

        <div class="col-lg-12 text-right my-3">
        <a href="?page=income" class="btn btn-success float-left"  id="btn_home" > <i class="fas fa-home"></i> กลับหน้าหลัก </a>
        <span class="text-success font-weight-bold" id="show_saved"></span>

            <a href="#" class="btn btn-secondary" id="btn_print" onclick="print_cart('<?php echo $income_code;?>')"> <i class="fas fa-print"></i> สั่งพิมพ์ </a>

        </div>

    </div>
</div>
<?php 

$con->close();
?>

<script>

function print_cart(id){
    //alert("test");
    window.open("../print/income_print.php?income_code="+id);
}

</script>

