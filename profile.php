<?php
//include("../../cake/includes/db_connect.php");
$con = connect();
$user_id = $_SESSION['user_id'];
$user_type_id = $_SESSION['user_type_id'];
$user_type_name = $_SESSION['user_type_name'];
if( $user_type_id == 0 ) {
    $q = "SELECT * from tbl_admin where `user_id` = '$user_id' ";
    
} else {
    $q = "SELECT * from tbl_user as u left join tbl_user_type as t on t.user_type_id = u.user_type_id where `user_id` = '$user_id' ";
}
$r = $con->query($q) or die ($q);
$ob = $r->fetch_object();

?>
<h3 class="text-center textshadow"> ข้อมูลส่วนตัว </h3>

<div class="card">
    <div class="row">
      <div class="col-lg-3 col-md-1">
      </div>
      <div class="col-lg-6 col-md-10">
        <div class="card-body card-block">
          <table class="table table-borderless">
            <tbody>
              <tr>
                <td align="right">รหัส: </td>
                <td style="color:#878787;"><?php echo $ob->user_id; ?></td>
              </tr>
              <tr>
                <td align="right">ชื่อ:</td>
                <td style="color:#878787;"><?php echo $ob->fname; ?></td>
              </tr>
              <tr>
                <td align="right">นามสกุล:</td>
                <td style="color:#878787;"><?php echo $ob->lname; ?></td>
              </tr>
              <tr>
                <td align="right">ที่อยู่:</td>
                <td style="color:#878787;"><?php echo $ob->address; ?></td>
              </tr>
              <tr>
                <td align="right">เบอร์โทรศัพท์:</td>
                <td style="color:#878787;"><?php echo $ob->tel; ?></td>
              </tr>
              <tr>
                <td align="right">อีเมล์:</td>
                <td style="color:#878787;"><?php echo $ob->email; ?></td>
              </tr>
              <tr>
                <td align="right">ชื่อผู้ใช้:</td>
                <td style="color:#878787;"><?php echo $ob->user_name; ?></td>
              </tr>
              <tr>
                <td align="right">รหัสผ่าน:</td>
                <td style="color:#878787;"><?php echo $ob->pass_word; ?></td>
              </tr>
              <tr>
                <td align="right">สถานะ:</td>
                <td style="color:#878787;">
                  <?php 
                  if( $user_type_id == 0 ){
                    echo "ผู้ดูแลระบบ";
                  } else {
                    echo $ob->user_type_name;
                  }
                  ?>
                  </td>
              </tr>
              <tr>
                <td></td>
                <td>
                  <a href="../../cake/?page=profile_edit" class="btn btn-primary btn-sm">
                    <i class="fa fa-edit"></i> แก้ไข
                  </a>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>